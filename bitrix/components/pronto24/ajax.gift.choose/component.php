<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$giftID = !empty(intval(htmlspecialchars($_POST['gift']))) ? intval(htmlspecialchars($_POST['gift'])) : false;

if ($giftID) {
	CModule::IncludeModule('iblock');
	CModule::IncludeModule('sale');

	$dbBasketItems = CSaleBasket::GetList(
		array(
			"NAME" => "ASC"
		),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
		false,
		false,
		array("PRICE", "QUANTITY")
	);
	$totalPrice = 0;
	while ($arItem = $dbBasketItems->Fetch()) {
		$totalPrice += $arItem['PRICE'] * $arItem['QUANTITY'];
	}
	$totalPrice = round($totalPrice);

	$arFilter = Array(
		"IBLOCK_ID" => 34,
		"ACTIVE" => "Y",
		"<=PROPERTY_MIN_SUM" => $totalPrice
	);

	$arProperties = Array('ID', 'NAME', 'IBLOCK_ID');
	$res = CIBlockElement::GetList(Array("PROPERTY_MIN_SUM" => "DESC"), $arFilter, false, false, $arProperties);

	$obElement = $res->GetNextElement();
	if(is_numeric($obElement->fields['ID'])) {
		$props = $obElement->GetProperties();
		foreach($props as $key => $property) {
			if(isset($property['VALUE']) && $key == 'GIFT') {
				//Установка нужного подарка
				foreach($property['VALUE'] as $gift) {
					if($giftID == $gift) {
						$_SESSION['CHOOSED_GIFT'] = $giftID;
					}
				}

			}
		}
	}
}

$result = Array('result' => 'ok');

return $result;