<form actiov="/about/reviews/" method="POST" id="sendReview" enctype="multipart/form-data">

	<h2 class="small-text">Уважаемые гости!</h2>

	<h2 class="small-text">
		Для совершенствования качества обслуживания убедительно
		просим вас указывать в отзыве конкретное кафе, которое вы
		посетили. Или адрес и номер заказа, если вы воспользовались
		услугами нашей доставки блюд. Спасибо!
	</h2>

	<div class="form-grid">
		<div class="form-row">
			<div class="form-cell">имя</div>
			<div class="form-cell">
				<div class="input-data">
					<input name="name" type="text">
				</div>
			</div>
		</div>
		<div class="form-row">
			<div class="form-cell">тема</div>
			<div class="form-cell">
				<div class="input-data">
					<input name="theme" type="text">
				</div>
			</div>
		</div>
		<div class="form-row">
			<div class="form-cell">ресторан</div>
			<div class="form-cell">
				<div class="input-data">
					<select name="restaurant">
						<? foreach($APPLICATION->IncludeComponent("pronto24:restaurants.select.list") as $key => $restaurant) : ?>
							<option value="<?=$key?>"><?=$restaurant?></option>
						<? endforeach ?>
					</select>
				</div>
			</div>
		</div>
	</div>

	<div class="textarea-box">
		<div class="form-label">ваш отзыв</div>
		<div class="input-data">
			<textarea name="text"></textarea>
			<!--div class="hint">Вы ввели какую то хрень</div-->
		</div>
	</div>
	<div class="m-b25">
		<input type="file" class="file-input" name="files[]" id="reviews-file" data-input-file
		       onchange="MainManager.getFileName()" multiple>
		<label for="reviews-file">загрузить файл</label>
		<ul class="ul-file" data-list-reviews-file></ul>
	</div>
	<? if (!$USER->IsAuthorized()): ?>
		<? /*<h2 class="small-text">
			Оставить отзыв может только зарегистрированный
			пользователь.
			<a href="#" onclick="MainManager.showModal('authorization'); return false;">Авторизуйтесь</a>,
			пожалуйста, или пройдите
			регистрацию прямо здесь:
		</h2>*/ ?>

		<div class="form-grid">
			<div class="form-row">
				<div class="form-cell">телефон</div>
				<div class="form-cell _txt-right">
					<div class="input-data">
						<input id="login" name="login" type="text">
					</div>
				</div>
			</div>
		</div>
	<? endif ?>

	<div class="_txt-center">
		<button type="submit" class="btn green m-t10" tabindex="6">Отправить</button>
	</div>

</form>