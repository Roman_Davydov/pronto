<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');

$arFilter = Array(
	"IBLOCK_CODE" => 'restaurant_menu',
	"ACTIVE" => "Y"
);

$arProperties = Array('PROPERTY_LINK', 'NAME', 'PREVIEW_PICTURE', 'PREVIEW_TEXT');
$res = CIBlockElement::GetList(Array("SORT"=>"ASC"), $arFilter, false, false, $arProperties);

while ($arElement = $res->GetNext()) {
	$picture = CFile::GetFileArray($arElement["PREVIEW_PICTURE"]);
	$arr = Array();
	$arr['LINK'] = $arElement['PROPERTY_LINK_VALUE'];
	$arr['SRC'] = $picture['SRC'];
	$arr['NAME'] = $arElement['NAME'];
	$arr['TEXT'] = $arElement['PREVIEW_TEXT'];
	$arResult[] = $arr;
}

$this->IncludeComponentTemplate();