<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$faker = 3;

CModule::IncludeModule('sale');

$cacheTime = 60;
$cacheId = 'pizzaCounter';
$cachePath = '/my/cache/path';
$obCache = new CPHPCache();
if ($obCache->InitCache($cacheTime, $cacheId, $cachePath)) {
	$counter = $obCache->GetVars();
} else {
	$arFilter = Array(
		">=DATE_INSERT" => date($DB->DateFormatToPHP(CSite::GetDateFormat("SHORT")), mktime(0, 0, 0, date("n"), date("d"), date("Y")))
	);

	$db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), $arFilter);

	while ($ar_sales = $db_sales->Fetch()) {
		$dbBasketItems = CSaleBasket::GetList(array(
			"NAME" => "ASC"
		),
			array(
				"LID" => SITE_ID,
				"ORDER_ID" => $ar_sales['ID']
			),
			false,
			false,
			array("ID", "NAME", "QUANTITY", "PRODUCT_ID")
		);

		while ($arItem = $dbBasketItems->Fetch()) {
			$res = CIBlockElement::GetList(Array(), Array('ID' => $arItem['PRODUCT_ID']), false, false, Array('ID', 'IBLOCK_ID'));
			$arElement = $res->GetNext();

			if ($arElement['IBLOCK_ID'] == 5)
				$arResult['pizzaCounter'] += $faker * $arItem['QUANTITY'];
		}
	}
	$counter = $arResult['pizzaCounter'] > 0 ? $arResult['pizzaCounter'] + date("G") : 0;

	$obCache->EndDataCache(array("counter" => $counter));
}

$arResult['pizzaCounter'] = $counter;


$this->IncludeComponentTemplate();