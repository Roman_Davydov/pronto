<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');

$arProperties = Array(
	'PROPERTY_ADDRESS',
	'PROPERTY_SECTION',
	'PROPERTY_STAGE',
	'PROPERTY_FLAT'
);

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), Array('ID' => $arParams['ID'], "IBLOCK_ID" => 29), false, false, $arProperties);

$element = $res->GetNextElement();

$arResult['address'] = $element->fields['PROPERTY_ADDRESS_VALUE'];
$arResult['room'] = $element->fields['PROPERTY_FLAT_VALUE'];
$arResult['entrance'] = $element->fields['PROPERTY_SECTION_VALUE'];
$arResult['floor'] = $element->fields['PROPERTY_STAGE_VALUE'];

return $arResult;