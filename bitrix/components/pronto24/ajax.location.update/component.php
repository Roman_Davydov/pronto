<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

global $USER;
$result = Array('result' => 'error');
$id = isset($_POST['id']) ? $_POST['id'] : false;
$city = isset($_POST['city']) ? $_POST['city'] : false;
$street = isset($_POST['street']) ? $_POST['street'] : false;
$house = isset($_POST['house']) ? $_POST['house'] : false;
$metro = isset($_POST['metro']) ? $_POST['metro'] : false;
$corpus = isset($_POST['corpus']) ? $_POST['corpus'] : false;
$section = isset($_POST['section']) ? $_POST['section'] : false;
$build = isset($_POST['build']) ? $_POST['build'] : false;
$stage = isset($_POST['stage']) ? $_POST['stage'] : false;
$flat = isset($_POST['flat']) ? $_POST['flat'] : false;

if ($id && $city && $street && $house) {
	CModule::IncludeModule('iblock');
	$el = new CIBlockElement;

	$arFields = Array(
		"NAME" => $street . ' ' . $house,
		"IBLOCK_ID" => 29,
		"PROPERTY_VALUES" => Array(
			'CITY' => $city,
			'STREET' => $street,
			'HOUSE' => $house,
			'FLAT' => $flat,
			'USER' => $USER->GetID()
		));
		$arFields["PROPERTY_VALUES"]["METRO"] = $metro;
		$arFields["PROPERTY_VALUES"]["CORPUS"] = $corpus;
		$arFields["PROPERTY_VALUES"]["SECTION"] = $section;
		$arFields["PROPERTY_VALUES"]["STAGE"] = $stage;
		$arFields["PROPERTY_VALUES"]["BUILD"] = $build;

	$el->Update($id, $arFields);

	$result = Array('result' => 'ok');
}

return $result;