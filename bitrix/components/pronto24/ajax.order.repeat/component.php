<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('sale');
CModule::IncludeModule('iblock');

$orderId = isset($_POST['orderId']) ? (int) $_POST['orderId'] : false;

if ($orderId) {
	$dbBasketItems = CSaleBasket::GetList(array(
		"NAME" => "ASC"
	),
		array(
			"LID" => SITE_ID,
			"ORDER_ID" => $orderId
		),
		false,
		false,
		array("ID", "QUANTITY", "PRODUCT_ID")
	);

	$arItems = Array();
	$arActualMods = Array();
	while ($arItem = $dbBasketItems->Fetch()) {
		$res = CIBlockElement::GetList(Array(), Array('ID' => $arItem['PRODUCT_ID']), false, false, Array('ID', 'IBLOCK_ID'));
		$arElement = $res->GetNext();
		if($arElement['IBLOCK_ID'] != 26) {
			$db_res = CSaleBasket::GetPropsList(
				array(
					"SORT" => "ASC",
					"NAME" => "ASC"
				),
				array("BASKET_ID" => $arItem['ID'])
			);
			while ($ar_res = $db_res->Fetch()) {
				if ($ar_res['NAME'] == 'MODIFICATOR') {
					$arItem['PROPS'][] = $ar_res;
				}
			}
			$arItems[] = $arItem;
		}
		else {
			$arActualMods[$arItem['ID']] = $arItem;
		}
	}

	foreach($arItems as $item) {
		if (isset($item["PROPS"])) {
			$arMods = Array();
			foreach ($item["PROPS"] as $modificator) {
				$modIdInCatalog = Add2BasketByProductID($arActualMods[$modificator['CODE']]['PRODUCT_ID'], $item['QUANTITY'] * $modificator['VALUE']);
				$arMods[] = Array("NAME" => "MODIFICATOR", "CODE" => $modIdInCatalog, "VALUE" => $modificator['VALUE']);
			}

			$basketResult = Add2BasketByProductID(
				$item['PRODUCT_ID'],
				$item['QUANTITY'],
				Array(),
				$arMods
			);
		} else {
			$basketResult = Add2BasketByProductID(
				$item['PRODUCT_ID'],
				$item['QUANTITY'],
				Array()
			);
		}
	}

	$APPLICATION->IncludeComponent("pronto24:basket.line", "json");
	$result = Array('result' => 'ok', 'basket' => $_SESSION['basket_line']);
}
else
	$result = Array('result' => 'error');

return $result;
?>