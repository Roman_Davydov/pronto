'use strict';

function LocationManager_f() {
	this.location = false;
	this.address;
	this.addressName;
	this.section;
	this.stage;
	this.flat;
	this.lat;
	this.lon;
	this.locations;
	this.locationID = false;
	this.restaurants;
	this.restaurant = false;
	this.zone = false;
	this.myMap;
	this.collection;
	this.suggestions;

	this.newPoint = function () {
		LocationManager.collection.removeAll();
		LocationManager.collection.add(new ymaps.Placemark([LocationManager.lat, LocationManager.lon], '', {
			iconImageHref: "/bitrix/templates/pronto/images/point.png",
			iconImageSize: [31, 47]
		}));
		LocationManager.myMap.setCenter([LocationManager.lat, LocationManager.lon]);
		LocationManager.myMap.geoObjects.add(LocationManager.collection);
		$("#map").show();
	};

	this.loadRestaurants = function () {
		LocationManager.restaurants = restPolygons();
	};

	this.loadBasketMap = function () {
		ymaps.ready(function () {
			LocationManager.myMap = new ymaps.Map('map', {center: [55.76, 37.64], zoom: 16});
			LocationManager.collection = new ymaps.GeoObjectCollection({});
		});
	};

	this.initSuggestions = function() {
		LocationManager.suggestions = $("[data-address]").suggestions({
			serviceUrl: "https://suggestions.dadata.ru/suggestions/api/4_1/rs",
			token: "f53809dc9ad4846a811a2f794772e0e9bbe157e4",
			type: "ADDRESS",
			count: 8,
			autoSelectFirst: true,
			deferRequestBy: 600,
			floating: true,
			timeout: 1000,
			minChars: 5,
			onSelect: function (suggestion) {
				if (suggestion.data.fias_level > 6) {
					LocationManager.lat = suggestion.data.geo_lat;
					LocationManager.lon = suggestion.data.geo_lon;
					LocationManager.newPoint();
					LocationManager.address = suggestion.value;
					LocationManager.addressName = suggestion.data.street_with_type + ' ' + suggestion.data.house;
					LocationManager.section = $('[data-section]').val();
					LocationManager.stage = $('[data-stage]').val();
					LocationManager.flat = $('[data-flat]').val();
					MainManager.showAddressError(false);
					LocationManager.testOutsideMap();
				}
				else {
					MainManager.clearHints();
					MainManager.showAddressError(true);
				}
			},
			onSelectNothing: function (data) {
				MainManager.clearHints();
			}
		});
		if($('[data-address]').val() != '') {
			$('[data-address]').focus();
		}
	};

	this.init = function () {
		$("#map").hide();
		if(window.location.pathname != '/personal/') {
			LocationManager.loadBasketMap();
			LocationManager.loadRestaurants();
			MainManager.showAddressError(false);
			$('.apply .text').hide();
			LocationManager.initSuggestions();
		}
		LocationManager.initLocations();
		$(document)
		 .on("click", "[data-address-list]", LocationManager.copyAddress);
	};

	this.copyAddress = function () {
		$("[data-address-list]").removeClass('active');
		$(this).addClass('active');
		var index = $(this).attr('data-address-list');
		LocationManager.doCopy(LocationManager.locations[index]);
	};

	this.doCopy = function (element) {
		LocationManager.locationID = element.id;
		$('[data-address]').val(element.address).focus();
		$('[data-section]').val(element.section);
		$('[data-stage]').val(element.stage);
		$('[data-flat]').val(element.flat);
	};

	this.initLocations = function () {
		if(typeof locations != 'undefined') {
			LocationManager.locations = locations;
			LocationManager.locations.forEach(function (element, index) {
				$('.addr-list h2').after('<button data-address-list="' + index + '" class="btn border">' + element.title + '</button>');
			});
		}
	};

	this.testOutsideMap = function () {
		LocationManager.zone = 0;
		LocationManager.restaurant = 0;
		if (typeof LocationManager.lat != 'undefined' && typeof LocationManager.lon != 'undefined') {
			ymaps.ready(function () {
				var myMap = new ymaps.Map('fakeMap', {center: [55.76, 37.64], zoom: 10});
				var point = new ymaps.Placemark([LocationManager.lat, LocationManager.lon], '', {
					iconImageHref: "/bitrix/templates/pronto/images/point.png",
					iconImageSize: [31, 47]
				});
				myMap.geoObjects.add(point);
				for (key in LocationManager.restaurants) {
					for (var zone in LocationManager.restaurants[key]) {
						var myPolygon = new ymaps.Polygon([
							LocationManager.restaurants[key][zone].polygon
						]);
						myMap.geoObjects.add(myPolygon);
						if (myPolygon.geometry.contains(point.geometry.getCoordinates())) {
							LocationManager.restaurant = key;
							LocationManager.zone = zone;
							break;
						}
					}
				}
				if (LocationManager.zone == 0 || LocationManager.restaurant == 0) {
					MainManager.showAddressError(true);
					MainManager.showTextAlert('К сожалению, ваш адрес не входит в зону доставки наших ресторанов, но вы можете сами забрать заказ в удобном для вас <a href="/restaurants/">ресторане</a>');
				}
			});
		}
	};

	this.saveLocation = function (callback) {
		LocationManager.section = $('[data-section]').val();
		LocationManager.stage = $('[data-stage]').val();
		LocationManager.flat = $('[data-flat]').val();

		if (LocationManager.address && LocationManager.addressName) {
			$.post(
				"/ajax/",
				{
					"action": 'locationUpdate',
					"id": LocationManager.locationID,
					"address": LocationManager.address,
					"addressName": LocationManager.addressName,
					"section": LocationManager.section,
					"stage": LocationManager.stage,
					"flat": LocationManager.flat
				},
				function (data) {
					if (data.result == 'ok') {
						if (!LocationManager.locationID) {
							var index = $('[data-address-list]').length;
							$('[data-address-list]').removeClass("active");
							$('.addr-list h2').after('<button data-address-list="' + index + '" class="btn border active">' + data.title + '</button>');
							LocationManager.locationID = data.locationID;
						}
						if (typeof callback == 'undefined')
							MainManager.showTextAlert('Новый адрес сохранен');
						else
							callback();
					}
				}, "json");
		}
		else
			MainManager.showTextAlert('Поля "город", "улица" и "дом" обязательны к заполнению');
	};
}