<div class="cabinet-row">
	<h2 class="small">История заказов</h2>
	<table class="order-list">
		<thead>
		<tr>
			<td>номер</td>
			<td>Дата</td>
			<td>Заказ</td>
			<td>сумма</td>
			<td>статус</td>
			<td>удалить</td>
			<td>повторить</td>
		</tr>
		</thead>
		<tbody>
		<? foreach ($arResult as $key => $item):?>
		<tr class="order tr<?= (round(($key - 1) / ordersOnPage)) + 1 ?>">
			<td><?= $item['ID']?></td>
			<td><?= $item['DATE']?></td>
			<td>
				<? foreach($item['ROWS'] as $row):?>
					<?= $row['NAME']?> <?= $row['QUANTITY'] > 1 ? "(x".$row['QUANTITY'].')' : "" ?>
					<?= isset($row['PROPS']) ? "+" . getModificatorStringFromPropsAndBasket($row['PROPS'], $item['ALL_PRODUCTS']) : ""?>
					<br>
				<? endforeach ?>
			</td>
			<td><?= $item['PRICE']?> руб.</td>
			<td><?= $item['STATUS_ID'] == 'F' ? 'Выполнен' : 'Выполняется' ?></td>
			<td>
				<? if($item['STATUS_ID'] == 'F'):?>
				<button data-delete-order="<?= $item['ID']?>" class="btn orange close-icon"></button>
				<? endif ?>
			</td>
			<td>
				<button data-repeat-order="<?= $item['ID']?>" class="btn orange ok"></button>
			</td>
		</tr>
		<? endforeach ?>
		</tbody>
	</table>

	<? if(count($arResult) > ordersOnPage):?>
		<? $bFirst = true; ?>
	<div class="paginate small">
		<? for($i = 1; $i < (round(count($arResult) / ordersOnPage) + 1) ; $i++):?>
		<a data-paginate-orders href="#" <?= $bFirst ? 'class="active"' : "" ?>><?= $i ?></a>
			<? $bFirst = false; ?>
		<? endfor ?>
	</div>
	<? endif ?>
</div>