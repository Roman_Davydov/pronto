<? if(is_array($arResult)) :?>
<table class="base small m-b25">
	<? foreach ($arResult as $occupation => $arPlaces): ?>
		<tr>
			<td><?= $occupation ?></td>
			<td>
				<? foreach ($arPlaces as $place): ?>
					<?= $place['PLACE'] ?> <?= $place['URGENT'] == 'Y' ? 'СРОЧНО!' : '' ?>
				<? endforeach; ?></td>
		</tr>
	<? endforeach; ?>
</table>
<? endif ?>