<?
CModule::IncludeModule("socialservices");
$arParams['BACKURL'] = $_SERVER['HTTP_REFERER'];
$oAuthManager = new CSocServAuthManager();
    $arServices = $oAuthManager->GetActiveAuthServices($arParams);
?>

<form action="">
    <h2>Регистрация</h2>
    <div class="form-grid">
        <div class="form-row">
            <div class="form-cell">телефон</div>
            <div class="form-cell _txt-right">
                <div class="input-data">
                    <input id="login" type="text" tabindex="1">
                    <!--div class="hint">Например +7 926 000 00 00</div-->
                </div>
            </div>
        </div>
    </div>
    <label class="checkbox-label" tabindex="5">
        <input class="checkbox" type="checkbox" name="ac" id="ac">
        <span class="checkbox-label-text">Ознакомлен с <a href="#" onclick="MainManager.clear(this); MainManager.showModal('agreement'); return false;">условиями</a></span>
    </label>
    <div class="_txt-center">
        <button class="btn green button-reg" type="submit" tabindex="6">Отправить</button>
        <div class="form-bottom">
            <p>регистрация через социальные сети:</p>
            <a href="#" onClick="<?=$arServices['VKontakte']['ONCLICK']?>" class="soc-icon-s_1"></a>
            <a href="#" onClick="<?=$arServices['Facebook']['ONCLICK']?>" class="soc-icon-s_2"></a>
        </div>
    </div>
</form>