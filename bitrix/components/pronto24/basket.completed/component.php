<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

if(is_numeric($_SESSION['ORDER_ID']) && $_SESSION['ORDER_ID'] > 0) {
	CModule::IncludeModule('iblock');

	$arSelect = Array("ID", "IBLOCK_ID", "NAME", "PROPERTY_JUPITER_ID", "PROPERTY_PLATRON_ID");
	$arFilter = Array("IBLOCK_ID"=>3, "PROPERTY_JUPITER_ID" => $_SESSION['RESTAURANT']);

	$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);

	$obElement = $res->GetNextElement();
	if($obElement) {
		$arProperties = $obElement->GetProperties();

		foreach($arProperties as $property) {
			if($property['CODE'] == 'PHONE') {
				$arResult['PHONES'] = $property['VALUE'];
			}
			if($property['CODE'] == 'LOCATION')
				$arResult['ADDRESS'] = $property['VALUE'];
		}
		if(!$_SESSION['UPLOAD']) {
			$APPLICATION->IncludeComponent("pronto24:jupiter.order.xml", "", Array('ORDER' => $_SESSION['ORDER_ID']));
			$_SESSION['UPLOAD'] = true;
		}
		$arResult['RESTAURANT'] = $obElement->fields['NAME'];

		$this->IncludeComponentTemplate();
	}
	else {
		//тут должен быть эксепшн
		$to = "warlight@mail.ru";
		$subject = "Ошибка на сайте Пронто";
		$subject = "=?UTF-8?B?" . base64_encode($subject) . "?=";
		$from = "Сайт pronto";

		$message .= "<br/><b>Проблемный Jupiter ID:</b> " . $_SESSION['RESTAURANT'];

		$headers = "From: $from\r\nReply-To: $email\r\n";
		$headers .= "Content-type: text/html; charset=utf-8;\r\n";

		mail($to, $subject, $message, $headers);
	}
}
else {
	require($_SERVER['DOCUMENT_ROOT'] . '/404.php');
}