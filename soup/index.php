<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');
$APPLICATION->SetTitle("Супы");
?>
	<div class="content">
		<div class="wrapper index product soup-style">
			<div class="intro">
				Супы
			</div>
			<div class="sort">
				<a href="#" class="active">все</a>
			</div>
			<? $APPLICATION->IncludeComponent("pronto24:catalog.section", '.default', Array('IBLOCK_ID' => 15)); ?>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>