<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');

$arProperties = Array(
	'ID',
	'IBLOCK_ID',
	'CODE',
	'NAME',
);

$arFilters = Array("IBLOCK_ID" => 35, "ACTIVE" => "Y");

if(isset($arParams['FILTER'])) {
	$arFilters['PROPERTY_' . $arParams['FILTER'] . '_VALUE'] = 'Y';
}


$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilters, false, false, $arProperties);

while ($arElement = $res->GetNext()) {
	$arResult[] = $arElement['NAME'];
}
if(empty($arParams['MESSAGE'] || empty($arParams['HEADERS']) || empty($arParams['SUBJECT']))) {
	return $arResult;
}
else {
	mail(implode(', ', $arResult), $arParams['SUBJECT'], htmlspecialchars_decode($arParams['MESSAGE']), $arParams['HEADERS']);
}