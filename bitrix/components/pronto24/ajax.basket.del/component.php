<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$productID = intval(htmlspecialchars($_POST["product_id"]));
$result = Array('result' => 'error');
if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
	if (IntVal($productID) > 0) {

		//а вдруг это пицца и к ней привязаны модификаторы? надо посчитать
		$product = CSaleBasket::GetByID($productID);
		$db_res = CSaleBasket::GetPropsList(
			array(
				"SORT" => "ASC",
				"NAME" => "ASC"
			),
			array("BASKET_ID" => $product['ID'])
		);
		while ($ar_res = $db_res->Fetch()) {
			if ($ar_res['NAME'] == 'MODIFICATOR') {
				$ar_res['CODE'];
				$ar_res['VALUE'];
				$productModificator = CSaleBasket::GetByID($ar_res['CODE']);
				//если не только здесь - нужно понять..
				if ($productModificator['QUANTITY'] == $ar_res['VALUE'] * $product['QUANTITY']) {
					//убираем совсем модификатор
					CSaleBasket::Delete($ar_res['CODE']);
				} else {
					//увеличим/уменьшим нужные позиции в корзине на нужные числа
					CSaleBasket::Update(
						$ar_res['CODE'], array("QUANTITY" => $productModificator["QUANTITY"] - $ar_res['VALUE'] * $product['QUANTITY']));
				}
			}
		}

		$result = CSaleBasket::Delete(
			$productID
		);

		if ($result) {
			$APPLICATION->IncludeComponent("pronto24:basket.line", "json");
			$result = Array('result' => 'ok', 'basket' => $_SESSION['basket_line']);

			$stocks = $APPLICATION->IncludeComponent("pronto24:stock.update", "", Array("TOTAL_PRICE" => $_SESSION['basket_line']['clear_price']));
			if (!empty($stocks)) {
				$result['stocks'] = $stocks;
			}
		}
	}
}

return $result;