<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');

global $USER;

$card = strlen(str_replace(' ', '', htmlspecialchars($_POST["card"]))) >= 10 ? $_POST["card"] : false ;

$result = Array('result' => 'error');

if ($card) {
	$arFilter = Array(
		"IBLOCK_ID" => 28,
		"ACTIVE" => "Y",
		"PROPERTY_USER" => false,
		"PROPERTY_USED" => false,
		'NAME' => $card
	);

	$arProperties = Array('NAME', 'PROPERTY_USER', 'PROPERTY_USED', "ID");
	$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arProperties);

	while ($arElement = $res->GetNext()) {
		$el = new CIBlockElement;

		//сначала, видимо, нужно отвязать старую
		$arFilter = Array(
			"IBLOCK_ID" => 28,
			"ACTIVE" => "Y",
			"PROPERTY_USER" => $userID
		);

		$arProperties = Array('NAME', 'PROPERTY_USER', 'ID');
		$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arProperties);
		while ($arElement = $res->GetNext()) {
			$el->Update($arElement['ID'], Array("PROPERTY_VALUES" => Array("USER" => false)));
		}

		$el->Update($arElement["ID"], Array("PROPERTY_VALUES" => Array("USER" => $USER->getID(), "USED" => 1)));
		$result = Array('result' => 'ok');
	}
}

return $result;