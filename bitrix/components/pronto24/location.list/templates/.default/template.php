<script>
	var locations = [];
	<? foreach ($arResult as $element): ?>
	var locationAddress = {
		title: '<?=$element['TITLE']?>',
		id: '<?=$element['ID']?>',
		address: '<?=$element['PROPERTY_ADDRESS_VALUE']?>',
		section: '<?=$element['PROPERTY_SECTION_VALUE']?>',
		stage: '<?=$element['PROPERTY_STAGE_VALUE']?>',
		flat: '<?=$element['PROPERTY_FLAT_VALUE']?>'
	};

	locations.push(locationAddress);
	<? endforeach; ?>
</script>