<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$productID = intval(htmlspecialchars($_POST["product_id"]));
$quantity = intval(htmlspecialchars($_POST["cnt"]));
$quantity = $quantity == 0 ? 1 : $quantity;
$result = Array('result' => 'error');

if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
	if (IntVal($productID) > 0) {
		if (isset($_POST["modificators"])) {
			$arMods = Array();
			foreach ($_POST["modificators"] as $modificator) {
				$modIdInCatalog = Add2BasketByProductID($modificator['id'], $quantity * $modificator['count']);
				$arMods[] = Array("NAME" => "MODIFICATOR", "CODE" => $modIdInCatalog, "VALUE" => $modificator['count']);
			}

			$basketResult = Add2BasketByProductID(
				$productID,
				$quantity,
				Array(),
				$arMods
			);
		} else {
			$basketResult = Add2BasketByProductID(
				$productID,
				$quantity,
				Array()
			);
		}

		if ($basketResult) {
			$APPLICATION->IncludeComponent("pronto24:basket.line", "json");
			$result = Array('result' => 'ok', 'id' => $basketResult, 'basket' => $_SESSION['basket_line']);
		}
	}
}

return $result;