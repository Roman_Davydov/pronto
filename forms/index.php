<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;

if($USER->IsAuthorized() && isset($_GET['action']) && $_GET['action'] == 'authorization') {
	LocalRedirect('/');
}

if (isset($_GET['action']) && empty($_GET['auth_service_id'])) {
	$APPLICATION->IncludeComponent("pronto24:forms", $_GET['action']);
	return false;
}

if(isset($_GET['auth_service_id'])) {
	$APPLICATION->IncludeComponent("bitrix:system.auth.authorize");
	return false;
}