<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$dir = '/home/wwwpronto2/www.pronto24.ru/exchange/';
//$doubleDir = '/home/wwwpronto2/www.pronto24.ru/xml_log/';
$idOrder = $arParams['ORDER'];
if(is_numeric($idOrder)) {
	if(!is_dir($dir)) mkdir($dir);
	CModule::IncludeModule('sale');
	$order = CSaleOrder::GetByID($idOrder);
	$arOrder = $APPLICATION->IncludeComponent("pronto24:jupiter.order.element", '.default', Array('ORDER' => $order));

	$arFullOrder = Array('service' => Array('order' => $arOrder, '@attributes' => Array('action' => 'orders', 'source' => 4, 'id' => $idOrder)));

	$xml = Array2XML::createXML('root', $arFullOrder);
	$contents = $xml->saveXML();
	if(!file_exists("$dir/order_$idOrder.xml"))
		file_put_contents("$dir/order_$idOrder.xml", $contents);
	if(!file_exists("$doubleDir/order_$idOrder.xml"))
		file_put_contents("$doubleDir/order_$idOrder.xml", $contents);
}