<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');

$arProperties = Array('PROPERTY_BANNER_PICTURE', 'NAME', 'CODE', 'PROPERTY_LINK', 'BANNER_PICTURE');
$res = CIBlockElement::GetList(
	Array("SORT" => "ASC"),
	Array("IBLOCK_ID" => 13, "ACTIVE" => "Y"),
	false,
	false,
	Array('NAME', "CODE", "PROPERTY_COLOR")
);

while ($arElement = $res->GetNext()) {
	$arResult['ELEMENTS'][$arElement['CODE']] = Array('NAME' => $arElement['NAME']);

	if (substr($APPLICATION->GetCurPage(), 1, strlen($arElement['CODE'])) ==$arElement['CODE']) {
		$arResult['ELEMENTS'][$arElement['CODE']]['SELECTED'] = TRUE;
		$arResult['CURRENT_SECTION_COLOR'] = $arElement['PROPERTY_COLOR_VALUE'];
	}
}

$this->IncludeComponentTemplate();