<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$arProperties = Array(
	'ID',
	'CODE',
	'DETAIL_PICTURE',
	'IBLOCK_ID',
	'NAME',
	'PROPERTY_HTML_TITLE'
);

$arFilters = Array("IBLOCK_CODE" => 'pizza', 'PROPERTY_B_CONSTRUCTOR_VALUE' => "Y");

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilters, false, false, $arProperties);

while ($obElement = $res->GetNextElement()) {
	$arElement = $obElement->fields;
	$arProps = $obElement->GetProperties();
	foreach($arProps as $key => $property) {
		$arElement[$key] = $property['VALUE'];
	}

	$picture = CFile::GetFileArray($arElement["DETAIL_PICTURE"]);
	$arElement["PICTURE"] = $picture['SRC'];

	$picture = CFile::GetFileArray($arElement["MORE_PICTURES"][0]);
	$arElement["MORE_PICTURES"] = $picture['SRC'];
	$arElement['PRICE'] = getPrice($arElement['ID']);
	$arResult[] = $arElement;
}

$this->IncludeComponentTemplate();