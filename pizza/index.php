<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Пицца");
?>
	<div class="content">
		<div class="wrapper index product pizza-style">
			<div class="intro">
				Пицца
			</div>
			<div class="sort">
				<a href="/pizza/" <?= empty($_GET)?"class='active'":"" ?>>все</a>
				<a href="/pizza/?meat" <?= isset($_GET['meat'])?"class='active'":"" ?>>мясные</a>
				<a href="/pizza/?seafood" <?= isset($_GET['seafood'])?"class='active'":"" ?>>с морепродуктами</a>
				<a href="/pizza/?vegeterian" <?= isset($_GET['vegeterian'])?"class='active'":"" ?>>вегетарианские</a>
				<a href="/pizza/?sets" <?= isset($_GET['sets'])?"class='active'":"" ?>>пицца-сеты</a>
			</div>
			<? $APPLICATION->IncludeComponent("pronto24:catalog.section", '.default', Array('IBLOCK_ID' => 5)); ?>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>