<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");
//сюда приходят все ajax-запросы. в соответствии с action-переменной запускается нужный компонент
if (isset($_POST['action'])) {
	switch($_POST['action']) {
		case 'auth':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.auth");
			break;
		case 'registration':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.registration");
			break;
		case 'basketadd':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.basket.add");
			break;
		case 'basketdel':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.basket.del");
			break;
		case 'basketupdate':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.basket.update");
			break;
		case 'basketcoupon':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.basket.coupon");
			break;
		case 'callme':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.callme");
			break;
		case 'changePersonals':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.change.personals");
			break;
		case 'changePassword':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.change.password");
			break;
		case 'changeDiscountCard':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.change.card");
			break;
		case 'locationUpdate':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.location.update");
			break;
		case 'subscribe':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.subscribe");
			break;
		case 'orderDetails':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.order.details");
			break;
		case 'deleteOrder':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.order.delete");
			break;
		case 'repeatOrder':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.order.repeat");
			break;
		case 'getLastOrder':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.order.last");
			break;
		case 'ajaxRecover':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.password.recover");
			break;
		case 'chooseGift':
			$result = $APPLICATION->IncludeComponent("pronto24:ajax.gift.choose");
			break;
	}
	echo json_encode($result);
}