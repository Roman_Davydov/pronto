'use strict';
$(document).ready(function () {
	MainManager = new MainManager_f();
	MainManager.init();
	MainManager.initHeight();
	$(window).resize(function () {
		MainManager.initHeight()
	});
	if(typeof RestaurantManager_f =='function') {
		RestaurantManager = new RestaurantManager_f();
		RestaurantManager.init();
	}

	if(typeof ProductManager_f =='function') {
		ProductManager = new ProductManager_f();
		ProductManager.init();
	}

	if(typeof PersonalManager_f =='function') {
		PersonalManager = new PersonalManager_f();
		PersonalManager.init();
	}

	if(typeof LocationManager_f =='function') {
		LocationManager = new LocationManager_f();
		LocationManager.init();
	}

	if(typeof ConstructorManager_f =='function') {
		ConstructorManager = new ConstructorManager_f();
		ConstructorManager.init();
	}
	if(typeof DeliveryManager_f =='function') {
		DeliveryManager = new DeliveryManager_f();
		DeliveryManager.init();
	}
});
