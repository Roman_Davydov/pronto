<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');

$res = CIBlockElement::GetByID($arParams["ID"]);

$obElement = $res->GetNextElement();
$arResult = Array();
if(is_numeric($obElement->fields['ID'])) {
	$arResult['ID'] = $obElement->fields['ID'];
	$arResult['NAME'] = $obElement->fields['NAME'];
	$arResult['CODE'] = $obElement->fields['CODE'];
	$arResult['IBLOCK_CODE'] = $obElement->fields['IBLOCK_CODE'];
	$props = $obElement->GetProperties();
	foreach ($props as $key => $property) {
		if (isset($property['VALUE'])) {
			$arResult[$key] = $property['VALUE'];
		}
	}
	$picture = CFile::GetFileArray($obElement->fields["DETAIL_PICTURE"]);
	$arResult['SRC'] = $picture['SRC'];
	$arResult['PRICE'] = getPrice($arResult['ID']);
}

$this->IncludeComponentTemplate();