<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');
CModule::IncludeModule('subscribe');

global $USER;
$userID = $USER->getID();
$params = $USER->GetByID($userID);
$arResult = $params->arResult[0];
$arResult['EMAIL'] = $USER->GetEmail();
$arEmailSplited = explode('@', $arResult['EMAIL']);

if($arEmailSplited['1'] == 'pronto24.ru') {
	$arResult['EMAIL'] = '';
}

//Subscription
$arResult['SUBSCRIPTION'] = false;
$subscr = CSubscription::GetList(
	array("ID" => "ASC"),
	array("ACTIVE" => "Y", "USER_ID" => $userID)
);
$subscr_arr = $subscr->Fetch();

if($subscr_arr) {
	$arResult['SUBSCRIPTION'] = $subscr_arr['EMAIL'];
}

//discount card
$arFilter = Array(
	"IBLOCK_ID" => 28,
	"ACTIVE" => "Y",
	"PROPERTY_USER" => $userID
);

$arProperties = Array('NAME', 'PROPERTY_USER');
$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arProperties);

while ($arElement = $res->GetNext()) {
	$arResult['CARD'] = $arElement['NAME'];
}

$APPLICATION->IncludeComponent("pronto24:location.list");

$this->IncludeComponentTemplate();
?>