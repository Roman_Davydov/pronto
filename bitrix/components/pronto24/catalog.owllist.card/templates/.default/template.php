<div data-product-card class="item">
	<a href="/<?= $arResult['IBLOCK_CODE'] ?>/<?=$arResult['CODE']?>/" class="item-link">
		<img src="<?= $arResult['SRC'] ?>" alt="">
		<div class="name" data-height><?= $arResult['NAME'] ?></div>
	</a>
	<div class="btn-group number-group">
		<button data-btn-number="" type="button" class="btn border btn-number" data-type="minus"></button>
		<input data-input-number="" type="text" name="" class="btn border input-number" value="1" min="1" readonly="">
		<button data-btn-number="" type="button" class="btn border btn-number" data-type="plus"></button>
	</div>
	<div class="rub"><?= $arResult['PRICE'] ?> руб.</div>

	<button data-product-id="<?= $arResult['ID'] ?>" data-cnt="1" class="btn pink small basket-add">
		в корзину
	</button>
</div>