<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('sale');

$orderId = isset($_POST['orderId']) ? (int) $_POST['orderId'] : false;

if ($orderId && CSaleOrder::StatusOrder($orderId, "UD"))
	return Array('result' => 'ok');
else
	return Array('result' => 'error');
?>