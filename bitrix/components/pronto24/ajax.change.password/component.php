<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

global $USER;

$password = isset($_POST['password']) ? $_POST['password'] : false;
$newPassword = isset($_POST['newPassword']) ? $_POST['newPassword'] : false;

$result = array('result' => 'error');

if($password && $newPassword && strlen($newPassword) > 5) {
	$login = $USER->GetLogin();
	$check = $USER->Login($login, $password);
	if($check === true) {
		$fields = Array(
			"PASSWORD"          => $newPassword,
			"CONFIRM_PASSWORD"  => $newPassword,
		);
		$result = $USER->Update($USER->GetID(), $fields);
		if($USER->LAST_ERROR == '') {
			$result = array('result' => 'ok');
		}
	}
}

return $result;