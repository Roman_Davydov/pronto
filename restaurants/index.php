<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');
$APPLICATION->SetTitle("Рестораны");
?>
	<div class="content restaurant all">
		<div class="wrapper">
			<h2 class="big">Рестораны</h2>

			<div class="sort sub m-b30">
				<a href="/restaurants/?moscow" <?= isset($_GET['moscow']) ? "class='active'" : "" ?>>Москва</a>
				<a href="/restaurants/?zelenograd" <?= isset($_GET['zelenograd']) ? "class='active'" : "" ?>>зеленоград</a>
				<a href="/restaurants/?mosobl" <?= isset($_GET['mosobl']) ? "class='active'" : "" ?>>московская	область</a>
				<a href="/restaurants/?russia" <?= isset($_GET['russia']) ? "class='active'" : "" ?>>Россия</a>
			</div>

			<div class="filters _txt-right m-b10 set-pills">
				<button data-children class="btn border m-r4">pronto kids</button>
				<button data-music class="btn border m-r4">живая музыка</button>
				<button data-lunch class="btn border m-r4">бизнес-ланч</button>
				<button data-banquet class="btn border m-r4">банкеты</button>
				<button data-all class="btn border active">все</button>
			</div>

			<div class="row">
				<div class="col col_3">
					<? $APPLICATION->IncludeComponent("pronto24:restaurants.list"); ?>
				</div>
				<div class="col col_3c">
					<div id="map" class="map"></div>
				</div>
			</div>

			<div class="img-bg"></div>

			<div class="_txt-center">
				<p>Первое заведение нашей популярной сети было открыто в 2000 году. С тех пор компания непрерывно
					развивается, и сегодня включает в себя более 40 ресторанов лучшей итальянской кухни.</p>
				<p>Классическое меню предлагает большой выбор пиццы, свежих паст, салатов, а также традиционных
					итальянских блюд из рыбы и мяса.</p>
				<p>В сети пиццерий «Пронто» есть большая винная карта, банкетное меню, стильная музыка, wi-fi и все,
					что нужно для полноценного отдыха. Детское меню для удобства посетителей вынесено
					в специальный раздел, цены приятно удивляют.</p>
				<p>Мы позаботились и о комфортном отдыхе самых маленьких гостей,
					у нас есть детские стульчики и многие другие полезные мелочи для малышей (раскраски, карандаши,
					воздушные шарики). В некоторых кафе имеются детские игровые комнаты.
				</p>
				<p>Сеть пиццерий «Пронто» предлагает организацию детских праздников и дней рождения. В перечне работ
					есть и услуги аниматоров. Они умеют занять детей разных возрастов, всегда готовы устроить конкурс,
					завести хоровод, сплясать с детьми.
				</p>
			</div>

		</div>

	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>