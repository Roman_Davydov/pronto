<div class="item">
	<a href="#" class="product-link">
		<img src="<?= $arResult['SRC'] ?>" alt="">
		<?= !empty($arResult['~HTML_TITLE']) ? html_entity_decode($arResult['~HTML_TITLE']) : '<span class="title">'.$arResult['NAME'].'</span>' ?>
	</a>
	<div class="text-intro" data-height><?= $arResult['DESCRIPTION']; ?></div>
	<div class="rub"><?= $arResult['PRICE'] ?> руб.</div>
</div>