<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');

$arBasketItems = Array();

$arItems = Array();
if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
	$arResult['NUM_PRODUCTS'] = 0;
	$arResult['TOTAL_PRICE'] = 0;
	$dbBasketItems = CSaleBasket::GetList(
		array(
			"NAME" => "ASC"
		),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
		false,
		false,
		array("ID", "PRODUCT_ID", "QUANTITY", "PRICE", "QUANTITY", "DISCOUNT_PRICE")
	);
	$arBasketItems = Array();
	while ($arItem = $dbBasketItems->Fetch()) {
		$res = CIBlockElement::GetList(Array(), Array('ID' => $arItem['PRODUCT_ID']), false, false, Array('ID', 'IBLOCK_ID'));
		$arElement = $res->GetNext();
		$arItem['PRICE'] = getPrice($arElement['ID']);
		$arItem['IBLOCK_ID'] = $arElement['IBLOCK_ID'];

		if($arItem['IBLOCK_ID'] != 26) {
			$arResult['NUM_PRODUCTS']++;
		}
		$arResult['TOTAL_PRICE'] += $arItem['PRICE'] * $arItem['QUANTITY'] - $arItem['DISCOUNT_PRICE'] * $arItem['QUANTITY'];
	}
	$arResult['TOTAL_PRICE'] = round($arResult['TOTAL_PRICE']);
	$arResult['TOTAL_CLEAR_PRICE'] = $arResult['TOTAL_PRICE'];
	$arResult['TOTAL_PRICE'] .= ' руб.';
	$lastChar = substr($arResult['NUM_PRODUCTS'], -1);
	$lastTwoChars = substr($arResult['NUM_PRODUCTS'], -2);

	if($lastChar == 1 && $arResult['NUM_PRODUCTS'] != 11) {
		$arResult['PRODUCT(S)'] = 'товар';
	}
	elseif($lastChar > 1 && $lastChar < 5 && $lastTwoChars < 10 && $lastTwoChars > 21) {
		$arResult['PRODUCT(S)'] = 'товара';
	}
	else {
		$arResult['PRODUCT(S)'] = 'товаров';
	}

	$this->IncludeComponentTemplate();
}