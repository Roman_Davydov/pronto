<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$arResult['menu'] = Array(
	"/about/" => "о компании",
	"/about/menu/" => "меню ресторанов",
	"/about/partners/" => "партнерам",
	/*"/about/discount/" => "дисконтные карты",*/
	"/about/reviews/" => "отзывы",
	"/about/vacancies/" => "вакансии",
	"/about/contacts/" => "контакты"
);

$this->IncludeComponentTemplate();