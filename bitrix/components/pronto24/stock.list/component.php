<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');

$bMain = isset($arParams['BMAIN']) && $arParams['BMAIN'] ? true : false;
if ($bMain) {
	$now = date('d.m.Y H:i:s');

	$arFilter = Array(
		"IBLOCK_ID" => 8,
		"ACTIVE" => "Y",
		Array(
			"LOGIC" => "OR",
			Array("<=DATE_ACTIVE_FROM" => $now),
			Array("DATE_ACTIVE_FROM" => false),
		),
		Array(
			"LOGIC" => "OR",
			Array(">=DATE_ACTIVE_TO" => $now),
			Array("DATE_ACTIVE_TO" => false),
		)
	);
} else
	$arFilter = Array("IBLOCK_ID" => 8);

if (isset($arParams['STOCKS'])) {
	$arFilter['ID'] = $arParams['STOCKS'];
}

$arProperties = Array('PROPERTY_STOCK_PICTURE', 'CODE', 'PROPERTY_DESCRIPTION', 'NAME', 'CODE', 'PROPERTY_LINK', 'STOCK_PICTURE');
$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arProperties);

while ($arElement = $res->GetNext()) {
	$picture = CFile::GetFileArray($arElement["PROPERTY_STOCK_PICTURE_VALUE"]);
	$arr = Array();
	$arr['LINK'] = $arElement['PROPERTY_LINK_VALUE'];
	$arr['SRC'] = $picture['SRC'];
	$arr['ALT'] = $arElement['NAME'];
	$arr['TEXT'] = $arElement['PROPERTY_DESCRIPTION_VALUE']['TEXT'];
	$arResult[$arElement['CODE']] = $arr;
}

$this->IncludeComponentTemplate();