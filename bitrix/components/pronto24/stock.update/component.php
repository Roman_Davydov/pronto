<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$result = Array();

if(CModule::IncludeModule("iblock") && CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {

	$arFilter = Array(
		"IBLOCK_ID" => 34,
		"ACTIVE" => "Y",
		"<=PROPERTY_MIN_SUM" => $arParams['TOTAL_PRICE']
	);

	$arProperties = Array('ID', 'NAME', 'IBLOCK_ID');
	$res = CIBlockElement::GetList(Array("PROPERTY_MIN_SUM" => "DESC"), $arFilter, false, false, $arProperties);

	$obElement = $res->GetNextElement();
	if(is_numeric($obElement->fields['ID'])) {
		$giftName = $obElement->fields['NAME'];
		$props = $obElement->GetProperties();
		foreach($props as $key => $property) {
			if(isset($property['VALUE']) && $key == 'GIFT') {
				//Установка нужного подарка
				$bFirst = true;
				$arGifts = Array();
				foreach($property['VALUE'] as $gift) {
					$arGift = Array();
					$res = CIBlockElement::GetList(Array(), Array('ID' => $gift), false, false, Array('ID', 'NAME', 'IBLOCK_ID', 'DETAIL_PAGE_URL', 'DETAIL_PICTURE', 'PROPERTY_LINK_TO', 'PROPERTY_PICTURE', 'PROPERTY_SIZE'));
					$arElement = $res->GetNext();
					$arGift['CHOOSED'] = empty($_SESSION['CHOOSED_GIFT']) ? $bFirst : ($_SESSION['CHOOSED_GIFT'] == $gift);
					if(empty($_SESSION['CHOOSED_GIFT'])) {
						$_SESSION['CHOOSED_GIFT'] = $gift;
					}
					$bFirst = false;
					$picture = CFile::GetFileArray($arElement["DETAIL_PICTURE"]);
					$arGift['DETAIL_PICTURE'] = $picture['SRC'];
					$arGift['NAME'] = $arElement['NAME'];
					$arGift['ID'] = $arElement['ID'];
					$arGifts[] = $arGift;
				}
				$result['stock'] = Array('name' => $giftName, 'gifts' => $arGifts);
			}
		}
	}
	else {
		unset($_SESSION['CHOOSED_GIFT']);
	}
}

return $result;