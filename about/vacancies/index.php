<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Работа в пронто");

if (isset($_POST) && !empty($_POST)) {
	$APPLICATION->IncludeComponent("pronto24:vacancy.send");
}
?>
	<div class="content about-wr">
		<div class="wrapper index about">

			<h1 class="title">пронто</h1>
			<h2 class="big for-mobile">Вакансии</h2>

			<div class="sort m-b30">
				<? $APPLICATION->IncludeComponent("pronto24:menu.about"); ?>
			</div>

			<!--div class="sort sub m-b40">
				<a href="#" class="active">Москва</a>
				<a href="#">зеленоград</a>
				<a href="#">московская область</a>
				<a href="#">россия</a>
			</div-->

			<? /*$APPLICATION->IncludeComponent("pronto24:vacations.table")*/ ?>

			<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "page",
					"AREA_FILE_SUFFIX" => "vacancies"
				)
			); ?>

			<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "page",
					"AREA_FILE_SUFFIX" => "vacancy_text"
				)
			); ?>

			<form enctype="multipart/form-data" method="POST" action="/about/vacancies/">
				<div class="content-form vacancies-form">
					<h2 class="m-b30">Написать нам</h2>
					<div class="row">
						<div class="col col_2">
							<div class="form-grid">
								<div class="form-row">
									<div class="form-cell">имя</div>
									<div class="form-cell">
										<div class="input-data">
											<input name="name" type="text">
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="form-cell">телефон</div>
									<div class="form-cell _txt-right">
										<div class="input-data">
											<input name="phone" data-phone type="text">
										</div>
									</div>
								</div>
								<div class="form-row">
									<div class="form-cell">вакансия</div>
									<div class="form-cell">
										<div class="input-data">
											<select name="profession" data-btn-select class="selectpicker">
												<? $APPLICATION->IncludeComponent("pronto24:vacations.list") ?>
											</select>
										</div>
									</div>
								</div>
							</div>

							<input type="file" class="file-input" id="vacancies-file" name="file" data-input-file
							       onchange="MainManager.getFileName(this)">
							<label for="vacancies-file">прикрепить резюме</label>
							<ul class="ul-file" data-list-vacancies-file></ul>

						</div>
						<div class="col col_2">
							<div class="textarea-box">
								<div class="input-data">
									<textarea name="text"></textarea>
								</div>
								<div class="form-label">ваш комментарий</div>
							</div>
						</div>
					</div>
					<div class="_txt-center m-t10">
						<button type="submit" class="btn pink big">Отправить</button>
					</div>
				</div>
			</form>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>