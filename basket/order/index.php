<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Оформить заказ");

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addCss('https://cdn.jsdelivr.net/jquery.suggestions/16.8/css/suggestions.css');
Asset::getInstance()->addJs('https://cdn.jsdelivr.net/jquery.suggestions/16.8/js/jquery.suggestions.min.js');

$APPLICATION->IncludeComponent('pronto24:restaurants.parser');
$APPLICATION->IncludeComponent('pronto24:basket.order');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>