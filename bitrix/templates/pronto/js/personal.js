'use strict';

function PersonalManager_f() {

	this.init = function () {
		$(document)
			.on("click", ".changePersonals", PersonalManager.changePersonals)
			.on("click", ".changePassword", PersonalManager.changePassword)
			.on("click", ".discountCard", PersonalManager.discountCard)
			.on("click", ".discountCardChange", PersonalManager.discountCardChange)
			.on("click", "[data-subscribe]", PersonalManager.subscribe)
			.on("click", "[data-unsubscribe]", PersonalManager.unsubscribe)
			.on("click", "[data-paginate-orders]", PersonalManager.paginatePage)
			.on("click", "[data-delete-order]", PersonalManager.deleteOrder)
			.on("click", "[data-repeat-order]", PersonalManager.repeatOrder);
		$('.order-list tr.order:not(.tr1)').hide();
	};

	this.deleteOrder = function() {
		var orderId = $(this).attr("data-delete-order");
		$.post(
			"/ajax/",
			{action: 'deleteOrder', orderId: orderId},
			function (data) {
				if (data.result == 'ok') {
					$("[data-delete-order="+orderId+"]").closest('tr').remove();
					return false;
				}
			}, "json");
	};

	this.repeatOrder = function() {
		var orderId = $(this).attr("data-repeat-order");
		$.post(
			"/ajax/",
			{action: 'repeatOrder', orderId: orderId},
			function (data) {
				if (data.result == 'ok') {
					ProductManager.updateBasket(data.basket);
					document.location.href = '/basket/';
					return false;
				}
			}, "json");
	};

	this.paginatePage = function() {
		$('.paginate a').removeClass('active');
		$(this).addClass('active');
		$('.order-list tr.order:not(.tr'+$(this).text()+')').hide();
		$('.order-list tr.order.tr'+$(this).text()).show();
		return false;
	};

	this.subscribe = function() {
		$.post(
			"/ajax/",
			{action: 'subscribe', subscribe: true},
			function (data) {
				if (data.result == 'ok') {
					$(".subscribe .activate-box").toggleClass('true');
					$(".email-subscr").text(data.email);
					return false;
				}
			}, "json");
	};

	this.unsubscribe = function() {
		$.post(
			"/ajax/",
			{action: 'subscribe'},
			function (data) {
				if (data.result == 'ok') {
					$(".subscribe .activate-box").toggleClass('true');
					return false;
				}
			}, "json");
	};

	this.discountCardChange = function() {
		var card = $('[data-card-number]').val();
		$.post(
			"/ajax/",
			{"action": 'changeDiscountCard', card: card},
			function (data) {
				if (data.result == 'ok') {
					PersonalManager.staticActivate();
					return false;
				}
			}, "json");
	};

	this.discountCard = function() {
		PersonalManager.staticActivate();
	};

	this.staticActivate = function () {
		$('[data-activate-input]').toggleClass('true');
		$('[data-activate-box]').toggleClass('true');
	};

	this.changePersonals = function () {
		var name = $('[data-name]').val();
		var lastName = $('[data-last-name]').val();
		var email = $('[data-email]').val();
		var birthday = $('[data-birthday]').val();
		$.post(
			"/ajax/",
			{action: 'changePersonals', name: name, lastName: lastName, email: email, birthday: birthday},
			function (data) {
				if (data.result == 'ok') {
					MainManager.showTextAlert('Личные данные сохранены');
					return false;
				}
				else {
					if(data.type == 'birthday') {
						MainManager.showTextAlert('Не корректный формат Дня Рождения. Должен быть ДД.ММ.ГГГГ');
					}
				}
			}, "json");
	};

	this.changePassword = function() {
		var password = $('[data-password]').val();
		var newPassword = $('[data-new-password]').val();
		$.post(
			"/ajax/",
			{action: 'changePassword', password: password, newPassword: newPassword},
			function (data) {
				if (data.result == 'ok') {
					MainManager.showTextAlert('Пароль отправлен на телефон');
					return false;
				}
				else {

				}
			}, "json");
	};
}