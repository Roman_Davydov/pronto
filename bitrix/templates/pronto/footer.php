<?if(!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED!==true) die();?>
</div>

<div class="footer">
	<div class="wrapper">
		<div class="row-col row">
			<div class="col col_4">
				<a href="/" class="logo"></a>
			</div>
			<div class="col col_2">
				<div class="money">
					<a href="https://money.yandex.ru/" target="_blank" class="mon-icon-s_1"></a>
					<a href="https://www.webmoney.ru/" target="_blank" class="mon-icon-s_2"></a>
					<a href="http://www.visa.com.ru/" target="_blank" class="mon-icon-s_3"></a>
					<a href="https://www.mastercard.ru/" target="_blank" class="mon-icon-s_4"></a>
					<a href="https://qiwi.com/" target="_blank" class="mon-icon-s_5"></a>
				</div>
				<div class="social">
					<a href="https://vk.com/pizzapronto" target="_blank" class="soc-icon-s_5"></a>
					<a href="https://www.facebook.com/prontopizzacafe" target="_blank" class="soc-icon-s_6"></a>
					<a href="https://ok.ru/prontopizza" target="_blank" class="soc-icon-s_7"></a>
					<a href="https://www.instagram.com/pronto_pizza_/" target="_blank" class="soc-icon-s_8"></a>
				</div>
			</div>
			<div class="col col_4">
				<div class="phone"><a href="tel:+74955055757">+7 495</a>/<a href="tel:+74995055757">499 <span>505-57-57</span></a></div>
				<button onClick="MainManager.showModal('call')" class="we-call btn black">заказать звонок</button>
			</div>
		</div>

		<? $APPLICATION->IncludeComponent("pronto24:menu.main");?>

		<div class="copy row">
                <span>
                    <a href="#" class="active">десктопная версия</a> | <a href="#">мобильная версия</a>
                </span>
                <span>
                    © 1999–<?= (date('Y')); ?> ООО «АВЕНО»
                </span>
		</div>
	</div>
</div>

<!-- Modal global-->
<div class="modal fade" id="modal" role="dialog" aria-hidden="true">
	<div class="modal-dialog">
		<button class="close-icon" data-dismiss="modal" aria-label="Close"></button>
		<div class="modal-content"></div>
	</div>
</div>

<!-- Modal basket -->
<div class="modal fade" id="basket">
	<div class="modal-dialog basket-dialog small">
		<div class="modal-content">
			<button class="close-icon" data-dismiss="modal" aria-label="Close"></button>
			<h1>Ваша корзина пуста</h1>
		</div>
	</div>
</div>

<div data-alert class="alert info">
	<div class="wrapper" data-alert-text></div>
</div>

<div data-alert-by class="alert by">
	<div class="wrapper small">
		<img data-widget-img src="" alt="">
		<span data-widget-text></span>
		<button class="close-icon" onclick="MainManager.hideAlertProduct()"></button>
		<a data-widget-href href="#">Смотреть</a>
	</div>
</div>

</body>
</html>