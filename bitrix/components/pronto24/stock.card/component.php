<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');

if(isset($_GET['code'])) {
	$CODE = str_replace('/', '', $_GET['code']);
	$arFilter = Array("IBLOCK_ID" => 8, "CODE" => $CODE);
}

$arProperties = Array('PROPERTY_STOCK_PICTURE', 'CODE', 'PROPERTY_DESCRIPTION', 'NAME', 'PROPERTY_LINK');
$res = CIBlockElement::GetList(Array(), $arFilter, false, false, $arProperties);

while ($arElement = $res->GetNext()) {
	$picture = CFile::GetFileArray($arElement["PROPERTY_STOCK_PICTURE_VALUE"]);
	$arr = Array();
	$arr['LINK'] = $arElement['PROPERTY_LINK_VALUE'];
	$arr['SRC'] = $picture['SRC'];
	$arr['ALT'] = $arElement['NAME'];
	$arr['TEXT'] = $arElement['PROPERTY_DESCRIPTION_VALUE']['TEXT'];
	$arResult = $arr;
}

$this->IncludeComponentTemplate();