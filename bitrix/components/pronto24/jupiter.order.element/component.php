<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('sale');
CModule::IncludeModule('iblock');

$rsBasket = CSaleBasket::GetList(array(
	"NAME" => "ASC"
),
	array(
		"LID" => SITE_ID,
		"ORDER_ID" => $arParams['ORDER']['ID']
	),
	false,
	false,
	array("ID", "NAME", "QUANTITY", "PRODUCT_ID", 'PRICE')
);

$arJupiterProducts = Array();
$arBasket = Array();
while ($arItem = $rsBasket->Fetch()) {
	$arJupiterItem = Array();
	$arJupiterItem['ID'] = $arItem['ID'];
	$arJupiterItem['NAME'] = $arItem['NAME'];
	$arJupiterItem['QUANTITY'] = $arItem['QUANTITY'];
	$arJupiterItem['PRICE'] = $arItem['PRICE'];
	$res = CIBlockElement::GetList(Array(), Array('ID' => $arItem['PRODUCT_ID']), false, false, Array('PROPERTY_OUTER_ID', 'IBLOCK_ID'));
	$arElement = $res->GetNextElement();
	$arJupiterItem['PRODUCT_ID'] = $arElement->fields['PROPERTY_OUTER_ID_VALUE'];

	if ($arElement->fields['IBLOCK_ID'] != 26) {
		$db_res = CSaleBasket::GetPropsList(
			array(
				"SORT" => "ASC",
				"NAME" => "ASC"
			),
			array("BASKET_ID" => $arItem['ID'])
		);
		while ($ar_res = $db_res->Fetch()) {
			if ($ar_res['NAME'] == 'MODIFICATOR' && !empty($ar_res['CODE'])) {
				$arJupiterItem['MODIFICATORS'][] = $ar_res;
			}
		}
		$arJupiterItem['MODIFICATOR'] = false;
	} else {
		$arJupiterItem['MODIFICATOR'] = true;
	}
	$arBasket[$arItem['ID']] = $arJupiterItem;
}

$arBasketClear = Array();
foreach ($arBasket as $item) {
	if ($item['MODIFICATOR'] != 1) {
		$arBasketClear[] = $item;
		if (isset($item['MODIFICATORS'])) {
			foreach ($item['MODIFICATORS'] as $modificator) {
				$newItem = $arBasket[$modificator['CODE']];
				$newItem['ID'] = $item['ID'];
				$newItem['QUANTITY'] = $modificator['VALUE'];
				$arBasketClear[] = $newItem;
			}
		}
	}
}

foreach ($arBasketClear as $item) {
	$newItem = Array();
	$newItem['name'] = $item['NAME'];
	$newItem['product_id'] = $item['PRODUCT_ID'];
	if ($item['MODIFICATOR']) {
		$newItem['product_parent_id'] = $arBasket[$item['ID']]['PRODUCT_ID'];
	}

	$newItem['price'] = (int)$item['PRICE'];
	$newItem['quantity'] = $item['QUANTITY'];
	$arJupiterProducts[] = $newItem;
}

$arBasket = $arBasketClear;

$db_props = CSaleOrderPropsValue::GetList(
	array("DATE_UPDATE" => "DESC"),
	array("ORDER_ID" => $arParams['ORDER']['ID'])
);

$arAddress = Array();

while ($props = $db_props->Fetch()) {
	switch ($props['CODE']) {
		case 'PHONE':
			$phone = 8 . $props['VALUE'];
			break;
		case 'NAME':
			$name = $props['VALUE'];
			break;
		case 'RESTORANT_ID':
			$restaurantID = $props['VALUE'];
			break;
		case 'ZONE':
			$zoneID = $props['VALUE'];
			break;
		case 'ADDRESS':
			$address = $props['VALUE'];
			break;
		case 'SECTION':
			$section = $props['VALUE'];
			break;
		case 'STAGE':
			$stage = $props['VALUE'];
			break;
		case 'FLAT':
			$flat = $props['VALUE'];
			break;
		case 'COMMENT':
			$comment = $props['VALUE'];
			break;
		case 'CHANGE':
			$change = $props['VALUE'];
			break;
		case 'GIFT':
			$gift = $props['VALUE'];
			break;
	}
}

if ($gift) {
	if (is_numeric($gift)) {
		$res = CIBlockElement::GetByID($gift);
		$arGift = $res->GetNext();
		$res = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $arGift["IBLOCK_ID"], 'ID' => $gift), false, false, Array("NAME", "PROPERTY_OUTER_ID"));
		$result = $res->GetNext();
		$JupiterGift = Array("name" => $arGift['NAME'], 'product_id' => $result['PROPERTY_OUTER_ID_VALUE'], 'price' => '0', 'quantity' => 1);
		$arJupiterProducts[] = $JupiterGift;
	}
}

$arAddress = Array(
	'city' => $address,
	'entrance' => $section,
	'floor' => $stage,
	'room' => $flat
);

if (!empty($arParams['ORDER']['USER_NAME']))
	$name = $arParams['ORDER']['USER_LAST_NAME'] . ' ' . $arParams['ORDER']['USER_NAME'];

$props = CSaleOrder::GetById($arParams['ORDER']['ID']);

$arPayment = Array();

if ($props['PAY_SYSTEM_ID'] == 2) {
	$arPayment['payment_id'] = 1;
} else {
	$systems = new CSalePaySystem;
	$arPaySys = $systems->GetByID($props['PAY_SYSTEM_ID']);
	$arExecuter = explode('/', $arPaySys['ACTION_FILE']);
	$exec = array_pop($arExecuter);
	if ($exec == 'platron') {
		$arPayment['payment_id'] = 3;
	} elseif ($exec == 'sbr') {
		$arPayment['payment_id'] = 2;
	}
}
if ($props['PAYED'] == 'Y') {
	$arPayment['payment_status'] = 2;
} else {
	$arPayment['payment_status'] = 1;
}
$arPayment['payment_sum'] = $arParams['ORDER']['PRICE'];

if ($arParams['ORDER']['REASON_MARKED'] == 'NOT_LOADED_TO_JUPITER') {
	$arParams['ORDER']['REASON_MARKED'] = '';
}
return Array(
	'@attributes' => Array('id' => $arParams['ORDER']['ID']),
	'client_id' => $arParams['ORDER']['USER_ID'],
	'client_full_name' => $name,
	'client_address' => $arAddress,
	'restaurant_id' => $restaurantID,
	'restaurant_zone_id' => $zoneID,
	'phone' => $phone,
	'client_comment' => $comment,
	'sum' => $arParams['ORDER']['PRICE'],
	'change_sum' => $change,
	'items' => Array('item' => $arJupiterProducts),
	'payment' => $arPayment,
	'key' => $arParams['ORDER']['REASON_MARKED']
);