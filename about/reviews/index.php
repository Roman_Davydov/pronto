<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Отзывы");

if (isset($_POST) && !empty($_POST)) {
	$APPLICATION->IncludeComponent("pronto24:review.add");
}
?>
	<div class="content page about-wr">
		<div class="wrapper index about">

			<h1 class="title">пронто</h1>
			<h2 class="big for-mobile">Отзывы</h2>

			<div class="sort m-b30">
				<? $APPLICATION->IncludeComponent("pronto24:menu.about"); ?>
			</div>

			<div class="_txt-center m-b30">
				<button class="btn pink big reviews-btn" onclick="MainManager.showModal('review')">написать отзыв
				</button>
			</div>

			<? $APPLICATION->IncludeComponent("pronto24:reviews.list", "", Array("ONPAGE" => 10)) ?>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>