<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arResult = false;

CModule::IncludeModule('iblock');

$arProperties = Array(
	'ID',
	'CODE',
	'IBLOCK_ID',
	'PROPERTY_WORKING_TIME_DELIVERY'
);

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 3, 'PROPERTY_JUPITER_ID' => $arParams['ID']), false, false, $arProperties);

$obElement = $res->GetNextElement();
$props = $obElement->GetProperties();

$yesterday = date('w') == 0 ? 6 : date('w') - 1;

$arSpl = explode('-', $props['WORKING_TIME_DELIVERY']['VALUE'][$yesterday]);

$timeBegin = strtotime(date('Y-m-d ') . $arSpl[0] . ":00");
$timeEnd = strtotime(date('Y-m-d ') . $arSpl[1] . ":00");

if ($timeEnd < $timeBegin) {
	$timeEnd = $timeEnd + (60 * 60 * 24);
}

$now = strtotime(date('Y-m-d H:i:s'));

if ($timeBegin < $now && $now < $timeEnd) {
	$arResult = true;
}

//Additional
CModule::IncludeModule('iblock');

$arSelect = Array(
	'ID',
	'IBLOCK_ID',
	'NAME'
);

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_CODE" => 'additional_timetable'), false, false, $arSelect);

while ($obElement = $res->GetNextElement()) {
	$arElement = $obElement->fields;

	foreach ($obElement->GetProperties() as $key => $property) {
		if ($key == "RESTAURANTS") {
			if (!empty($property['VALUE'])) {
				$bRestaurant = false;
				foreach ($property['VALUE'] as $value) {
					if ($arParams['ID'] == $value)
						$bRestaurant = true;
				}
			} else {
				$bRestaurant = true;
			}
		} else {
			if ($key == 'TYPE') {
				$arElement['TYPE'] = $property['VALUE_XML_ID'];
			} else {
				$arElement[$key] = $property['VALUE'];
			}
		}
	}

	if ($arElement['TYPE'] == 'delivery' && $bRestaurant) {
		$arSpl = explode('-', $arElement['ADDITIONAL_TIME']);

		$timeBegin = strtotime($arElement['DATE'] . $arSpl[0] . ":00");
		$timeEnd = strtotime($arElement['DATE'] . $arSpl[1] . ":00");
		$bNext = false;
		if ($timeEnd < $timeBegin) {
			$bNext = true;
			$timeEnd = $timeEnd + (60 * 60 * 24);
		}

		$today = $bNext ? strtotime($arElement['DATE']) + (60 * 60 * 24) : strtotime($arElement['DATE']);

		if(strtotime(date('Y-m-d')) == $today) {
			if($arResult) {
				if($now < $timeBegin || $now > $timeEnd) {
					$arResult = false;
				}
			}
			else {
				if($now > $timeBegin || $now < $timeEnd) {
					$arResult = true;
				}
			}
		}
	}
}

return $arResult;