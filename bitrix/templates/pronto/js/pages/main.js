'use strict';

$(document).ready(function(){

	$('[data-main-slider]').owlCarousel({
		items: 1,
		nav: true,
		loop:true,
		navRewind: true,
		autoplayTimeout: 5000,
		autoplay: true,
		autoplayHoverPause: true,
		navText: ['','']
	});

	//parallax
	var scene2 = document.getElementById('parallax-bottom');
	var parallaxBottom = new Parallax(scene2);

	if($('.content.last div.wrapper.index a.contest').attr('href') == '#') {
		$('.content.last div.wrapper.index a.contest').click(function() {
			return false;
		}).css('cursor', 'default');
	}
});