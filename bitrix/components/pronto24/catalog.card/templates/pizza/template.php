<div class="row">
	<div class="col col_2 product-img">
		<img src="<?= $arResult['SRC'] ?>" alt="">
	</div>
	<div class="col col_2 _p5">
		<?= !empty($arResult['~HTML_TITLE']) ? html_entity_decode($arResult['~HTML_TITLE']) : '<span class="title">'.$arResult['NAME'].'</span>' ?>
		<div class="text-intro">
			<?= $arResult['DESCRIPTION'] ?>
		</div>
		<div data-product-card class="set pizza">
			<div class="title-set">добавить ингредиенты:</div>
			<div data-modificators class="set-scroll-product" data-set-scroll><? $APPLICATION->IncludeComponent("pronto24:modificators") ?></div>
			<div class="check-size">
				<div class="title-set">размер:</div>
				<? $bFirst = true; ?>
				<? foreach ($arResult['SIZES'] as $key => $size): ?>
					<label class="checkbox-label radio">
						<input data-price="<?= $size['PRICE'] ?>" class="size-selector checkbox"
						       value="<?= $size['ID'] ?>" type="radio" name="radio"
						       id="radio-<?= $size['ID'] ?>" <?= $bFirst ? 'checked' : '';
						$bFirst = false; ?>>
						<span class="checkbox-label-text"><?= $size['SIZE'] ?> см<span
								class="ic icon-svg-2s<?= $key == 1 ? '' : 2?>"></span></span>
					</label>
				<? endforeach ?>
			</div>
			<div class="rub"><span class="price"><?= $arResult['SIZES'][0]['PRICE'] ?></span> руб.</div>
			<div class="set-col">
				<div class="btn-group number-group">
					<button data-btn-number="" type="button" class="btn border btn-number"
					        data-type="minus"></button>
					<input data-input-number="" type="text" name="" class="btn border input-number" value="1"
					       min="1" readonly="">
					<button data-btn-number="" type="button" class="btn border btn-number"
					        data-type="plus"></button>
				</div>
			</div>
			<div class="set-col">
				<button data-product-id="<?= $arResult['SIZES'][0]['ID'] ?>" data-cnt="1"
				        class="btn pink small basket-add">
					в корзину
				</button>
			</div>
		</div>
	</div>
</div>

<? if (!empty($arResult['RECOMENDATIONS'])) : ?>
	<div class="title-slide">Добавьте к заказу:</div>
	<div class="product-slider" data-product-slider>
		<? foreach ($arResult['RECOMENDATIONS'] as $ID) : ?>
			<? $APPLICATION->IncludeComponent("pronto24:catalog.owllist.card", '.default', Array("ID" => $ID)); ?>
		<? endforeach ?>
	</div>
<? endif ?>