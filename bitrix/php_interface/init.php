<?php

AddEventHandler("iblock", "OnBeforeIBlockElementAdd", Array("CreateVacationClass", "OnBeforeIBlockElementAddHandler"));
AddEventHandler('main', 'OnEpilog', '_Check404Error', 1);
function _Check404Error()
{
	if (defined('ERROR_404') && ERROR_404 == 'Y') {
		global $APPLICATION;
		$APPLICATION->RestartBuffer();
		include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/header.php';
		include $_SERVER['DOCUMENT_ROOT'] . '/404.php';
		include $_SERVER['DOCUMENT_ROOT'] . SITE_TEMPLATE_PATH . '/footer.php';
	}
}

class CreateVacationClass
{
	// создаем обработчик события "OnBeforeIBlockElementAdd"
	function OnBeforeIBlockElementAddHandler(&$arFields)
	{
		if($arFields['IBLOCK_ID'] == 4) {
			$arFields['NAME'] = $arFields['NAME'] . ' ' .
				self::getValueByPropertyValue($arFields['PROPERTY_VALUES'][9]) . ' - ' .
				self::getValueByPropertyValue($arFields['PROPERTY_VALUES'][7]);
		}
	}

	function getValueByPropertyValue($propValue) {
		CModule::IncludeModule('iblock');

		$res = CIBlockElement::GetByID($propValue['n0']['VALUE']);
		if($ar_res = $res->GetNext())
			return $ar_res['NAME'];
	}
}

?>