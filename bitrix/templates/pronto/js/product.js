'use strict';

function ProductManager_f() {

	this.init = function () {
		$(document)
			.on("click", ".basket-add", ProductManager.basketAdd)
			.on("click", ".basket-del", ProductManager.deleteProduct)
			.on("click", ".promo .btn", ProductManager.addCoupon)
			.on("change", ".selectpicker", ProductManager.pizzaMainSelect)
			.on("change", ".size-selector", ProductManager.pizzaCardSelect);
		ProductManager.initCounter();
	};

	this.addCoupon = function () {
		var coupon = $(this).parent().find('.coupon-txt').val();
		if (coupon.length > 3) {
			$.post(
				"/ajax/",
				{"action": 'basketcoupon', "coupon": coupon},
				function (data) {
					if (data.result == 'ok') {
						//ProductManager.updateBasket;
						return false;
					}
				}, "json");
		}
	};

	this.pizzaCardSelect = function () {
		$(this).closest('[data-product-card]').find('.price').text($(this).attr('data-price'));
		$(this).closest('[data-product-card]').find('.basket-add').attr('data-product-id', $(this).val());
	};

	this.pizzaMainSelect = function () {
		$(this).closest('[data-product-card]').find('.price').text($(this).find('option:selected').attr('data-price'));
		$(this).closest('[data-product-card]').find('.basket-add').attr('data-product-id', $(this).val());
	};

	this.deleteProduct = function () {
		var productId = $(this).attr('data-basket-product-id');
		var rowToDel = $(this).closest('div.basket-row')
		$.post(
			"/ajax/",
			{"action": 'basketdel', "product_id": productId},
			function (data) {
				if (data.result == 'ok') {
					$(rowToDel).remove();
					ProductManager.updateBasket(data.basket);
					return false;
				}
			}, "json");
	};

	this.basketAddAnimate = function(e) {
		var img = $(e.target).closest('[data-product-card]').find('a img').first();
		if(img.length == 0) {
			var img = $('.product-img img').first();
		}
		var clone = $(img).clone().css('position', 'absolute').css('top', '121px');
		$(img).after(clone);

		var basket = $('[data-basket]');
		if(basket.length > 0) {
			if(basket.offset().top < 0) {
				var basket = $('[data-basket-2]');
			}
			var top = $(basket).position().top;
			var left = $(basket).position().left;
		}


		$(clone).animate({
			width: '25px',
			height: '25px',
			opacity: 0.5,
			top: top,
			left: left
		}, 1500, function() {
			$(this).remove();
		});
	};

	this.basketAdd = function (e) {
		/*ProductManager.basketAddAnimate(e);
		return false;*/
		var productId = $(e.target).attr('data-product-id');
		var count = $(e.target).attr('data-cnt');
		var modificators = $(e.target).closest('[data-product-card]').find('[data-modificators]');

		if (typeof ConstructorManager != 'undefined') {
			var productId = ConstructorManager.returnPizzaId();
			var count = 1;
			var arModificatorsValues = ConstructorManager.returnModificators();
			if (arModificatorsValues.length == 0) {
				return false;
			}
		}
		else {
			var arModificatorsValues = new Array;
			if (modificators.length > 0) {
				var arModStrings = $(modificators).find('[data-input-number]');
				for (var index = 0; index < arModStrings.length; ++index) {
					if ($(arModStrings[index]).val() > 0) {
						var mod = {'id': $(arModStrings[index]).attr('data-product-id'), 'count': $(arModStrings[index]).val()};
						arModificatorsValues.push(mod);
					}
				}
			}
		}

		if (modificators.length > 0) {
			var arModStrings = $(modificators).find('[data-input-number]');
			for (var index = 0; index < arModStrings.length; ++index) {
				$(arModStrings[index]).val(0);
			}
		}

		$.post(
			"/ajax/",
			{"action": 'basketadd', "product_id": productId, "cnt": count, "modificators": arModificatorsValues},
			function (data) {
				if (data.result == 'ok') {
					if (typeof ConstructorManager != 'undefined') {
						$('.item-link').removeClass('active');
						$('.item-link').closest('.item').find('[data-input-constructor]').val(1)
					}
					else {
						$(e.target).attr('data-cnt', 1);
						$(e.target).closest('[data-product-card]').find('[data-input-number]').val(1);
					}
					ProductManager.updateBasket(data.basket);
					return false;
				}
			}, "json");
	};

	this.initCounter = function () {
		var btnCount = $('[data-btn-number]');
		btnCount.click(function (e) {
			e.preventDefault();
			var type = $(this).attr('data-type');
			var input = $(this).parent().find('[data-input-number]');
			var currentVal = parseFloat(input.val());
			var max = parseFloat(input.attr('max'));
			var min = parseFloat(input.attr('min'));

			if (!currentVal || currentVal == "" || currentVal == "NaN") currentVal = 0;
			if (max == "" || max == "NaN") max = '';
			if (min == "" || min == "NaN") min = 0;

			if (type == 'plus') {
				if (max && (max == currentVal || currentVal > max)) {
					input.val(max);
				} else {
					input.val(currentVal + 1);
				}
			} else if (type == 'minus') {
				if (min && (min == currentVal || currentVal < min)) {
					input.val(min);
				} else if (currentVal > 0) {
					input.val(currentVal - 1);
				}
			}
			if ($(e.target).hasClass('basketCounter')) {
				ProductManager.changeBasketCount(e);
			}
			else {
				ProductManager.changeProductCount(e);
			}
		});
	};

	this.changeBasketCount = function (e) {
		var count = $(e.target).parent().find('[data-input-number]').val();
		var productId = $(e.target).parent().find('[data-input-number]').attr('data-basket-product-id');
		$.post(
			"/ajax/",
			{"action": 'basketupdate', "product_id": productId, "cnt": count},
			function (data) {
				if (data.result == 'ok') {
					ProductManager.updateBasket(data.basket);
					return false;
				}
			}, "json");
	};

	this.updateBasket = function (basketData) {
		$('.basket-cnt-products').text(basketData.count);
		$('.basket-product-s').text(basketData.products);
		$('.basket-price').text(basketData.price);
		if ($('.basket-cnt-products').text() == 0) {
			document.location.href = '/basket/';
		}
		if (parseInt($('.total.basket-price').text()) >= 650) {
			$('.apply .text').html('<b>Бесплатная доставка</b>');
		}
		else {
			$('.apply .text').html('<b>Бесплатная доставка от 650 руб.</b>вам осталось заказать на сумму <span class="rest">' + (650 - parseInt($('.total.basket-price').text())) + '</span> руб.');
		}
	};

	this.changeProductCount = function (e) {
		var val = $(e.target).parent().find('[data-input-number]').val();
		var btn = $(e.target).closest('[data-product-card]').find('.basket-add');
		$(btn).attr('data-cnt', val);
	};
}