<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");

global $USER;
$arGroups = CUser::GetUserGroup($USER->GetID());
if (!in_array(1, $arGroups)) {
	require($_SERVER['DOCUMENT_ROOT'] . '/404.php');
}
$APPLICATION->SetTitle("Зоны доставки");
use Bitrix\Main\Page\Asset;

Asset::getInstance()->addJs('https://api-maps.yandex.ru/2.0-stable/?load=package.full&mode=release&lang=ru-RU');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/delivery.js');

CModule::IncludeModule('iblock');

$arFilters = Array("IBLOCK_ID" => 3);

$arProperties = Array(
	'ID',
	'IBLOCK_ID',
	'NAME',
	'PROPERTY_JUPITER_ID'
);

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilters, false, false, $arProperties);

while ($arElement = $res->GetNext()) {
	if (!is_null($arElement['PROPERTY_JUPITER_ID_VALUE'])) {
		$arResult['PLACES'][$arElement['PROPERTY_JUPITER_ID_VALUE']] = $arElement['NAME'];
	}
}

?>
	<script>
		function restaurants(key) {
			var names = [
				<? foreach($arResult['PLACES'] as $key => $value) :?>
				{<?=$key?>:
			"<?=$value?>"
		},
			<? endforeach ?>
		]
			;
			for (var i = 0; i < names.length; i++) {
				if (typeof (names[i][key]) != 'undefined') {
					return names[i][key];
				}
			}
			return null;
		}

		function test() {
			return true;
		}
	</script>
	<style>
		.suggestions-suggestion {
			color: #000;
		}
	</style>

	<div class="map" id="map" style="height:500px;"></div>
<? $APPLICATION->IncludeComponent('pronto24:restaurants.parser'); ?>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>