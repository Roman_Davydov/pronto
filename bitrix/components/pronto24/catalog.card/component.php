<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');

$arProperties = Array(
	'ID',
	'CODE',
	'DETAIL_PICTURE',
	'IBLOCK_ID',
	'IBLOCK_CODE',
	'PROPERTY_SIZE',
	'NAME',
	'PROPERTY_HTML_TITLE',
	'PROPERTY_DESCRIPTION',
	'PROPERTY_PICTURE',
	'PROPERTY_WEIGHT',
	'PROPERTY_PRICE',
	'PROPERTY_POSITION',
	'PROPERTY_OUTER_ID',
	'PROPERTY_B_SEAFOOD',
	'PROPERTY_B_MEAT',
	'PROPERTY_B_VEGETERIAN',
	'PROPERTY_LINK_TO',
	'PROPERTY_OUTER_ID'
);
$CODE = explode('?', $_GET['code']);
$CODE[0] = str_replace('/', '', $CODE[0]);

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $arParams['IBLOCK_ID'], 'CODE' => $CODE[0]), false, false, $arProperties);

$obElement = $res->GetNextElement();

if (is_numeric($obElement->fields['ID'])) {
	$arResult = $obElement->fields;
	$arProps = $obElement->GetProperties();
	foreach ($arProps as $key => $property) {
		if (isset($property['VALUE'])) {
			$arResult[$key] = $property['VALUE'];
		}
	}

	if(isset($_SESSION['STOP_LIST']) && in_array($arResult['PROPERTY_OUTER_ID_VALUE'], $_SESSION['STOP_LIST'])) {
		LocalRedirect('/'.$arResult['IBLOCK_CODE'].'/');
	}
	else {
		$picture = CFile::GetFileArray($obElement->fields["DETAIL_PICTURE"]);
		$arResult['SRC'] = $picture['SRC'];
		foreach ($arResult['MORE_PICTURES'] as $key => $IDpicture) {
			$picture = CFile::GetFileArray($IDpicture);
			$arResult['MORE_PICTURES'][$key] = $picture['SRC'];
		}
		$arResult['PRICE'] = getPrice($arResult['ID']);

		if ($arParams['IBLOCK_ID'] == 5) {//pizza and sizes
			$arResult['SIZES'][] = Array(
				'ID' => $arResult['ID'],
				'PRICE' => $arResult['PRICE'],
				'SIZE' => $obElement->fields["PROPERTY_SIZE_VALUE"]
			);
			$res = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => $arParams['IBLOCK_ID'], 'PROPERTY_LINK_TO' => $arResult['ID']), false, false, $arProperties);
			while ($ob = $res->GetNextElement()) {
				$arFields = $ob->GetFields();
				$arResult['SIZES'][] = Array('ID' => $arFields['ID'], 'PRICE' => getPrice($arFields['ID']), 'SIZE' => $arFields["PROPERTY_SIZE_VALUE"]);
				$arResult['NAME'] = compareStrings($arResult['NAME'], $arFields['NAME']);
			}
		}
		if (!empty($arResult['RECOMENDATIONS'])) {
			$res = CIBlockElement::GetByID($arResult['RECOMENDATIONS']);
			$obElement = $res->GetNextElement();
			$arProperties = $obElement->GetProperties();
			unset($arResult['RECOMENDATIONS']);
			foreach ($arProperties as $key => $value) {
				foreach ($value['VALUE'] as $ID) {
					if ($ID != $arResult['ID'])
						$arResult['RECOMENDATIONS'][] = $ID;
				}
			}
		}
		$APPLICATION->SetTitle($APPLICATION->GetTitle() . ' ' . $arResult['NAME']);
		$this->IncludeComponentTemplate();
	}
} else {
	require($_SERVER['DOCUMENT_ROOT'] . '/404.php');
}