<?
CModule::IncludeModule("socialservices");
$arParams['BACKURL'] = $_SERVER['HTTP_REFERER'];
$oAuthManager = new CSocServAuthManager();
$arServices = $oAuthManager->GetActiveAuthServices($arParams);
?>

<form action="#">
    <h2>Вход</h2>
    <div class="form-grid">
        <div class="form-row">
            <div class="form-cell">логин</div>
            <div class="form-cell">
                <div class="input-data">
                    <input type="text" id="login">
                </div>
            </div>
        </div>
        <div class="form-row"> <!--add class error-->
            <div class="form-cell">пароль</div>
            <div class="form-cell">
                <div class="input-data">
                    <input type="password" id="password">
                    <!--div class="hint">Минимум 6 символов</div-->
                </div>
            </div>
        </div>
    </div>
    <div class="_txt-center">
        <a class="sl" href="#">Не помню пароль</a> <br>
        <button class="btn green button-auth" type="submit">войти</button>
        <div class="form-bottom">
            <p>войти через социальные сети:</p>
            <a href="#" onClick="<?=$arServices['VKontakte']['ONCLICK']?>" class="soc-icon-s_1"></a>
            <a href="#" onClick="<?=$arServices['Facebook']['ONCLICK']?>" class="soc-icon-s_2"></a>
        </div>
    </div>
</form>
