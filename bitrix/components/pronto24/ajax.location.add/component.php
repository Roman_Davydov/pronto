<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

global $USER;
$result = Array('result' => 'error');
$locationID = isset($_POST['locationID']) && $_POST['locationID'] != 'false' ? $_POST['locationID'] : false;
$city = isset($_POST['city']) ? $_POST['city'] : false;
$address = isset($_POST['address']) ? $_POST['address'] : false;
$addressName = isset($_POST['addressName']) ? $_POST['addressName'] : false;
$section = isset($_POST['section']) ? $_POST['section'] : false;
$stage = isset($_POST['stage']) ? $_POST['stage'] : false;
$flat = isset($_POST['flat']) ? $_POST['flat'] : false;

if ($addressName && $address) {
	CModule::IncludeModule('iblock');
	$el = new CIBlockElement;
	if($locationID) {
		$arFields = Array(
			"NAME" => $addressName,
			"IBLOCK_ID" => 29,
			"PROPERTY_VALUES" => Array(
				'ADDRESS' => $address,
				'USER' => $USER->GetID()
			));
		$arFields["PROPERTY_VALUES"]["FLAT"] = $flat;
		$arFields["PROPERTY_VALUES"]["SECTION"] = $section;
		$arFields["PROPERTY_VALUES"]["STAGE"] = $stage;

		$el->Update($locationID, $arFields);

		$result = Array('result' => 'ok');
	}
	else {
		$arFields = Array(
			"NAME" => $addressName,
			"IBLOCK_ID" => 29,
			"PROPERTY_VALUES" => Array(
				'ADDRESS' => $address,
				'USER' => $USER->GetID()
			));
		if ($section)
			$arFields["PROPERTY_VALUES"]["FLAT"] = $flat;
		if ($section)
			$arFields["PROPERTY_VALUES"]["SECTION"] = $section;
		if ($stage)
			$arFields["PROPERTY_VALUES"]["STAGE"] = $stage;

		if($ID = $el->Add($arFields))
			$result = Array('result' => 'ok', 'title' => $addressName, 'locationID' => $ID);
	}
}

return $result;