<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Ресторан"); ?>

    <div class="content restaurant one">
        <div class="wrapper">
            <h2 class="big m-b50">Рестораны</h2>
            <div class="row">
                <div class="col col_3">
                    <? $arStocks = $APPLICATION->IncludeComponent("pronto24:restaurants.card"); ?>
                </div>
                <div class="col col_3c">
                    <div id="map" class="map"></div>
                    <div class="stock row">
                        <? $APPLICATION->IncludeComponent("pronto24:stock.list", '.default', Array('STOCKS' => $arStocks)); ?>
                    </div>
                </div>
            </div>
        </div>
    </div>

<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>