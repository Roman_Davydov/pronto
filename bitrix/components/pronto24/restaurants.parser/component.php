<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$filename =  $_SERVER['DOCUMENT_ROOT'] . '/xml/RESTAURANT_ZONES.XML';

$xmlStr = file_get_contents($filename);

$restaurants = new SimpleXMLElement($xmlStr);
$arResult = Array();
if ($restaurants->attributes() == 'restaurantZones') {
	$items = $restaurants->items;
	foreach ($items->item as $item) {
		$polygon = $item[0];
		$attributes = $item->attributes();
		$arItem = Array(
			'sum_min' => (int)$attributes['sum_min'],
			'time_min' => (int)$attributes['time_min'],
			'time_max' => (int)$attributes['time_max'],
			'polygon' => strToPolygon((string)$polygon)
		);
		$arResult[(int)$attributes['restaurant_id']][(int)$attributes['id']] = $arItem;
	}
}

if ($arParams['RETURN'] == 'Y') {
	return $arResult;
} else {
	$this->IncludeComponentTemplate();
}

function strToPolygon($str)
{
	$arPolygon = Array();
	$arLines = explode("\n", $str);
	foreach ($arLines as $line) {
		if (!empty($line)) {
			$arPolygon[] = '[' . $line . ']';
		}
	}
	return implode(',', $arPolygon);
}