<script>
	function restPolygons() {
		var RestPolygons = {
			<? foreach($arResult as $key => $item) :?>
			"<?= $key ?>": {
				<?foreach($item as $idZone => $zone) :?>
				"<?=$idZone?>": {
					sum_min: <?= $zone['sum_min']?>,
					time_min: <?= $zone['time_min']?>,
					time_max: <?= $zone['time_max']?>,
					polygon: [<?= $zone['polygon']?>]
				},
				<? endforeach ?>
			},
			<? endforeach ?>
		};
		return RestPolygons;
	}
</script>