<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$productID = intval(htmlspecialchars($_POST["product_id"]));
$quantity = intval(htmlspecialchars($_POST["cnt"]));
$result = Array('result' => 'error');
if (CModule::IncludeModule("iblock") && CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
	if (IntVal($productID) > 0) {
		$product = CSaleBasket::GetByID($productID);

		//на сколько увеличиваем/уменьшаем кол-во
		$avrQuantity = $quantity - $product['QUANTITY'];

		$db_res = CSaleBasket::GetPropsList(
			array(
				"SORT" => "ASC",
				"NAME" => "ASC"
			),
			array("BASKET_ID" => $product['ID'])
		);
		while ($ar_res = $db_res->Fetch())
		{
			if($ar_res['NAME'] == 'MODIFICATOR') {
				$ar_res['CODE'];
				$ar_res['VALUE'];
				$productModificator = CSaleBasket::GetByID($ar_res['CODE']);
				//увеличим/уменьшим нужные позиции в корзине на нужные числа
				CSaleBasket::Update(
					$ar_res['CODE'], array("QUANTITY" => $productModificator["QUANTITY"] + $ar_res['VALUE']*$avrQuantity));
			}
		}

		$arFields = array(
			"QUANTITY" => $quantity
		);

		$result = CSaleBasket::Update(
			$productID, $arFields
		);

		if ($result) {
			$APPLICATION->IncludeComponent("pronto24:basket.line", "json");
			$result = Array('result' => 'ok', 'basket' => $_SESSION['basket_line']);

			$stocks = $APPLICATION->IncludeComponent("pronto24:stock.update", "", Array("TOTAL_PRICE" => $_SESSION['basket_line']['clear_price']));
			if(!empty($stocks)) {
				$result['stocks'] = $stocks;
			}
		}
	}
}

return $result;