<div class="content-white">
	<div class="wrapper cabinet">
		<div class="row">
			<a href="?logout=yes">
				<button class="btn border fl-r">выход</button>
			</a>
			<h2 class="big">Личный кабинет</h2>
		</div>
		<div class="cabinet-row">
			<h2 class="small">Личные данные</h2>
			<div class="row">
				<div class="col col_2">
					<div class="form-grid">
						<div class="form-row">
							<div class="form-cell">имя*</div>
							<div class="form-cell">
								<div class="input-data">
									<input data-name type="text" value="<?= $USER->GetFirstName() ?>">
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="form-cell">фамилия*</div>
							<div class="form-cell">
								<div class="input-data">
									<input data-last-name type="text" value="<?= $USER->GetLastName() ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col col_2">
					<div class="form-grid">
						<div class="form-row">
							<div class="form-cell">день рождения*</div>
							<div class="form-cell">
								<div class="input-data">
									<input maxlength="10" data-birthday value="<?= $arResult['PERSONAL_BIRTHDAY'] ?>"
									       type="text">
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="form-cell">e-mail</div>
							<div class="form-cell">
								<div class="input-data">
									<input data-email type="text" value="<?= $arResult['EMAIL'] ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="fl-r">
					<button class="btn green changePersonals">сохранить</button>
				</div>
			</div>
		</div>

		<? /*<div class="cabinet-row">

			<h2 class="small">Изменение номера телефона</h2>
			<div class="row">
				<div class="col col_2">
					<div class="form-grid">
						<div class="form-row">
							<div class="form-cell nowrap">новый номер*</div>
							<div class="form-cell _txt-right">
								<div class="input-data phone">
									<span>+7</span>
									<input type="text">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="fl-r">
					<button class="btn green" onclick="MainManager.showModal('phone')">Отправить</button>
				</div>
			</div>
		</div>*/ ?>

		<div class="cabinet-row">
			<div class="addr-list" data-addr-list>
				<h2 class="m-b20 small">
					Адрес доставки
				</h2>
			</div>

			<div class="addr-input">
				<input type="text" data-address>
			</div>

			<div class="m-b20 input-data row">

				<div class="form-grid _3">
					<div class="form-row">
						<div class="form-cell">подъезд</div>
						<div class="form-cell">
							<div class="input-data">
								<input data-section type="text">
							</div>
						</div>
						<div class="form-cell">этаж</div>
						<div class="form-cell">
							<div class="input-data">
								<input data-stage type="text">
							</div>
						</div>
						<div class="form-cell">квартира или номер офиса</div>
						<div class="form-cell">
							<div class="input-data">
								<input data-flat type="text">
							</div>
						</div>
					</div>
				</div>
			</div>

			<? /*<<div class="row">

				div class="fl-r">
					<button class="btn border m-r4" data-add-location>+ добавить адрес</button>
					<button class="btn green" data-save-location>Сохранить</button>
				</div>

			</div>*/ ?>

		</div>

		<? $APPLICATION->IncludeComponent("pronto24:order.list"); ?>

		<div class="cabinet-row">
			<h2 class="small">Дисконтная карта</h2>
			<div class="row">
				<div class="col col_2">
					<div class="activate-box <?= isset($arResult['CARD']) ? '' : 'true' ?>" data-activate-input>
						<div class="form-grid">
							<div class="form-row">
								<div class="form-cell">номер*</div>
								<div class="form-cell">
									<div class="input-data activate">
										<input data-card-number type="text" value="<?= $arResult['CARD'] ?>">
										<button class="discountCardChange btn orange big">
											активировать
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div data-activate-box class="activate-box <?= isset($arResult['CARD']) ? 'true' : '' ?>">
					<button class="btn border fl-r m-b10 discountCard">изменить</button>
					<p><?= $arResult['CARD'] ?></p>
					<p class="color">Ваша карта предоставляет вам скидку 5%</p>
				</div>
			</div>
		</div>

		<div class="cabinet-row subscribe">
			<h2 class="small">Подписка</h2>
			<div class="row">
				<div class="col col_2">
					<div class="activate-box <?= $arResult['SUBSCRIPTION'] ? '' : 'true'?>">
						<div class="form-grid">
							<div class="form-row">
								<div class="form-cell">e-mail</div>
								<div class="form-cell">
									<div class="input-data activate">
										<input type="text" value="<?= $arResult['EMAIL'] ?>" readonly="true">
										<button data-subscribe class="btn orange big">
											подписаться
										</button>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="activate-box <?= $arResult['SUBSCRIPTION'] ? 'true' : ''?>">
					<button data-unsubscribe class="btn border fl-r m-b10">отменить подписку
					</button>
					<p class="email-subscr"><?= $arResult['SUBSCRIPTION'] ?></p>
					<p class="color">Вы подписаны на еженедельную рассылку новостей и акций компании «Пронто»</p>
				</div>
			</div>
		</div>

		<div class="cabinet-row">
			<h2 class="small">Изменение пароля</h2>
			<div class="row">
				<div class="col col_2">
					<div class="form-grid">
						<div class="form-row">
							<div class="form-cell">старый пароль</div>
							<div class="form-cell">
								<div class="input-data">
									<input data-password type="password">
								</div>
							</div>
						</div>
					</div>
				</div>

				<div class="col col_2">
					<div class="form-grid">
						<div class="form-row">
							<div class="form-cell">новый пароль*</div>
							<div class="form-cell">
								<div class="input-data">
									<input data-new-password type="password">
									<div class="hint">Минимум 6 символов</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="fl-r">
					<button class="btn green changePassword">сохранить</button>
				</div>
			</div>
		</div>
	</div>
</div>