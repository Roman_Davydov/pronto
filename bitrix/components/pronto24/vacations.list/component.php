<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');

$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 1), false, false, Array('ID', 'NAME'));

while ($arElement = $res->GetNext()) {
	$arResult[$arElement['ID']] = $arElement['NAME'];
}

$this->IncludeComponentTemplate();