<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('sale');
CModule::IncludeModule('iblock');

$db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"));
$ar_sales = $db_sales->Fetch();

$arResult['id'] = $ar_sales["ID"];
$arResult['user'] = $ar_sales["USER_NAME"];

$rsBasket = CSaleBasket::GetList(array(
	"NAME" => "ASC"
),
	array(
		"LID" => SITE_ID,
		"ORDER_ID" => $arResult['id']
	),
	false,
	false,
	array("PRODUCT_ID", "NAME", 'PRICE', "DETAIL_PAGE_URL")
);

$maxPrice = 0;

while ($arItem = $rsBasket->Fetch()) {
	if($arItem['PRICE'] > $maxPrice) {
		$maxPriceItem = $arItem;
	}
}

$arResult['product_name'] = $maxPriceItem['NAME'];
$arResult['price'] = (int) $maxPriceItem['PRICE'];
$arResult['url'] = $maxPriceItem['DETAIL_PAGE_URL'];

$res = CIBlockElement::GetByID($maxPriceItem['PRODUCT_ID']);

$obElement = $res->GetNextElement();

$picture = CFile::GetFileArray($obElement->fields['DETAIL_PICTURE']);
$arResult['src'] = $picture['SRC'];

return array('result' => 'ok', 'order' => $arResult);