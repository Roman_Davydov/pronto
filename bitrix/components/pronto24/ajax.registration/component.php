<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if(isset($_POST['login'])) {
	$rawLogin = $_POST['login'];
	$rawLogin = str_replace(Array('+',' ', '-', '(', ')'), Array('', '', '', '', ''), $rawLogin);
	$firstLetter = substr($rawLogin, 0, 1);
	if($firstLetter == 8 || $firstLetter == 7) {
		$rawLogin = substr($rawLogin, 1);
	}
	$firstLetter = substr($rawLogin, 0, 1);
	if($firstLetter != 9) {
		return array('result' => 'error', 'error' => 'mobile');
	}
	if(strlen($rawLogin) == 10) {
		global $USER;
		$rsUser = $USER->GetByLogin($rawLogin);
		$arUser = $rsUser->Fetch();
		if(!$arUser) {
			CModule::IncludeModule("echogroup.smsru");
			$pass = newPassword();
			$ID = $USER->Add(Array("LOGIN" => $rawLogin, "LID" => "ru", "ACTIVE" => "Y", "PASSWORD" => $pass, "EMAIL" => $rawLogin . '@pronto24.ru', "GROUP_ID" => Array(5), "PERSONAL_PHONE" => $rawLogin));
			$smsOperator = new CEchogroupSmsru;
			$smsOperator->Send('7'.$rawLogin, "пароль " . $pass, "ProntoPizza");
			return array('result' => 'ok', 'user' => $ID);
		}
		else {
			return array('result' => 'error', 'error' => 'double');
		}
	}

	return array('result' => 'error');
}