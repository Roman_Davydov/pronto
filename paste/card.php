<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');
$APPLICATION->SetTitle("Паста");
?>
	<div class="content">
		<div class="wrapper index product paste-style">
			<? $APPLICATION->IncludeComponent("pronto24:catalog.card", '.default', Array("IBLOCK_ID" => 6)); ?>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>