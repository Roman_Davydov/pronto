<?php
function getPrice($id)
{
	$arPrice = CPrice::GetBasePrice($id);
	$arSplited = explode('.', $arPrice['PRICE']);
	return $arSplited[0];
}

function compareStrings($string1, $string2)
{
	$newWord = '';
	for ($i = 0; $i < mb_strlen($string1); $i++) {
		if (mb_substr($string1, $i, 1) == mb_substr($string2, $i, 1)) {
			$newWord .= mb_substr($string1, $i, 1);
		} else
			break;
	}
	return trim($newWord);
}

function getModificatorStringFromPropsAndBasket($arProprs, $basket) {
	$names = Array();
	foreach($arProprs as $mod) {
		$names[] = $basket[$mod['CODE']]['NAME'] . ($mod['VALUE']>1? ' (x'.$mod['VALUE'].')':'');
	}
	return implode(', ', $names);
}

function newPassword() {
	$passCharacters = "0123456789ABCDEFGHIJKLMNOP";
	$pass = '';
	for($i = 0; $i < 8; $i++) {
		$pass .= $passCharacters[rand(0, 26)];
	}
	return $pass;
}
?>