<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');

$onPage = isset($arParams['ONPAGE']) ? $arParams['ONPAGE'] : 10;

$page = isset($_GET['page'])&&is_numeric($_GET['page'])&&$_GET['page']>0 ? $_GET['page'] : 1;

$arFilter = Array(
	"IBLOCK_ID" => 7,
	"ACTIVE" => "Y"
);

$arProperties = Array('PROPERTY_TEXT_REVIEW', 'PROPERTY_USER', 'CREATED_BY');
$res = CIBlockElement::GetList(Array("ID"=>"DESC"), $arFilter, false, Array("iNumPage" => $page, "nPageSize" => $onPage), $arProperties);
$arResult = Array();
while ($arElement = $res->GetNext()) {
	$arResult[] = Array(
		'TEXT' => $arElement['PROPERTY_TEXT_REVIEW_VALUE']['TEXT'],
		'USER' => $arElement['PROPERTY_USER_VALUE']
	);
}

$count = $res->SelectedRowsCount();
$this->IncludeComponentTemplate();

$pages = intval($count/$onPage);
if($count%$onPage > 0) $pages++;

if($pages > 1) {
	echo '<div class="paginate">';
	for($i = 1; $i <= $pages; $i++) {
		echo '<a href="?page='.$i.'"'.($i==$page?' class="active"':'').'>'.$i.'</a>';
	}
	echo '</div>';
}
?>