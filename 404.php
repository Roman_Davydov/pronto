<? if ($_SERVER['DOCUMENT_URI'] == "/404.php") {
	$_SERVER['REQUEST_URI'] = $_SERVER['DOCUMENT_URI'];
}
include_once($_SERVER['DOCUMENT_ROOT'].'/bitrix/modules/main/include/urlrewrite.php');
CHTTP::SetStatus('404 Not Found');
@define('ERROR_404', 'Y');
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("Страница не найдена"); ?>

<div class="content">
	<ul id="parallax" class="parallax" style="transform: translate3d(0px, 0px, 0px); transform-style: preserve-3d; backface-visibility: hidden;">
		<li class="layer" data-depth="0.04" style="transform: translate3d(7.37412px, -5.33999px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: relative; display: block; left: 0px; top: 0px;">
			<div class="wrapper">
				<div class="background _3"></div>
			</div>
		</li>
		<li class="layer" data-depth="0.06" style="transform: translate3d(11.0612px, -8.00998px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
			<div class="wrapper">
				<div class="background _1"></div>
			</div>
		</li>
		<li class="layer" data-depth="0.1" style="transform: translate3d(18.4353px, -13.35px, 0px); transform-style: preserve-3d; backface-visibility: hidden; position: absolute; display: block; left: 0px; top: 0px;">
			<div class="wrapper">
				<div class="background _6"></div>
			</div>
		</li>
	</ul>
	<div class="wrapper index" style="margin-top:150px; margin-bottom:150px;">
		<h1>Страница не найдена</h1>
		<div class="sort">
			<a href="/">Перейдите на главную</a>
		</div>
	</div>
</div>

<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php"); ?>