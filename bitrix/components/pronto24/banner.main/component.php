<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');

$now = date('d.m.Y H:i:s');

$arFilter = Array(
	"IBLOCK_ID" => 2,
	"ACTIVE" => "Y",
	Array(
		"LOGIC" => "OR",
		Array("<=DATE_ACTIVE_FROM" => $now),
		Array("DATE_ACTIVE_FROM" => false),
	),
	Array(
		"LOGIC" => "OR",
		Array(">=DATE_ACTIVE_TO" => $now),
		Array("DATE_ACTIVE_TO" => false),
	)
);

$arProperties = Array('PROPERTY_BANNER_PICTURE', 'NAME', 'CODE', 'PROPERTY_LINK', 'BANNER_PICTURE');
$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilter, false, false, $arProperties);

while ($arElement = $res->GetNext()) {
	$picture = CFile::GetFileArray($arElement["PROPERTY_BANNER_PICTURE_VALUE"]);
	$arr = Array();
	if(is_numeric($arElement['PROPERTY_LINK_VALUE'])) {
		$arLinkRes = CIBlockElement::GetByID($arElement['PROPERTY_LINK_VALUE']);
		$arLink = $arLinkRes->GetNext();
	}
	$arr['LINK'] = 'stocks/'.$arLink['CODE'];
	$arr['SRC'] = $picture['SRC'];
	$arr['ALT'] = $arElement['NAME'];
	$arResult['BANNERS'][] = $arr;
}

$this->IncludeComponentTemplate();