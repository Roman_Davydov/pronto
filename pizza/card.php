<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');
$APPLICATION->SetTitle("Пицца");
?>
	<div class="content">
		<div class="wrapper index product pizza card">
			<? $APPLICATION->IncludeComponent("pronto24:catalog.card", 'pizza', Array("IBLOCK_ID" => 5)); ?>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>