<table class="base m-b40">
	<tr>
		<td>
			<p>
				Ваши предложения по развитию сети
				и аренде помещений
				направляйте по адресу:
			</p>
			<a href="#">aa.naletov@gmail.com</a>
		</td>
		<td>
			<p>
				Ваши предложения в отдел закупок
				направляйте по адресам:
			</p>
			<div class="grid">
				<p>Food (продукты):</p>
				<a href="#">shlyapoval@yandex.ru</a>
			</div>
			<div class="grid">
				<p>Beverages (бар):</p>
				<a href="#">kir_bar@mail.ru</a>
			</div>
		</td>
	</tr>
	<tr>
		<td>
			<p>
				Ваши предложения по вопросам
				оборудования, телефонизации и интернета
				направляйте по адресу:
			</p>
			<a href="#">shev@pronto24.ru</a>
		</td>
		<td>
			<p>
				Ваши предложения по рекламе,
				маркетингу и полиграфии
				направляйте по адресу:
			</p>
			<a href="#">pronto24site@gmail.com</a>
		</td>
	</tr>
</table>