<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die(); ?>
<? $_SESSION['basket_line'] = Array(
	'count' => $arResult['NUM_PRODUCTS'],
	'products' => $arResult['PRODUCT(S)'],
	'price' => $arResult['TOTAL_PRICE'],
	'clear_price' => $arResult['TOTAL_CLEAR_PRICE']
); ?>