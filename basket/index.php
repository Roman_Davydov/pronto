<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Корзина");
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');

$APPLICATION->IncludeComponent('pronto24:basket.basket');

require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>