<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');
global $USER;

$result = Array('result' => 'error');

$name = isset($_POST['name']) ? $_POST['name'] : false;
$theme = isset($_POST['theme']) ? $_POST['theme'] : false;
$restaurant = isset($_POST['restaurant']) ? $_POST['restaurant'] : false;
$text = isset($_POST['text']) ? $_POST['text'] : false;
$phone = isset($_POST['login']) ? $_POST['login'] : false;

if (!$USER->IsAuthorized()) {
	if (!$phone) {
		$result['error'] = 'phone';
		return $result;
	}

	$result = $APPLICATION->IncludeComponent("pronto24:ajax.registration");

	if ($result['result'] != 'ok') {
		return $result;
	}
	$user = $result['user'];
	$reviewUser = $USER->GetByID($user);
} else {
	$user = $USER->GetID();
	$reviewUser = $USER;
}

if ($reviewUser->GetFirstName == '') {
	$USER->Update($user, Array("NAME" => $name));
}

if (!empty($_FILES)) {
	$arFiles = Array();
	foreach ($_FILES['files']['name'] as $i => $fileName) {
		rename($_FILES['files']['tmp_name'][$i], $fileName);
		$arFiles[] = CFile::MakeFileArray($fileName);
	}
}

$arFields = Array(
	"PROPERTY_VALUES" => Array(
		'THEME' => $theme,
		'TEXT_REVIEW' => $text,
		'RESTAURANT' => $restaurant,
		'USER' => $name,
		'USER_ID' => $user,
		"FILE" => $arFiles
	),
	"IBLOCK_ID" => 7,
	"ACTIVE" => "N",
	"NAME" => $name . ' - ' . $theme
);
$obRestaurant = CIBlockElement::GetByID($restaurant);

$arRestaurant = $obRestaurant->GetNext();

$el = new CIBlockElement;
if ($ID = $el->Add($arFields)) {
	$subject = "Новый отзыв на сайте";
	
	$message = "<p>На сайте новый отзыв. Данные клиента:</p>";
	$message .= "<br/><b>Имя клиента:</b> " . htmlspecialchars($name);
	$message .= "<br/><b>Контакты клиента:</b> " . htmlspecialchars($phone);
	$message .= "<br/><b>Тема отзыва:</b> " . htmlspecialchars($theme);
	$message .= "<br/><b>Ресторан:</b> " . $arRestaurant['NAME'];
	$message .= "<br/><b>Сообщение:</b> " . htmlspecialchars($text);
	$message .= "<p>Данное письмо сформировано автоматически. Отвечать на него не нужно.</p>";

	$headers = "Content-type: text/html; charset=utf-8 \r\n";
	$headers .= "From: Сайт pronto \r\n";
	$headers .= "Reply-To: info@pronto24.ru \r\n";

	$APPLICATION->IncludeComponent('pronto24:email.send', '', Array('FILTER' => 'B_REVIEW', 'SUBJECT' => $subject, 'MESSAGE' => $message, 'HEADERS' => $headers));

	return Array('result' => 'ok');
}