<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Паста");
?>
	<div class="content">

		<ul id="parallax" class="parallax">
			<li class="layer" data-depth="0.04">
				<div class="wrapper">
					<div class="background _3"></div>
				</div>
			</li>
			<li class="layer" data-depth="0.06">
				<div class="wrapper">
					<div class="background _1"></div>
				</div>
			</li>
			<li class="layer" data-depth="0.1">
				<div class="wrapper">
					<div class="background _6"></div>
				</div>
			</li>
		</ul>

		<div class="wrapper index product paste-style">
			<div class="intro">
				Паста
			</div>
			<div class="sort">
				<a href="/paste/" <?= empty($_GET)?"class='active'":"" ?>>все</a>
				<a href="/paste/?seafood" <?= isset($_GET['seafood'])?"class='active'":"" ?>>с морепродуктами</a>
				<a href="/paste/?meat" <?= isset($_GET['meat'])?"class='active'":"" ?>>мясная</a>
			</div>
			<? $APPLICATION->IncludeComponent("pronto24:catalog.section", '.default', Array('IBLOCK_ID' => 6)); ?>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>