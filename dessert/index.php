<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Десерты");
?>
	<div class="content">

		<ul id="parallax" class="parallax">
			<li class="layer" data-depth="0.06">
				<div class="wrapper">
					<div class="background _1"></div>
					<div class="background _2"></div>
				</div>
			</li>
			<li class="layer" data-depth="0.08">
				<div class="wrapper">
					<div class="background _3"></div>
					<div class="background _4"></div>
				</div>
			</li>
		</ul>
		<div class="wrapper index product dessert-style">
			<div class="intro m-b40">
				десерты
			</div>
			<? $APPLICATION->IncludeComponent("pronto24:catalog.section", '.default', Array('IBLOCK_ID' => 11)); ?>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>