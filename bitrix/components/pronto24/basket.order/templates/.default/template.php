<div class="content">
	<div class="wrapper">
		<h2 class="big">Заказ</h2>
		<div class="content-form">
			<div class="row m-b25">
				<div class="col col_2">
					<h2 class="m-b20 orange">Личные данные</h2>
					<div class="form-grid">
						<div class="form-row">
							<div class="form-cell">* имя</div>
							<div class="form-cell">
								<div class="input-data">
									<input <?= !empty($arResult['NAME']) ? 'disabled' : '' ?> data-name type="text"
									                                                          value="<?= !empty($arResult['NAME']) ? $arResult['NAME'] : (!empty($_SESSION['DEFAULTS']['name'])? $_SESSION['DEFAULTS']['name'].'session' : '') ?>">
								</div>
							</div>
						</div>
						<div class="form-row">
							<div class="form-cell">* телефон</div>
							<div class="form-cell _txt-right">
								<div class="input-data">
									<input <?= '';!empty(phoneMask($arResult['PHONE'])) ? 'disabled' : '' ?> data-phone type="text"
									                                                           value="<?= !empty($arResult['PHONE']) ? $arResult['PHONE'] : (!empty($_SESSION['DEFAULTS']['phone']) ? $_SESSION['DEFAULTS']['phone'] : '') ?>">
								</div>
							</div>
						</div>
					</div>
				</div>
				<div class="col col_2">
					<h2 class="m-b20 orange">Cпособ оплаты</h2>
					<div class="checkbox-right">
						<label class="checkbox-label radio">
							<input class="checkbox" type="radio" name="PAY_SYSTEM_ID" value="1" <?= !empty($_SESSION['DEFAULTS']['paySystem']) ? ($_SESSION['DEFAULTS']['paySystem']==1 ? 'checked':'') : 'checked' ?>>
							<span class="checkbox-label-text">Наличными курьеру</span>
						</label>
						<div class="right-input">
							<span>сдача с</span>
							<input type="text" value="<?= !empty($_SESSION['DEFAULTS']['change'])? $_SESSION['DEFAULTS']['change'] : '' ?>" data-change>
						</div>
					</div>

					<label class="checkbox-label radio">
						<input class="checkbox" type="radio" name="PAY_SYSTEM_ID" value="2" <?= !empty($_SESSION['DEFAULTS']['paySystem']) ? ($_SESSION['DEFAULTS']['paySystem']==2 ? 'checked':'') : '' ?>>
						<span class="checkbox-label-text">Банковская карта</span>
					</label><br>
					<label class="checkbox-label radio">
						<input class="checkbox" type="radio" name="PAY_SYSTEM_ID" value="3" <?= !empty($_SESSION['DEFAULTS']['paySystem']) ? ($_SESSION['DEFAULTS']['paySystem']==3 ? 'checked':'') : '' ?>>
						<span class="checkbox-label-text">Электронные деньги</span>
					</label>
				</div>
			</div>
			<div class="addr-list">
				<h2 class="m-b20 orange">
					Адрес доставки
				</h2>
			</div>

			<div class="addr-input">
				<input data-address value="<?= !empty($_SESSION['DEFAULTS']['address'])? $_SESSION['DEFAULTS']['address'] : '' ?>"  type="text">
			</div>

			<div class="m-b30 input-data row">
				<div class="form-grid _3">
					<div class="form-row">
						<div class="form-cell">подъезд</div>
						<div class="form-cell">
							<div class="input-data">
								<input value="<?= !empty($_SESSION['DEFAULTS']['section'])? $_SESSION['DEFAULTS']['section'] : '' ?>" data-section type="text">
							</div>
						</div>
						<div class="form-cell">этаж</div>
						<div class="form-cell">
							<div class="input-data">
								<input value="<?= !empty($_SESSION['DEFAULTS']['stage'])? $_SESSION['DEFAULTS']['stage'] : '' ?>" data-stage type="text">
							</div>
						</div>
						<div class="form-cell">квартира/ <br> № офиса*</div>
						<div class="form-cell">
							<div class="input-data">
								<input value="<?= !empty($_SESSION['DEFAULTS']['flat'])? $_SESSION['DEFAULTS']['flat'] : '' ?>" data-flat type="text">
							</div>
						</div>
					</div>
				</div>
				<div class="hint _error">Необходимо указать точный и корректный адрес доставки</div>
			</div>

			<div class="m-b30 basket-map" id="map"></div>

			<h2 class="m-b20 orange">Комментарий к заказу</h2>

			<div class="m-b30 comment-area">
				<textarea data-comment><?= !empty($_SESSION['DEFAULTS']['comment'])? $_SESSION['DEFAULTS']['comment'] : '' ?></textarea>
			</div>

			<h2 class="m-b20 orange">
				Итого к оплате
			</h2>
			<div class="sum"><?= $arResult['TOTAL_PRICE'] ?> руб.</div>
			<div class="apply">
				<div class="text">
					К сожалению, ваш адрес не входит в зону доставки наших ресторанов, <br>
					но вы можете сами забрать заказ в удобном для вас ресторане
				</div>
				<button class="btn pink big send-order" disabled="">Отправить</button>
			</div>
		</div>
	</div>
</div>

<? function phoneMask($phone) {
	echo $phone;
	$output = '+7 (';
	for($i = 0; $i < 12; $i++) {
		if($i == 3) {
			$output .= ') ';
		}
		if($i == 6) {
			$output .= ' ';
		}
		if($i == 8) {
			$output .= '-';
		}
		$output .= $phone[$i];
	}
	return $output;
}
?>