<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Заказ пиццы на дом, доставка по Москве и области"); ?>
	<div class="content">

		<!--Parallax for main page-->
		<ul id="parallax-bottom" class="parallax bottom">
			<li class="layer _1" data-depth="0.02">
				<div class="background"></div>
			</li>
			<li class="layer _2" data-depth="0.15">
				<div class="wrapper">
					<div class="background"></div>
				</div>
			</li>
		</ul>


		<div class="wrapper index product pizza main-style">

			<div class="intro">
				<span>итальянская</span>
				<div>Пицца</div>
				<span>на&nbsp;тонком&nbsp;тесте</span>
			</div>

			<? $APPLICATION->IncludeComponent("pronto24:catalog.section", '.default', Array('IBLOCK_ID' => 5)); ?>

			<? /*<div class="_txt-center">
				<button class="btn more">Смотреть все</button>
			</div>*/ ?>
		</div>
	</div>
	<div class="section">
		<div class="wrapper index">
			<div class="stock row">
				<? $APPLICATION->IncludeComponent("pronto24:stock.list", '.default', Array('BMAIN' => 'Y')); ?>
			</div>
			<div class="delivery">
				<div class="title">
					<span>Оперативная<br><i>доставка блюд</i></span>
				</div>
				<div class="row">
					<div class="col col_4">
						Бесплатная доставка
						(мы доставляем с 11:00 до 00:00)
					</div>
					<div class="col col_4">
						650 рублей — минимальная сумма заказа
					</div>
					<div class="col col_4">
						Оплата картой и наличными
					</div>
					<div class="col col_4">
						Мы отбираем свежие и качественные продукты
					</div>
				</div>
			</div>
		</div>
	</div>

	<div class="content last">
		<div class="wrapper index _txt-center">
			<div class="text">
				Открывая двери первого ресторана более 16 лет назад,
				мы ставили своей целью познакомить каждого посетителя
				с кухней солнечной Италии и подарить ему частичку
				этой чудесной страны.
			</div>
			<div class="intro">
				<span>сегодня</span>
				<div class="title-intro">пронто</div>
				<span>это:</span>
			</div>

			<div class="row about">
				<div class="col-about">
					cвыше <a href="/restaurants/">20 ресторанов</a>,
					расположенных в Москве,
					Московской области и регионах
				</div>
				<div class="col-about">
					оперативная <a href="/about/delivery/">доставка</a>
					пиццы, салатов, суши,
					горячих блюд и десертов
				</div>
			</div>
			<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "page",
					"AREA_FILE_SUFFIX" => "flag"
				)
			); ?>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>