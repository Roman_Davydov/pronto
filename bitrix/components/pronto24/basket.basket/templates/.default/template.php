<? if (empty($arResult['ROWS']))
	LocalRedirect('/'); ?>
<div class="content">
	<div class="wrapper basket product">
		<h2 class="big">Корзина</h2>
		<div class="basket-list">
			<? foreach ($arResult['ROWS'] as $item) : ?>
				<div class="basket-row <?= in_array($item['OUTER_ID'], $_SESSION['STOP_LIST']) ? 'error' : '' ?>">
					<div class="cell">
						<a href="<?= $item['DETAIL_PAGE_URL'] ?>/">
							<span class="wr-img">
								<img src="<?= $item['DETAIL_PICTURE'] ?>" alt="">
							</span>
							<span class="name"><?= $item['NAME'] ?></span>
							<? if ($item['IBLOCK_ID'] == 5) : ?>
								<span class="param"><?= $item['SIZE'] ?> см
									<? if (isset($item['PROPS'])) : ?>
										<br>+ <?= getModificatorStringFromPropsAndBasket($item['PROPS'], $arResult['ALL_PRODUCTS']); ?>
									<? endif ?>
								</span>
							<? endif ?>
						</a>
						<? if (in_array($item['OUTER_ID'], $_SESSION['STOP_LIST'])) : ?>
							<div class="error-text">
								К сожалению, данное блюдо недоступно для заказа сегодня. Выберите, пожалуйста, альтернативное блюдо в
								<a href="/">меню</a>.
							</div>
						<? endif ?>
					</div>
					<div class="cell">
						<div class="btn-group number-group">
							<button data-btn-number="" type="button" class="btn border btn-number basketCounter"
							        data-type="minus">
							</button>
							<input data-basket-product-id="<?= $item['ID'] ?>" data-input-number="" type="text"
							       class="btn border input-number"
							       value="<?= $item['QUANTITY'] ?>" min="1" readonly="">
							<button data-btn-number="" type="button" class="btn border btn-number basketCounter"
							        data-type="plus">

							</button>
						</div>
						<button data-basket-product-id="<?= $item['ID'] ?>" class="basket-del close-icon"></button>
						<div class="rub"><?= $item['PRICE'] ?> руб.</div>
					</div>
				</div>
			<? endforeach ?>
		</div>
		<? if ($arResult['GIFTS']) : ?>
			<div class="present">
				<div class="present-info">
					<div class="present-title">Ваш заказ участвует в акции:</div>
					<div class="present-text"><?= $arResult['GIFT_NAME']?></div>
					<div class="present-check">выберите подарок:</div>
				</div>

				<div class="present-list product-slider">

					<? foreach ($arResult['GIFTS'] as $item) : ?>
						<div class="item">
							<a target="_blank" href="<?= $item['DETAIL_PAGE_URL'] ?>" class="item-link <?= $item['CHOOSED'] ? 'select' : ''?>" data-check-present="<?=$item['ID']?>">
								<img src="<?= $item['DETAIL_PICTURE'] ?>" alt="">
								<div class="name"><?= $item['NAME'] ?></div>
								<button class="btn pink small"><span>выбрать</span><span class="_h">выбрано</span></button>
							</a>
						</div>
					<? endforeach ?>
					<div class="item" data-example style="display:none;">
						<a target="_blank" class="item-link" data-check-present>
							<img alt="">
							<div class="name"></div>
							<button class="btn pink small"><span>выбрать</span><span class="_h">выбрано</span></button>
						</a>
					</div>
				</div>
			</div>
		<? endif ?>
		<div class="row">
			<div class="promo content-form">
				<input type="text" class="coupon-txt" placeholder="ПРОМО-КОД">
				<button class="btn ok"></button>
			</div>
			<div class="basket-sum">
				<div class="total-info">
					<? if ($arResult['TOTAL_PRICE'] != $arResult['DISCOUNT_TOTAL_PRICE']) : ?>
						<span><?= $arResult['FORMATTED_TOTAL_PRICE'] ?></span>
						<? if (isset($arResult['COUPON_LIST'])) : ?>
							<? foreach ($arResult['COUPON_LIST'] as $coupon): ?>
								<span><?= $coupon['DISCOUNT_NAME'] ?></span>
							<? endforeach ?>
						<? endif ?>
						<span class="total basket-price"><?= $arResult['FORMATTED_DISCOUNT_TOTAL_PRICE'] ?></span>
					<? else : ?>
						<span class="total basket-price"><?= $arResult['FORMATTED_TOTAL_PRICE'] ?></span>
					<? endif ?>
				</div>
			</div>
		</div>
		<div class="apply">
			<div class="text">
				<b>Бесплатная доставка<? if ($arResult['TOTAL_PRICE'] < 650): ?> от 650 руб.<? endif ?></b>
				<? if ($arResult['TOTAL_PRICE'] < 650): ?>
					вам осталось заказать на сумму <span class="rest"><?= (650 - $arResult['TOTAL_PRICE']) ?></span> руб.
				<? endif ?>
			</div>
			<button data-order-go class="btn pink big">оформить заказ</button>
		</div>
		<? if (!empty($arResult['RECOMENDATIONS'])) : ?>
			<div class="title-slide">Добавьте к заказу:</div>
			<div class="product-slider" data-product-slider>
				<? foreach ($arResult['RECOMENDATIONS'] as $ID) : ?>
					<? $APPLICATION->IncludeComponent("pronto24:catalog.owllist.card", '.default', Array("ID" => $ID)); ?>
				<? endforeach ?>
			</div>
		<? endif ?>
	</div>
</div>