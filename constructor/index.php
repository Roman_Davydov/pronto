<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use Bitrix\Main\Page\Asset;


Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/pages/constructor.js');

$APPLICATION->SetTitle("Конструктор пиццы");
?>
	<div class="content">

		<ul id="parallax" class="parallax">

			<li class="layer" data-depth="0.03">
				<div class="wrapper">
					<div class="background _1"></div>
				</div>
			</li>
			<li class="layer" data-depth="0.03">
				<div class="wrapper">
					<div class="background _2"></div>
				</div>
			</li>

			<li class="layer" data-depth="0.08">
				<div class="wrapper">
					<div class="background _6"></div>
				</div>
			</li>
		</ul>

		<div class="wrapper index product pizza card constructor-wr">

			<div class="intro">
				конструктор пиццы
			</div>

			<div class="row">
				<div class="col col_2 constructor-img" data-constructor-view>
					<img src="/bitrix/templates/pronto/images/constr-bg.png" alt="">
				</div>
				<div class="col col_2 _p5">
					<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
							"AREA_FILE_SHOW" => "page",
							"AREA_FILE_SUFFIX" => "pizza_maker_text"
						)
					); ?>
					<div class="set pizza">
						<div class="rub"><span class="pizza_sum">0</span> руб.</div>
						<div class="btn-group row">
							<button class="btn border set-btn" data-constructor-refreshed>
								собрать заново
							</button>
							<button data-cnt="1" class="btn pink set-btn basket-add">
								в корзину
							</button>
						</div>
					</div>
				</div>
			</div>

			<div class="title-slide">Добавьте ингредиенты:</div>
			<ul class="nav-tabs" role="tablist">
				<? $APPLICATION->IncludeComponent('pronto24:constructor.modificators', "types"); ?>
			</ul>

			<div class="tab-content">
				<? $APPLICATION->IncludeComponent('pronto24:constructor.pizza'); ?>
				<? $APPLICATION->IncludeComponent('pronto24:constructor.modificators'); ?>
			</div>

			<div class="constructor-list">
				<div class="section">
					<a class="collapse-icon" data-toggle="collapse" href="#list"></a>
					<h1>Моя пицца</h1>
				</div>
				<div class="collapse in" id="list">
					<div class="section br first" data-constructor-base>
						<div class="icon-first _ic"></div>
						<p>Основа</p>
					</div>
					<div class="section last" data-constructor-list>
						<div class="icon-last _ic"></div>
						<p>Ингредиенты</p>
					</div>
				</div>
			</div>
			<div class="total"><span class="pizza_sum">0</span> руб.</div>

			<div class="apply">
				<button data-cnt="1" class="btn pink set-btn basket-add">
					в корзину
				</button>
				<div class="set pizza for-mobile">
					<div class="btn-group row">
						<button class="btn border set-btn" data-constructor-refreshed="">
							собрать заново
						</button>
						<button data-cnt="1" class="btn pink set-btn basket-add">
							в корзину
						</button>
					</div>
				</div>
			</div>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>