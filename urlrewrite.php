<?
$arUrlRewrite = array(
	array(
		"CONDITION" => "#^/bitrix/services/ymarket/#",
		"RULE" => "",
		"ID" => "",
		"PATH" => "/bitrix/services/ymarket/index.php",
	),
	array(
		"CONDITION" => "#^/restaurants/#",
		"RULE" => "code=\$1",
		"ID" => "",
		"PATH" => "/restaurants/card.php",
	),
	array(
		"CONDITION" => "#^/dessert/#",
		"RULE" => "code=\$1",
		"ID" => "",
		"PATH" => "/dessert/card.php",
	),
	array(
		"CONDITION" => "#^/snacks/#",
		"RULE" => "code=\$1",
		"ID" => "",
		"PATH" => "/snacks/card.php",
	),
	array(
		"CONDITION" => "#^/stocks/#",
		"RULE" => "code=\$1",
		"ID" => "",
		"PATH" => "/stocks/card.php",
	),
	array(
		"CONDITION" => "#^/drink/#",
		"RULE" => "code=\$1",
		"ID" => "",
		"PATH" => "/drink/card.php",
	),
	array(
		"CONDITION" => "#^/forms/#",
		"RULE" => "action=\$1",
		"ID" => "",
		"PATH" => "/forms/index.php",
	),
	array(
		"CONDITION" => "#^/paste/#",
		"RULE" => "code=\$1",
		"ID" => "",
		"PATH" => "/paste/card.php",
	),
	array(
		"CONDITION" => "#^/pizza/#",
		"RULE" => "code=\$1",
		"ID" => "",
		"PATH" => "/pizza/card.php",
	),
	array(
		"CONDITION" => "#^/sushi/#",
		"RULE" => "code=\$1",
		"ID" => "",
		"PATH" => "/sushi/card.php",
	),
	array(
		"CONDITION" => "#^/soup/#",
		"RULE" => "code=\$1",
		"ID" => "",
		"PATH" => "/soup/card.php",
	),
	array(
		"CONDITION" => "#^/hot/#",
		"RULE" => "code=\$1",
		"ID" => "",
		"PATH" => "/hot/card.php",
	),
);

?>