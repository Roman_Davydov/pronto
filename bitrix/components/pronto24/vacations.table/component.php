<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');

$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 1), false, false, Array('ID', 'NAME'));
$arOccupations = Array();
while ($arElement = $res->GetNext()) {
	$arOccupations[$arElement['ID']] = $arElement['NAME'];
}

$res = CIBlockElement::GetList(Array(), Array("IBLOCK_ID" => 3), false, false, Array('ID', 'PROPERTY_METRO'));
$arPlaces = Array();
while ($arElement = $res->GetNext()) {
	$arPlaces[$arElement['ID']] = $arElement['PROPERTY_METRO_VALUE'];
}

$res = CIBlockElement::GetList(Array(), Array("ACTIVE" => "Y", "IBLOCK_ID" => 4), false, false, Array('PROPERTY_OCCUPATION', 'PROPERTY_URGENT', 'PROPERTY_PLACE'));

$arStrings = Array();

while ($arElement = $res->GetNext()) {
	$arStrings[$arElement['PROPERTY_OCCUPATION_VALUE']][] =
		Array('URGENT' => ($arElement['PROPERTY_URGENT_VALUE']=='Y'? true:false), 'PLACE' => $arElement['PROPERTY_PLACE_VALUE']);
}

foreach($arStrings as $key => $value) {
	foreach($value as $place) {
		$arResult[$arOccupations[$key]][] = Array('URGENT' => $place['URGENT'], 'PLACE' => $arPlaces[$place['PLACE']]);
	}
}

$this->IncludeComponentTemplate();