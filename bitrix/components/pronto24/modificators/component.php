<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

$arProperties = Array(
	'IBLOCK_ID',
	'ID',
	'NAME',
	'PROPERTY_OUTER_ID'
);

$arFilters = Array("IBLOCK_ID" => 26, "ACTIVE" => "Y");

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilters, false, false, $arProperties);

while ($arElement = $res->GetNext()) {
	if(empty($_SESSION['STOP_LIST']) || (isset($_SESSION['STOP_LIST']) && !in_array($arElement['PROPERTY_OUTER_ID_VALUE'], $_SESSION['STOP_LIST']))) {
		$arElement['PRICE'] = getPrice($arElement['ID']);
		$arResult[] = $arElement;
	}
}

$this->IncludeComponentTemplate();