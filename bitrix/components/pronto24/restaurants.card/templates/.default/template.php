<div class="restaurant-list">
	<div class="res-sep">
		<? if (isset($arResult['METRO'])) : ?>
			<p>
				<?= implode('<br>', $arResult['METRO']); ?>
			</p>
		<? endif ?>
		<p>
			<?= $arResult['LOCATION'] ?>
		</p>
		<p>
			Ресторан:<br>
			<?= implode('<br>', $arResult['WORKING_TIME_RESTAURANT']); ?>
		</p>
		<? if (!empty($arResult['WORKING_TIME_DELIVERY'])): ?><p>
			Доставка:<br>
			<?= implode('<br>', $arResult['WORKING_TIME_DELIVERY']); ?>
			</p><? endif ?>
		<p>
			<?= implode('<br>', $arResult['PHONE']); ?>
		</p>
		<? if (isset($arResult['PHOTOS'])) : ?>
			<div class="_img">
				<? foreach ($arResult['PHOTOS'] as $photo) : ?>
					<p>
						<img src="<?= $photo ?>">
					</p>
				<? endforeach ?>
			</div>
		<? endif ?>
	</div>
	<? if (isset($arResult['METRO'])) : ?>
		<? foreach ($arResult['METRO_COLOR'] as $color) : ?>
			<div style="background:#<?= $color ?>!important" class="line"></div>
		<? endforeach ?>
	<? endif ?>
</div>

<script>
	function points() {
		var points = [
			{geo: [<?= $arResult['GEO'] ?>]}
		]
		return points;
	}

	function center() {
		return [<?= $arResult['GEO'] ?>];
	}

	function zoom() {
		return 10;
	}
</script>