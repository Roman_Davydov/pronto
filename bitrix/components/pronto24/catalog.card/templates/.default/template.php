<div class="row">
	<div class="col col_2 product-img">
		<img src="<?= $arResult['SRC'] ?>" alt="">
	</div>
	<div data-product-card class="col col_2 _p5">
		<?= !empty($arResult['~HTML_TITLE']) ? html_entity_decode($arResult['~HTML_TITLE']) : '<span class="title">'.$arResult['NAME'].'</span>' ?>
		<div class="text-intro"><?= $arResult['DESCRIPTION']; ?></div>
		<div class="set row">
			<div class="set-col">
				<div class="gr"><?= $arResult['WEIGHT'] ?></div>
				<div class="btn-group number-group">
					<button data-btn-number="" type="button" class="btn border btn-number" data-type="minus"></button>
					<input data-input-number="" type="text" name="" class="btn border input-number" value="1" min="1"
					       readonly="">
					<button data-btn-number="" type="button" class="btn border btn-number" data-type="plus"></button>
				</div>
			</div>
			<div class="set-col">
				<div class="rub"><?= $arResult['PRICE'] ?> руб.</div>
				<button data-cnt="1" data-product-id="<?= $arResult['ID'] ?>" class="btn pink small basket-add">
					в корзину
				</button>
			</div>
		</div>
	</div>
</div>
<? if (!empty($arResult['RECOMENDATIONS'])) : ?>
	<div class="title-slide">Добавьте к заказу:</div>
	<div class="product-slider" data-product-slider>
		<? foreach ($arResult['RECOMENDATIONS'] as $ID) : ?>
			<? $APPLICATION->IncludeComponent("pronto24:catalog.owllist.card", '.default', Array("ID" => $ID)); ?>
		<? endforeach ?>
	</div>
<? endif ?>