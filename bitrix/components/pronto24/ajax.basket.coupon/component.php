<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$coupon = htmlspecialchars($_POST["coupon"]);

$result = Array('result' => 'error');
if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
	$couponResult = CCatalogDiscountCoupon::SetCoupon($coupon);
	if ($couponResult) {
		$result = Array('result' => 'ok', 'basket' => $_SESSION['basket_line']);
	}
}

return $result;