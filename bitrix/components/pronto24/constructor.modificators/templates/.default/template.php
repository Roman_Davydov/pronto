<? foreach ($arResult as $key => $type) : ?>
<div class="tab-pane" id="tab-<?=$type['SORT']?>">
	<div class="title-set"><?=$key?></div>
	<div class="product-slider" data-constructor-slider>
		<? foreach ($type['ELEMENTS'] as $element) : ?>
			<div class="item">
				<a href="#" class="item-link" data-element="modificator" data-view-img="<?=$element['MORE_PICTURES']?>" data-constructor-change="<?= $element['ID']?>"
				   data-text="<?= $element['NAME']?>" data-rub="<?= $element['PRICE'] ?>">
					<img src="<?= $element['PICTURE'] ?>" alt="">
					<div class="name"><?= $element['NAME'] ?></div>
				</a>
				<div class="btn-group number-group">
					<button data-btn-constructor="<?= $element['ID']?>" type="button" class="btn border btn-number"
					        data-type="minus">
					</button>
					<input data-input-constructor="<?= $element['ID']?>" type="text" name="" class="btn border input-number"
					       value="1" min="1" readonly="">
					<button data-btn-constructor="<?= $element['ID']?>" type="button" class="btn border btn-number"
					        data-type="plus">
					</button>
				</div>
				<div style="margin-top:8px;"><?= $element['WEIGHT'] ?> гр.</div>
				<div class="rub"><span class="price"><?= $element['PRICE'] ?></span> руб.</div>
			</div>
		<? endforeach ?>
	</div>
</div>
<? endforeach ?>