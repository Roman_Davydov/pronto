'use strict';

function DeliveryManager_f() {
	this.polygons;

	this.init = function () {
		DeliveryManager.polygons = restPolygons();

		ymaps.ready(function() {
			var myMap = new ymaps.Map('map', {center: [55.76, 37.64], zoom: 9});
			myMap.controls.add(
				new ymaps.control.ZoomControl()
			);
			for (key in DeliveryManager.polygons) {
				for (var zone in DeliveryManager.polygons[key]) {
					var myPolygon = new ymaps.Polygon([
						DeliveryManager.polygons[key][zone].polygon
					], {hintContent: DeliveryManager.returnHint(key, zone, DeliveryManager.polygons[key][zone].sum_min)}, {
						fillColor: DeliveryManager.color(DeliveryManager.polygons[key][zone]),
						strokeColor: '#0000FF',
						opacity: 0.6,
						strokeWidth: 1
					});
					myMap.geoObjects.add(myPolygon);
				}
			}
		});
	};

	this.color = function(data) {
		if (data.sum_min == 1000)
			return '#e63131';
		if (data.time_min == 60 && data.time_max == 60)
			return '#8ea14e';
		return '#e4b100';
	};

	this.returnHint = function(key, zone, min_sum) {
		if(!test()) {
			return null;
		}
		else {
			return restaurants(key) + ' / Зона: ' + zone + ' / Сумма: ' + min_sum;
		}
	}
}