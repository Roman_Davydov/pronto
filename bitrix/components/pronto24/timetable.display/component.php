<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arWeekDays = Array(
	0 => "Пн",
	1 => "Вт",
	2 => "Ср",
	3 => "Чт",
	4 => "Пт",
	5 => "Сб",
	6 => "Вс"
);

$arNewDays = Array();

foreach ($arParams['timetable'] as $day => $time) {
	if (empty($arNewDays[$time])) {
		$arNewDays[$time] = Array($day);
	} else {
		$arNewDays[$time][] = $day;
	}
}
if (count($arNewDays) == 1) {
	foreach ($arNewDays as $time => $days) {
		$splTime = explode('-', $time);
		$arResult[] = 'Ежедневно с ' . $splTime[0] . ' до ' . $splTime[1];
	}
} else {
	foreach ($arNewDays as $time => $days) {
		if (count($days) == 1) {
			$strDays = $arWeekDays[$days[0]];
		} else {
			$bSeq = true;
			$bReverse = false;
			foreach($days as $day) {
				if($bSeq) {
					if(!isset($dayBefore)) {
						$dayBefore = $day;
					}
					else {
						if($day == $dayBefore + 1 || ($day == 6 && $days[0] == 0)) {
							if($day == 6 && $days[0] == 0) {
								$bReverse = true;
							}
							$dayBefore = $day;
						}
						else {
							$bSeq = false;
						}
					}
				}
			}
			if($bSeq) {
				if(!$bReverse) {
					$strDays = $arWeekDays[$days[0]] . '-' . $arWeekDays[array_pop($days)];
				}
				else {
					$strDays = $arWeekDays[array_pop($days)] . '-' . $arWeekDays[array_pop($days)];
				}
			}
			else {
				$strDays = str_replace(array_keys($arWeekDays), $arWeekDays, implode(',', $days));
			}
		}

		$splTime = explode('-', $time);
		$arResult[] = $strDays . ' с ' . $splTime[0] . ' до ' . $splTime[1];
	}
}

//Additional
CModule::IncludeModule('iblock');

$arProperties = Array(
	'ID',
	'IBLOCK_ID',
	'NAME',
);
$CODE = explode('?', $_GET['code']);
$res = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_CODE" => 'additional_timetable'), false, false, $arProperties);

$arAdditional = Array();
while($obElement = $res->GetNextElement()) {
	$arElement = $obElement->fields;
	foreach($obElement->GetProperties() as $key => $property) {
		if($key == 'TYPE') {
			$arElement[$key] = $property['VALUE_XML_ID'];
		}
		else
			$arElement[$key] = $property['VALUE'];

		if($key == "RESTAURANTS") {
			if(!empty($property['VALUE'])) {
				$bRestaurant = false;
				foreach($property['VALUE'] as $value) {
					if($arParams['ID'] == $value)
						$bRestaurant = true;
				}
			}
			else {
				$bRestaurant = true;
			}
		}
	}

	if($bRestaurant && $arElement['TYPE'] == $arParams['TYPE']) {
		$diff = strtotime($arElement['DATE']) - time();
		if(($diff > 0 && $diff < 60 * 60 * 24 * 7) || ($diff < 0 && $diff > -1 * 60 * 60 * 24)) {
			$arResult[] = '<span style="color:#e63131">' . $arElement['DATE'] . ' ' . $arElement['NAME'] . '<br>' . $arElement['ADDITIONAL_TIME'].'</span>';
		}
	}
}

return $arResult;