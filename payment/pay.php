<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

$paySystem = $_GET['pay_system'];

if(isset($paySystem) && is_numeric($paySystem)) {
	$APPLICATION->IncludeComponent(
		"bitrix:sale.order.payment",
		"",
		Array(
			"PAY_SYSTEM_ID" => $paySystem,
			"PERSON_TYPE_ID" => "1"
		)
	);
}

require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/epilog_after.php");?>