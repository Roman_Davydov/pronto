<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

DEFINE('ordersOnPage', 3);

CModule::IncludeModule('sale');
CModule::IncludeModule('iblock');

global $USER;

$arFilter = Array(
	"USER_ID" => $USER->GetID(),
	'STATUS_ID' => Array('N', 'F')
);

$db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), $arFilter);

while ($ar_sales = $db_sales->Fetch()) {
	$arDateTime = explode(' ', $ar_sales['DATE_INSERT']);
	$arDate = explode('.', $arDateTime[0]);
	$arOrder = Array('ID' => $ar_sales['ID'], 'DATE' => $arDate[0] . '.' . $arDate[1] . '.' . substr($arDate[2], 2, 2), 'PRICE' => $ar_sales['PRICE'], 'STATUS' => $ar_sales['STATUS_ID']);

	$dbBasketItems = CSaleBasket::GetList(array(
		"NAME" => "ASC"
	),
		array(
			"LID" => SITE_ID,
			"ORDER_ID" => $ar_sales['ID']
		),
		false,
		false,
		array("ID", "NAME", "QUANTITY", "PRODUCT_ID")
	);

	$arItems = Array();
	$arBasketItems = Array();
	while ($arItem = $dbBasketItems->Fetch()) {
		$res = CIBlockElement::GetList(Array(), Array('ID' => $arItem['PRODUCT_ID']), false, false, Array('ID', 'IBLOCK_ID', 'PROPERTY_LINK_TO', 'PROPERTY_SIZE'));
		$arElement = $res->GetNext();
		$arIDsInBasket[] = $arElement['ID'];
		$arItem['IBLOCK_ID'] = $arElement['IBLOCK_ID'];
		$arItem['SIZE'] = $arElement['PROPERTY_SIZE_VALUE'];

		if ($arItem['IBLOCK_ID'] != 26) {
			$db_res = CSaleBasket::GetPropsList(
				array(
					"SORT" => "ASC",
					"NAME" => "ASC"
				),
				array("BASKET_ID" => $arItem['ID'])
			);
			while ($ar_res = $db_res->Fetch()) {
				if ($ar_res['NAME'] == 'MODIFICATOR') {
					$arItem['PROPS'][] = $ar_res;
				}
			}
		}

		$arItems[$arItem['ID']] = $arItem;

		if ($arItem['IBLOCK_ID'] != 26) {
			$arBasketItems[] = $arItem;
		}
	}
	$arOrder['ALL_PRODUCTS'] = $arItems;
	$arOrder['ROWS'] = $arBasketItems;

	$arResult[] = $arOrder;
}

$this->IncludeComponentTemplate();