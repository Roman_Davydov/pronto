<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$arProperties = Array(
	'ID',
	'CODE',
	'DETAIL_PICTURE',
	'IBLOCK_ID',
	'PROPERTY_OUTER_ID',
	'NAME'
);

$arFilters = Array("IBLOCK_CODE" => 'modificators');

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilters, false, false, $arProperties);

while ($obElement = $res->GetNextElement()) {
	$arElement = $obElement->fields;
	$arProps = $obElement->GetProperties();
	foreach ($arProps as $key => $property) {
		$arElement[$key] = $property['VALUE'];
		if ($key == 'TYPE')
			$sort = $property['VALUE_SORT'];
	}
	if(empty($_SESSION['STOP_LIST']) || (isset($_SESSION['STOP_LIST']) && !in_array($arElement['PROPERTY_OUTER_ID_VALUE'], $_SESSION['STOP_LIST']))) {
		$picture = CFile::GetFileArray($arElement["DETAIL_PICTURE"]);
		$arElement["PICTURE"] = $picture['SRC'];
		$picture2 = CFile::GetFileArray($arElement["PICTURE_CONSTRUCTOR"]);
		$arElement["MORE_PICTURES"] = $picture2['SRC'];
		$arElement['PRICE'] = getPrice($arElement['ID']);
		$arResult[$arElement['TYPE']]['SORT'] = $sort + 1;
		$arResult[$arElement['TYPE']]['ELEMENTS'][] = $arElement;
	}
}

$this->IncludeComponentTemplate();