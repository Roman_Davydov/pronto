<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('subscribe');

global $USER;
$userID = $USER->GetID();

if (is_numeric($userID)) {
	$result = array('result' => "ok");
	$obSubscr = CSubscription::GetList(
		array("ID" => "ASC"),
		array("ACTIVE" => "Y", "1USER_ID" => $userID)
	);
	$arSubscr = $obSubscr->Fetch();

	$arResult['EMAIL'] = $USER->GetEmail();
	$arEmailSplited = explode('@', $arResult['EMAIL']);

	if($arEmailSplited['1'] == 'pronto24.ru') {
		$result = array('result' => "error");
	}
	else {
		if ($_POST['subscribe']) {
			if (empty($arSubscr)) {
				$arFields = Array(
					"USER_ID" => $userID,
					"EMAIL" => $USER->GetEmail(),
					"ACTIVE" => "Y",
					"CONFIRMED" => "Y",
					"SEND_CONFIRM" => "N",
					"RUB_ID" => 1
				);
				$subscr = new CSubscription;

				$IDADD = $subscr->Add($arFields);

				$result['email'] = $USER->GetEmail();
			}
		} else {
			$res = CSubscription::Delete($arSubscr['ID']);
		}
	}
} else {
	$result = array('result' => "error");
}
return $result;