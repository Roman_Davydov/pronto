'use strict';

function ConstructorManager_f() {
	this.init = function () {
		$('[data-constructor-slider]').each(function () {
			var length = $(this).find('.item').length;
			var nav;
			if (length > 6)
				nav = true;
			else
				nav = false;

			$(this).owlCarousel({
				nav: nav,
				items: 6,
				dots: false,
				navRewind: false,
				pullDrag: false,
				navText: ['', ''],
				responsive: {
					0: {
						items: 2
					},
					640: {
						items: 2
					},
					700: {
						items: 4
					},
					930: {
						items: 6
					}
				}
			});
		});

		$(document)
			.on("click", "[data-constructor-change]", ConstructorManager.onClickElement)
			.on("click", "[data-btn-constructor]", ConstructorManager.onChange)
			.on("click", "[data-constructor-clear]", ConstructorManager.onClearMod)
			.on("click", "[data-constructor-refreshed]", ConstructorManager.onFullRefresh)
		ConstructorManager.add($('[data-element]:first').attr('data-constructor-change'), 'pizza');
		$('[data-element]:first').addClass('active');
	};

   	this.onFullRefresh = function () {
		$('[data-constructor-change]').removeClass('active');
		$('[data-constructor-img]').remove();
		$('[data-constructor-row]').remove();
		ConstructorManager.add($('[data-element]:first').attr('data-constructor-change'));
		ConstructorManager.calculate();
	};

	this.onClearMod = function (e) {
		e.preventDefault();
		var id = $(this).attr('data-constructor-clear');
		ConstructorManager.clear(id);
	};

	this.onChange = function (e) {
		e.preventDefault();
		var type = $(this).attr('data-type');
		var id = $(this).attr('data-btn-constructor');
		var input = $('[data-input-constructor="' + id + '"]');
		var currentVal = parseFloat(input.val());
		var max = parseFloat(input.attr('max'));
		var min = parseFloat(input.attr('min'));
		if (!currentVal || currentVal == "" || currentVal == "NaN") currentVal = 0;
		if (max == "" || max == "NaN") max = '';
		if (min == "" || min == "NaN") min = 0;
		if (type == 'plus') {
			if (max && (max == currentVal || currentVal > max)) {
				input.val(max);
			} else {
				input.val(currentVal + 1);
			}
		} else if (type == 'minus') {
			if (min && (min == currentVal || currentVal < min)) {
				input.val(min);
				$(this).closest('.item').find('.item-link').removeClass('active');
			} else if (currentVal > 0) {
				input.val(currentVal - 1);
			}
		}
		ConstructorManager.calculate();
	};

	this.returnPizzaId = function () {
		return $('div.constructor-list .section.first .row').attr('data-constructor-row');
	};

	this.returnModificators = function () {
		var arModificatorsValues = new Array;
		$('div.section.last div.row').each(function () {
			var mod = {'id': $(this).attr('data-constructor-row'), 'count': $(this).find('.input-number').val()};
			arModificatorsValues.push(mod);
		});
		return arModificatorsValues;
	};

	this.clear = function (id) {
		$('[data-constructor-change="' + id + '"]').removeClass('active');
		$('[data-constructor-img="' + id + '"]').remove();
		$('[data-constructor-row="' + id + '"]').remove();
		ConstructorManager.calculate();
	};

	this.add = function (id, type) {
		var viewSrc = $('[data-constructor-change=' + id + ']').attr('data-view-img');

		if (type != 'mod') {
			var zindex = 1;
		}
		else {
			var zindex = 3;
			$('.view-img').each(function () {
				if (zindex < $(this).css('z-index')) {
					zindex = $(this).css('z-index');
				}
			});
			zindex++;
		}

		$('[data-constructor-view]').append('<img style="z-index:' + zindex + ';" class="view-img" src="' + viewSrc + '" data-constructor-img="' + id + '">');

		var el = $('[data-constructor-change=' + id + ']');
		var text = el.attr('data-text');
		var rub = el.attr('data-rub');
		var html =
			'<div class="row" data-constructor-row="' + id + '">' +
			'<div class="col col_2">' +
			'<div class="name">' + text + '</div>' +
			'</div>' +
			'<div class="col col_2 _r">' +
			'<div class="rub"><span class="price">' + rub + '</span> руб.</div>' +
			'<div class="close">' +
			'<button data-constructor-clear="' + id + '" class="close-icon"></button>' +
			'</div>' +
			'<div class="btn-group number-group">' +
			'<button data-btn-constructor="' + id + '" type="button" class="btn border btn-number" data-type="minus"></button>' +
			'<input data-input-constructor="' + id + '" type="text" name="" class="btn border input-number" value="1" min="1" readonly="">' +
			'<button data-btn-constructor="' + id + '" type="button" class="btn border btn-number" data-type="plus"></button>' +
			'</div>' +
			'</div>' +
			'</div>';

		if ($('[data-constructor-change=' + id + ']').attr('data-element') == 'pizza') {
			$('[data-constructor-base]').append(html);
		} else {
			$('[data-constructor-list]').append(html);
		}

		ConstructorManager.calculate();
	};

	this.calculate = function () {
		var allSumm = 0;

		$('.price').each(function () {
			var cnt = $(this).closest('.col_2._r').find('.input-number').val();
			if (cnt > 0) {
				allSumm += $(this).text() * cnt;
			}
		});

		$('.pizza_sum').text(allSumm);
	};

	this.onClickElement = function (e) {
		e.preventDefault();
		var $this = $(this);
		var id = $this.attr('data-constructor-change');
		if (!$this.hasClass('active')) {
			$this.addClass('active');
			var type = 'mod';
			if ($this.attr('data-element') == "pizza") {
				var type = 'pizza';
				$('[data-element=pizza]').each(function () {
					if (id != $(this).attr('data-constructor-change')) {
						ConstructorManager.clear($(this).attr('data-constructor-change'));
					}
				});
			}
			ConstructorManager.add(id, type);
		}
	};
}