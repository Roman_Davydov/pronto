<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/pages/children.js');
$APPLICATION->SetTitle("Десткое меню");
?>
	<div class="content children product children-style">

		<div class="section">
			<div class="intro">
				детское меню
			</div>
			<p style="text-align:center">*Детское меню: подается только в ресторанах не распространяется на доставку</p>
			<div class="wrapper">
				<div class="children-slider" data-children-slider>
					<? $APPLICATION->IncludeComponent("pronto24:catalog.section", 'children', Array('IBLOCK_ID' => 12, 'POSITION' => 1)); ?>
				</div>
			</div>
		</div>

		<div class="section blue">
			<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" version="1.1" width="100%" height="72px" viewBox="0 0 1978.7 98.6" >
				<path class="st0" d="M0,0v98.6h1978.7v-26C1323.6-148.9,656.8,254.9,0,0z"/>
			</svg>

			<div class="wrapper">
				<div class="children-slider" data-children-slider>
					<? $APPLICATION->IncludeComponent("pronto24:catalog.section", 'children', Array('IBLOCK_ID' => 12, 'POSITION' => 2)); ?>
				</div>
			</div>

		</div>

		<div class="section purple">
			<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" version="1.1" width="100%" height="72px" viewBox="0 0 1978.7 98.6" >
				<path class="st0" d="M0,72.6l0,26h1978.7l0-98.6C1321.9,254.9,655.1-148.9,0,72.6z"/>
			</svg>

			<div class="wrapper">
				<div class="children-slider" data-children-slider>
					<? $APPLICATION->IncludeComponent("pronto24:catalog.section", 'children', Array('IBLOCK_ID' => 12, 'POSITION' => 3)); ?>
				</div>
			</div>

		</div>

		<div class="section green">
			<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" version="1.1" width="100%" height="72px" viewBox="0 0 1978.7 98.6" >
				<path class="st0" d="M0,0v98.6h1978.7v-26C1323.6-148.9,656.8,254.9,0,0z"/>
			</svg>

			<div class="wrapper">
				<div class="children-slider single" data-children-slider-single>
					<? $APPLICATION->IncludeComponent("pronto24:catalog.section", 'children', Array('IBLOCK_ID' => 12, 'POSITION' => 4)); ?>
				</div>
			</div>
		</div>

		<div class="section white">
			<svg xmlns="http://www.w3.org/2000/svg" preserveAspectRatio="none" version="1.1" width="100%" height="72px" viewBox="0 0 1978.7 98.6" >
				<path class="st0" d="M0,72.6l0,26h1978.7l0-98.6C1321.9,254.9,655.1-148.9,0,72.6z"/>
			</svg>

			<div class="wrapper">
				<div class="_txt-center">
					<h3>PRONTO PIZZA рада Вас приветствовать в наших кафе!</h3>
					<p>
						Вы прекрасно сможете отдохнуть и насладиться изысканной итальянской кухней. <br>
						Во время Вашего отдыха ваше чадо будет проводить время с пользой и удовольствием в "Pronto Kids club"!
					</p>
					<p>
						Каждые выходные, в субботу и воскресенье вас ждут детские праздники с разной тематикой и задумкой.
						К нам приходят сказочные герои из популярных современных мультиков и сказок. Запутанные приключения, волшебные испытания, конкурсы и фокусы – всё, что обожают дети, будет их ждать в «Пронто» по выходным –
						приходите сами и приводите своих друзей!
					</p>
				</div>
				<div class="grid-box">
					<div class="row">
						<div class="col col_3c">
							<p class="p-color big">День рождения в кафе «Пронто» </p>
							<p>
								С детским клубом в «Пронто» вы можете больше не ломать голову, где провести день рождения вашего ребенка . У нас есть различные программы, ориентированные на возраст гостей и их вкусовые пристрастия. Душевные аниматоры, радушный прием, музыка, интересные конкурсы, аквагрим, моделирование из шаров – всё это входит в программу детского праздника в кафе «Пронто». Дополнительно Вашему вниманию можем предложить кукольный театр, ростовых кукол, оформление шарами, фокусника, шоу мыльных пузырей, веселых сказочных героев в мастерской волшебника их более 300.
								А ещё – у нас постоянно проходят акции, которые непременно вас порадуют!
							</p>
							<p>
								При заказе детского дня рождения в «Пронто» на будний день,  при заказе любого из четырех праздничных пакетов – <span class="s-color">шар Сюрприз №1 в подарок!</span> Это фейерверк из мелких воздушных шариков и конфетти, который доставляет детям шквал эмоций!
							</p>
							<p>
								Для наших гостей мы всегда готовы предложить бонусы и подарки ко дню рождения.
								Все эти приятные моменты и детали праздничных программ уточняйте у администраторов.
							</p>
						</div>
						<div class="col col_3">
							<p class="p-color big">Школа юного Пиццайоло и кондитера</p>
							<p>
								Каждую Субботу мы ждем вас и вашего ребенка в "Школе юного Пиццайоло и кондитера"! Это увлекательные кулинарные уроки для самых маленьких и их родителей. Ваш ребенок приготовит СВОЮ, уникальную пиццу и другие блюда! Ему выдадут одежду настоящего Пиццайоло! Все уроки проводятся весело и легко - ребенок не будет скучать и станет настоящим детским поваром, его даже наградят дипломом! Для этого нужно посетить весь "курс" кулинарных мастер-классов, состоящий из 5 уроков.
							</p>
							<p>
								Подробности  и стоимость уточняйте.
							</p>
						</div>
					</div>
				</div>

				<div class="_txt-center small">
					<p class="p-color big">Чем мы порадуем Вас в будние дни?</p>
					<p>
						Интересными и увлекательными занятиями для детей от 2 лет! <br>
						Пока родители наслаждаются нашей итальянской кухней, дети осваивают следующие направления:
					</p>
				</div>

				<div class="row small m-b20">
					<div class="col col_2">
						<b>Набор в группу детей от 3-6 лет:</b>
						<ol>
							<li>Общее развитие (обогащение словарного запаса ребенка
								в игровой форме, а также развитие коммуникативных навыков).</li>
							<li>Английский язык для детей ( в игровой форме).</li>
							<li>Творчество (развитие творческих способностей, воображения).</li>
							<li>Песочная анимация ( развитие мелкой моторики от 3 лет).</li>
							<li>Школа Юного поваренка.</li>
							<li>Беби фитнес - комплекс упражнений на равновесие,
								координацию движений и развитие внимания.</li>
						</ol>
					</div>
					<div class="col col_2">
						<b>Набор в группу детей от 5-7 лет</b>
						<ol>
							<li>Интересная подготовка к школе (чтение, логика, окружающий
								мир, в игровой форме).</li>
							<li>Беби театр  - театральная студия ( развитие воображения,
								внимания, памяти).</li>
							<li>Песочная анимация.</li>
							<li>Гончарная мастерская.</li>
							<li>Беби - фитнес( хореография).</li>
							<li>Школа юного поваренка.</li>
						</ol>
					</div>
				</div>

				<div class="_txt-center">
					<p class="small m-b40">В каждой группе не более 4-5 человек. Занятия платные. Расписание и цену уточняйте у администраторов ресторана.</p>
					<h3>
						Мы делаем всё для того, <br>
						чтобы вам захотелось прийти к нам снова и снова!
					</h3>
				</div>
			</div>
		</div>

		<div class="section _txt-center orange">
			<div class="wrapper">
				<h3 class="m-b50">Детский игровой центр есть в данных ресторанах:</h3>
				<div class="res-list m-b20">
					<? $APPLICATION->IncludeComponent("pronto24:restaurants.children.list"); ?>
				</div>
				<h3 class="_l"><a href="/restaurants/">Все рестораны «Пронто»</a></h3>
			</div>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>