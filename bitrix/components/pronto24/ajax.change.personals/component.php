<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

global $USER;

$name = isset($_POST['name']) ? $_POST['name'] : false;
$lastName = isset($_POST['lastName']) ? $_POST['lastName'] : false;
$email = isset($_POST['email']) ? $_POST['email'] : false;
$birthday = isset($_POST['birthday']) ? $_POST['birthday'] : false;
$arBirthday = explode('.', $birthday);

$result = array('result' => 'error');

$fields = Array(
	"NAME" => $name,
	"LAST_NAME" => $lastName,
	"EMAIL" => $email
);

if(checkdate($arBirthday[1], $arBirthday[0], $arBirthday[2])) {
	$fields['PERSONAL_BIRTHDAY'] = $arBirthday[0].'.'.$arBirthday[1].'.'.$arBirthday[2];
}

$result = $USER->Update($USER->GetID(), $fields);
if ($USER->LAST_ERROR == '') {
	$result = array('result' => 'ok');
}

return $result;