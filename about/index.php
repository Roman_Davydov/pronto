<?
require($_SERVER["DOCUMENT_ROOT"]."/bitrix/header.php");
$APPLICATION->SetTitle("О компании");
?>
	<div class="content about-wr">
		<div class="wrapper index about _txt-center">

			<h1 class="title">пронто</h1>
			<h2 class="big for-mobile">Пронто</h2>

			<div class="sort m-b50">
				<? $APPLICATION->IncludeComponent("pronto24:menu.about"); ?>
			</div>

			<? $APPLICATION->IncludeComponent("bitrix:main.include", "", Array(
					"AREA_FILE_SHOW" => "page",
					"AREA_FILE_SUFFIX" => "about_us",
					"EDIT_TEMPLATE" => "standard.php"
				)
			); ?>

		</div>
	</div>
<?require($_SERVER["DOCUMENT_ROOT"]."/bitrix/footer.php");?>