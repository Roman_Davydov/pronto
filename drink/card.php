<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');
$APPLICATION->SetTitle("Закуски");
?>
	<div class="content">
		<div class="wrapper index product drink-style">
			<? $APPLICATION->IncludeComponent("pronto24:catalog.card", '.default', Array("IBLOCK_ID" => 16)); ?>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>