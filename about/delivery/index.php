<?
require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Доставка");

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/delivery.js');

$APPLICATION->IncludeComponent("pronto24:restaurants.parser");
?>
	<script>
		function test() {
			return false;
		}
	</script>
	<div class="content">
		<div class="wrapper delivery-wr">
			<div class="delivery">
				<div class="title">
					<span>Оперативная<br><i>доставка блюд</i></span>
				</div>
				<h2 class="big for-mobile">Доставка</h2>
				<div class="row m-b50">
					<div class="col col_4">
						Бесплатная доставка
						(мы доставляем с 11:00 до 00:00)
					</div>
					<div class="col col_4">
						650 рублей — минимальная сумма заказа
					</div>
					<div class="col col_4">
						Оплата картой и наличными
					</div>
					<div class="col col_4">
						Мы отбираем свежие и качественные продукты
					</div>
				</div>
				<div class="info">
					<h2>минимальная сумма заказа: <br>от 650 рублей</h2>
					<p class="fill">в зависимости от зоны доставки:</p>
				</div>
				<div class="map" id="map"></div>
			</div>
			<div class="row zone">
				<div class="col col_3">
					<h1>зона а</h1>
					<p>минимальная сумма доставки: 650 рублей время доставки: 1 час</p>
				</div>
				<div class="col col_3">
					<h1>зона в</h1>
					<p>минимальная сумма доставки: 650 рублей время доставки: 1 час 15 минут</p>
				</div>
				<div class="col col_3">
					<h1>зона с</h1>
					<p>минимальная сумма доставки: 1000 рублей время доставки: 1 час 30 минут</p>
				</div>
			</div>
			<div class="zone-alert">
				В ряде случаев время доставки может быть изменено,
				уточняйте данную информацию у наших операторов
			</div>
			<div class="_txt-center fill-s">
				Заказы подтверждаются рестораном в течении 10 минут после оформления на сайте.
				если подтверждение не произошло, свяжитесь с нашими операторами
				по телефону +7 495/499 505-57-57
			</div>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php"); ?>