<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');
$APPLICATION->SetTitle("Суши");
?>
	<div class="content">
		<div class="wrapper index product sushi-style">
			<div class="intro">
				Суши
			</div>
			<div class="sort">
				<a href="/sushi/" <?= empty($_GET)?"class='active'":"" ?>>все</a>
				<a href="/sushi/?salat" <?= isset($_GET['salat'])?"class='active'":"" ?>>салаты</a>
				<a href="/sushi/?soup" <?= isset($_GET['soup'])?"class='active'":"" ?>>супы</a>
				<a href="/sushi/?sushi" <?= isset($_GET['sushi'])?"class='active'":"" ?>>суши</a>
				<a href="/sushi/?roll" <?= isset($_GET['roll'])?"class='active'":"" ?>>роллы</a>
				<a href="/sushi/?sets" <?= isset($_GET['sets'])?"class='active'":"" ?>>суши-сеты</a>
			</div>
			<? $APPLICATION->IncludeComponent("pronto24:catalog.section", '.default', Array('IBLOCK_ID' => 14)); ?>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>