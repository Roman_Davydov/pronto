<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$login = isset($_REQUEST['login']) ? $_REQUEST['login'] : false;
$password = isset($_REQUEST['password']) ? $_REQUEST['password'] : false;
$key = isset($_REQUEST['key']) ? $_REQUEST['key'] : false;
if($key) {
	$arKeys = explode(',', $key);
}
$result = array('result' => 'error', 'error' => 'wrong authorization');

$hash = md5(time());

if ($login && $password) {
	global $USER;
	$arAuthResult = $USER->Login($_REQUEST['login'], $_REQUEST['password']);
	$arGroups = $USER->GetUserGroup($USER->GetID());
	if (isset($arAuthResult['TYPE']) || !in_array(6, $arGroups)) {
		return $result;
	}

	$arOrderList = Array();

	CModule::IncludeModule('sale');

	$arFilter = Array(
		"MARKED" => "Y"
	);

	$db_sales = CSaleOrder::GetList(array("DATE_INSERT" => "ASC"), $arFilter);
	while ($ar_sales = $db_sales->Fetch()) {
		if (in_array($ar_sales['REASON_MARKED'], $arKeys)) {
			CSaleOrder::UnsetMark($ar_sales['ID']);
		} else {
			if(empty($ar_sales['REASON_MARKED'])) {
				CSaleOrder::SetMark($ar_sales['ID'], $hash);
				$ar_sales['REASON_MARKED'] = $hash;
			}
			$arOrderList[] = $ar_sales;
		}
	}
} else {
	return $result;
}
$arElements = Array();

foreach ($arOrderList as $order) {
	$arElements[] = $APPLICATION->IncludeComponent("pronto24:jupiter.order.element", '.default', Array('ORDER' => $order));
}

$arOrders = Array('orders' => Array('order' => $arElements));
$xml = Array2XML::createXML('root', $arOrders);
header('Content-type: application/xml');
echo $xml->saveXML();