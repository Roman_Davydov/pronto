<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');
global $USER;

if($USER->IsAuthorized()) {
	$arProperties = Array(
		'ID',
		'IBLOCK_ID',
		'NAME',
		'PROPERTY_ADDRESS',
		'PROPERTY_SECTION',
		'PROPERTY_STAGE',
		'PROPERTY_FLAT',
		'PROPERTY_USER'
	);

	$arFilters = Array("IBLOCK_ID" => 29, "CREATED_BY" => $USER->GetID());

	$res = CIBlockElement::GetList(Array("ID" => "DESC"), $arFilters, false, false, $arProperties);

	while ($arElement = $res->GetNext()) {
		$arElement['TITLE'] = $arElement['NAME'];
		$arResult[] = $arElement;
	}

	$this->IncludeComponentTemplate();
}