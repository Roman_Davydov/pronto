<a href="<?= $arResult['CODE']?>" class="product-link">
	<img src="<?= $arResult['SRC'] ?>" alt="">
	<?= !empty($arResult['~HTML_TITLE']) ? html_entity_decode($arResult['~HTML_TITLE']) : '<span class="title">'.$arResult['NAME'].'</span>' ?>
</a>
<div class="text-intro" data-height><?= $arResult['DESCRIPTION']; ?></div>
<div class="set row">
	<div class="set-col">
		<div class="gr"><?= $arResult['WEIGHT'] ?></div>
		<div class="btn-group number-group">
			<button data-btn-number="" type="button" class="btn border btn-number" data-type="minus"></button>
			<input data-input-number="" type="text" name="" class="btn border input-number" value="1" min="1"
			       readonly="">
			<button data-btn-number="" type="button" class="btn border btn-number" data-type="plus"></button>
		</div>
	</div>

	<div class="set-col">
		<div class="rub"><?= $arResult['PRICE'] ?> руб.</div>
		<button data-cnt="1" data-product-id="<?= $arResult['ID'] ?>" class="btn pink small basket-add">
			в корзину
		</button>
	</div>
</div>