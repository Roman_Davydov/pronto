<? require($_SERVER["DOCUMENT_ROOT"]."/bitrix/modules/main/include/prolog_before.php");

if(is_numeric($_REQUEST['PAY_SYSTEM_ID'])) {
	$APPLICATION->IncludeComponent(
		"bitrix:sale.order.payment.receive",
		"",
		Array(
			"PAY_SYSTEM_ID_NEW" => $_REQUEST['PAY_SYSTEM_ID'],
			"PERSON_TYPE_ID" => "1"
		)
	);
}
if(empty($_SESSION['ORDER_ID']) || !is_numeric($_SESSION['ORDER_ID'])) {
	$_SESSION['ORDER_ID'] = $_REQUEST["orderId"];
	$_SESSION['NAME'] = 'Уважаемый клиент';
	$arResult['RESTAURANT'] = 'Пронто';
}
LocalRedirect('/basket/completed/');