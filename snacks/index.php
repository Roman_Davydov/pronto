<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Закуски");
?>
	<div class="content">

		<ul id="parallax" class="parallax snacks">
			<li class="layer" data-depth="0.04">
				<div class="wrapper">
					<div class="background _3"></div>
				</div>
			</li>
			<li class="layer" data-depth="0.06">
				<div class="wrapper">
					<div class="background _1"></div>
					<div class="background _2"></div>
					<div class="background _5"></div>
				</div>
			</li>
			<li class="layer" data-depth="0.08">
				<div class="wrapper">
					<div class="background _4"></div>
				</div>
			</li>
			<li class="layer" data-depth="0.1">
				<div class="wrapper">
					<div class="background _6"></div>
				</div>
			</li>
		</ul>
		<div class="wrapper index product snacks-style">
			<div class="intro">
				салаты & закуски
			</div>
			<div class="sort">
				<a href="/snacks/" <?= empty($_GET)?"class='active'":"" ?>>все</a>
				<a href="/snacks/?salat" <?= isset($_GET['salat'])?"class='active'":"" ?>>салаты</a>
				<a href="/snacks/?cold" <?= isset($_GET['cold'])?"class='active'":"" ?>>холодные закуски</a>
				<a href="/snacks/?hot" <?= isset($_GET['hot'])?"class='active'":"" ?>>горячие закуски</a>
			</div>
			<? $APPLICATION->IncludeComponent("pronto24:catalog.section", '.default', Array('IBLOCK_ID' => 9)); ?>
		</div>
	</div>
<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>