<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Личный кабинет");
use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/location.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/personal.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');

if($USER->IsAuthorized())
$APPLICATION->IncludeComponent("pronto24:personal.page");
else
	LocalRedirect('/');
?>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>