<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');

$arProperties = Array(
	'ID',
	'IBLOCK_ID',
	'PROPERTY_B_CHILDREN'
);
$arFilters = Array("IBLOCK_ID" => 3, "PROPERTY_B_CHILDREN_VALUE" => "Y");

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilters, false, false, $arProperties);

while ($obElement = $res->GetNextElement()) {
	$arElement = Array();
	$props = $obElement->GetProperties();
	foreach($props as $key => $property) {
		if(isset($property['VALUE'])) {
			$arElement[$key] = $property['VALUE'];
		}
	}
	$arResult[] = $arElement;
}

$this->IncludeComponentTemplate();