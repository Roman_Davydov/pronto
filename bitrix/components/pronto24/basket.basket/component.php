<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');

$arBasketItems = Array();
$arIDsInBasket = Array();
$arItems = Array();
$arRecomendations = Array();
$arDiscounts = Array();
if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
	$dbBasketItems = CSaleBasket::GetList(
		array(
			"NAME" => "ASC"
		),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
		false,
		false,
		array("ID", "NAME", "PRICE", "QUANTITY", "PRODUCT_ID", "DETAIL_PAGE_URL", "DISCOUNT_PRICE")
	);
	while ($arItem = $dbBasketItems->Fetch()) {
		$res = CIBlockElement::GetList(Array(), Array('ID' => $arItem['PRODUCT_ID']), false, false, Array('ID', 'IBLOCK_ID', 'DETAIL_PICTURE', 'PROPERTY_LINK_TO', 'PROPERTY_PICTURE', 'PROPERTY_SIZE', 'PROPERTY_RECOMENDATIONS', 'PROPERTY_B_CONSTRUCTOR', "PROPERTY_OUTER_ID"));
		$arElement = $res->GetNext();
		if ($arElement['PROPERTY_B_CONSTRUCTOR_VALUE'] == 'Y')
			$arItem['DETAIL_PAGE_URL'] = '/constructor';
		$arIDsInBasket[] = $arElement['ID'];
		$arItem['PRICE'] = getPrice($arElement['ID']);
		$arItem['IBLOCK_ID'] = $arElement['IBLOCK_ID'];
		$arItem['SIZE'] = $arElement['PROPERTY_SIZE_VALUE'];
		$arItem['OUTER_ID'] = $arElement['PROPERTY_OUTER_ID_VALUE'];
		if (!empty($arElement['PROPERTY_LINK_TO_VALUE_ID'])) {
			$arIDsInBasket[] = $arElement['PROPERTY_LINK_TO_VALUE'];
			$res2 = CIBlockElement::GetList(Array(), Array('ID' => $arElement['PROPERTY_LINK_TO_VALUE']), false, false, Array('IBLOCK_ID', 'DETAIL_PICTURE', 'PROPERTY_PICTURE', 'ID', 'PROPERTY_RECOMENDATIONS', "PROPERTY_OUTER_ID"));
			$arElement2 = $res2->GetNext();
			$picture = CFile::GetFileArray($arElement2["DETAIL_PICTURE"]);
			$arElement['PROPERTY_RECOMENDATIONS_VALUE'] = $arElement2['PROPERTY_RECOMENDATIONS_VALUE'];
			$arItem['OUTER_ID'] = $arElement2['PROPERTY_OUTER_ID_VALUE'];
		} else {
			$picture = CFile::GetFileArray($arElement["DETAIL_PICTURE"]);
		}
		$arRecomendations[] = $arElement['PROPERTY_RECOMENDATIONS_VALUE'];
		$arItem['DETAIL_PICTURE'] = $picture['SRC'];
		if ($arItem['IBLOCK_ID'] != 26) {
			$db_res = CSaleBasket::GetPropsList(
				array(
					"SORT" => "ASC",
					"NAME" => "ASC"
				),
				array("BASKET_ID" => $arItem['ID'])
			);
			while ($ar_res = $db_res->Fetch()) {
				if ($ar_res['NAME'] == 'MODIFICATOR') {
					$arItem['PROPS'][] = $ar_res;
				}
			}
		}

		$arItems[$arItem['ID']] = $arItem;

		if ($arItem['IBLOCK_ID'] != 26) {
			$arBasketItems[] = $arItem;
		}
	}
//price corrections for pizza
	foreach ($arBasketItems as $key => $pizza) {
		if ($pizza['IBLOCK_ID'] == 5) {
			foreach ($pizza['PROPS'] as $property) {
				$arBasketItems[$key]['PRICE'] += $arItems[$property['CODE']]['PRICE'] * $property['VALUE'];
				$arBasketItems[$key]['DISCOUNT_PRICE'] += $arItems[$property['CODE']]['DISCOUNT_PRICE'] * $property['VALUE'];
			}
		}
	}
	foreach ($arBasketItems as $arItem) {
		$arResult['NUM_PRODUCTS']++;
		$arResult['TOTAL_PRICE'] += $arItem['PRICE'] * $arItem['QUANTITY'];
		$arResult['DISCOUNT_TOTAL_PRICE'] += $arItem['DISCOUNT_PRICE'] * $arItem['QUANTITY'];
	}
	$arResult['FORMATTED_TOTAL_PRICE'] = $arResult['TOTAL_PRICE'] . ' руб.';
	$arResult['ROWS'] = $arBasketItems;
	$arResult['ALL_PRODUCTS'] = $arItems;

	//recomendations
	$arUniqueRecsBlocks = array_unique($arRecomendations);
	$resRec = CIBlockElement::GetList(Array(), Array('IBLOCK_CODE' => 'recomendations', 'ID' => $arUniqueRecsBlocks));

	$arBasketRecomendations = Array();

	while ($obElement = $resRec->GetNextElement()) {
		$arProps = $obElement->GetProperties();
		foreach ($arProps as $key => $property) {
			if ($key == "PRODUCT") {
				$arBasketRecomendations = array_merge($arBasketRecomendations, $property['VALUE']);
			}
		}
	}

	$arResult['RECOMENDATIONS'] = array_unique($arBasketRecomendations);
	foreach ($arResult['RECOMENDATIONS'] as $key => $ID) {
		if (in_array($ID, $arIDsInBasket)) {
			unset($arResult['RECOMENDATIONS'][$key]);
		}
	}

	$arResult['DISCOUNT_TOTAL_PRICE'] = round($arResult['TOTAL_PRICE'] - $arResult['DISCOUNT_TOTAL_PRICE']);
	$arResult['FORMATTED_DISCOUNT_TOTAL_PRICE'] = $arResult['DISCOUNT_TOTAL_PRICE'] . ' руб.';

	//$arDiscounts = array_unique($arDiscounts);
	$couponsResult = CCatalogDiscountCoupon::GetCoupons();
	foreach ($couponsResult as $coupon) {
		$arFilter = array('COUPON' => $coupon);
		$dbCoupon = CCatalogDiscountCoupon::GetList(array(), $arFilter);
		if ($arCoupon = $dbCoupon->Fetch()) {
			$arResult['COUPON_LIST'][] = $arCoupon;
		}
	}

	$arFilter = Array(
		"IBLOCK_ID" => 34,
		"ACTIVE" => "Y",
		"<=PROPERTY_MIN_SUM" => $arResult['DISCOUNT_TOTAL_PRICE']
	);

	$arProperties = Array('ID', 'NAME', 'IBLOCK_ID');
	$res = CIBlockElement::GetList(Array("PROPERTY_MIN_SUM" => "DESC"), $arFilter, false, false, $arProperties);

	$obElement = $res->GetNextElement();
	if(is_numeric($obElement->fields['ID'])) {
		$arResult['GIFT_NAME'] = $obElement->fields['NAME'];
		$props = $obElement->GetProperties();
		foreach($props as $key => $property) {
			if(isset($property['VALUE']) && $key == 'GIFT') {
				//Установка нужного подарка
				$bFirst = true;
				foreach($property['VALUE'] as $gift) {
					$res = CIBlockElement::GetList(Array(), Array('ID' => $gift), false, false, Array('ID', 'NAME', 'IBLOCK_ID', 'DETAIL_PAGE_URL', 'DETAIL_PICTURE', 'PROPERTY_LINK_TO', 'PROPERTY_PICTURE', 'PROPERTY_SIZE'));
					$arElement = $res->GetNext();
					$arElement['CHOOSED'] = empty($_SESSION['CHOOSED_GIFT']) ? $bFirst : ($_SESSION['CHOOSED_GIFT'] == $gift);
					if(empty($_SESSION['CHOOSED_GIFT'])) {
						$_SESSION['CHOOSED_GIFT'] = $gift;
					}
					$bFirst = false;
					$picture = CFile::GetFileArray($arElement["DETAIL_PICTURE"]);
					$arElement['DETAIL_PICTURE'] = $picture['SRC'];
					$arResult['GIFTS'][] = $arElement;
				}
			}
		}
	}

	$this->IncludeComponentTemplate();
}

return $arResult;
