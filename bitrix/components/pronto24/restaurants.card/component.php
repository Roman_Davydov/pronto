<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');

$arProperties = Array(
	'ID',
	'CODE',
	'IBLOCK_ID',
	'NAME',
	'PROPERTY_LOCATION',
	'PROPERTY_PHONE',
	'PROPERTY_WORKING_TIME_RESTAURANT',
	'PROPERTY_WORKING_TIME_DELIVERY',
	'PROPERTY_METRO',
	'PROPERTY_GEO',
	'PROPERTY_METRO_COLOR',
	'PROPERTY_STOCKS',
	'PHOTOS'
);
$CODE = explode('?', $_GET['code']);
$CODE[0] = str_replace('/', '', $CODE[0]);

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), Array("IBLOCK_ID" => 3, 'CODE' => $CODE[0]), false, false, $arProperties);

$obElement = $res->GetNextElement();
$arResult = Array();

if(is_numeric($obElement->fields['ID'])) {
	$arResult['ID'] = $obElement->fields['ID'];
	$arResult['CODE'] = $obElement->fields['CODE'];
	$props = $obElement->GetProperties();
	foreach ($props as $key => $property) {
		if (isset($property['VALUE']) && $key != 'PHOTOS') {
			$arResult[$key] = $property['VALUE'];
		}
		if($key == 'PHOTOS') {
			foreach($property['VALUE'] as $photoId) {
				$picture = CFile::GetFileArray($photoId);
				$arResult['PHOTOS'][] = $picture['SRC'];
			}
		}
	}
	$arResult['WORKING_TIME_RESTAURANT'] = $APPLICATION->IncludeComponent('pronto24:timetable.display', "", Array("TYPE" => "main", "ID" => $arResult['ID'], "timetable" => $arResult['WORKING_TIME_RESTAURANT']));
	$arResult['WORKING_TIME_DELIVERY'] = $APPLICATION->IncludeComponent('pronto24:timetable.display', "", Array("TYPE" => "delivery", "ID" => $arResult['ID'], "timetable" => $arResult['WORKING_TIME_DELIVERY']));

	$this->IncludeComponentTemplate();

	return $arResult['STOCKS'];
}
else {
	require($_SERVER['DOCUMENT_ROOT'] . '/404.php');
}