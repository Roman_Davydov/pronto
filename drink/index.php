<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
	$APPLICATION->SetTitle("Напитки");
?>
	<div class="content">
		<div class="wrapper index product drink-style">
			<div class="intro">
				Бар & Напитки
			</div>
			<div class="sort">
				<a href="/drink/" <?= empty($_GET)?"class='active'":"" ?>>все</a>
				<a href="/drink/?drink" <?= isset($_GET['drink'])?"class='active'":"" ?>>напитки</a>
				<a href="/drink/?beer" <?= isset($_GET['beer'])?"class='active'":"" ?>>пиво</a>
				<a href="/drink/?juice" <?= isset($_GET['juice'])?"class='active'":"" ?>>соки и нектары</a>
				<a href="/drink/?fresh" <?= isset($_GET['fresh'])?"class='active'":"" ?>>свежевыжатый сок</a>
				<a href="/drink/?snack" <?= isset($_GET['snack'])?"class='active'":"" ?>>снеки</a>
			</div>
			<? $APPLICATION->IncludeComponent("pronto24:catalog.section", '.default', Array('IBLOCK_ID' => 16)); ?>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>