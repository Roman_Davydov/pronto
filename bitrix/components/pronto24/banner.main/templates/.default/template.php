<div class="main-slider-wr">
	<div data-main-slider class="owl-carousel owl-theme main-slider">
		<? foreach ($arResult['BANNERS'] as $banner): ?>
			<a href="<?= $banner['LINK'] ?>">
				<div><img src="<?= $banner['SRC'] ?>" alt="<?= $banner['ALT'] ?>"></div>
			</a>
		<? endforeach; ?>
	</div>
</div>