<?php
$i = 1; ?>
<div class="product restaurant-menu">
	<? foreach ($arResult as $element): ?>
	<? if ($i % 3 == 1) : ?>
	<div class="row row-col">
		<? endif ?>
		<div class="col col_3">
			<a href="<?= $element['LINK'] ?>" target="_blank" class="product-link">
                        <span class="img">
                            <img src="<?= $element['SRC'] ?>">
                        </span>
				<span class="title"><?= $element['NAME'] ?></span>
			</a>
			<div class="text-intro" data-height>
				<?= $element['TEXT'] ?>
			</div>
			<? if ($i % 3 == 0 || $i == count($arResult['elements'])) : ?>
		</div>
	<? endif ?>
		<? $i++ ?>
		<? endforeach; ?>
	</div>
</div>