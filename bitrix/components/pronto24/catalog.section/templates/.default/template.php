<?php
$i = 1;
foreach ($arResult['elements'] as $element) :?>
	<? if ($i % 3 == 1) : ?>
		<div class="row row-col">
	<? endif ?>
	<div data-product-card class="col col_3">
		<? $APPLICATION->IncludeComponent("pronto24:catalog.element", $arResult['template'], $element) ?>
	</div>
	<? if ($i % 3 == 0 || $i == count($arResult['elements'])) : ?>
		</div>
	<? endif ?>
	<? $i++ ?>
<? endforeach; ?>