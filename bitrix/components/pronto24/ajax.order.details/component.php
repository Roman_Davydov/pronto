<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('sale');
CModule::IncludeModule('iblock');

global $USER;

$name = isset($_POST['name']) ? $_POST['name'] : false;
$phone = isset($_POST['login']) ? $_POST['login'] : false;
$address = isset($_POST['address']) ? $_POST['address'] : false;
$addressName = isset($_POST['addressName']) ? $_POST['addressName'] : false;
$locationID = isset($_POST['locationID']) ? $_POST['locationID'] : false;
$restaurantID = isset($_POST['restaurant']) ? $_POST['restaurant'] : false;
$zoneID = isset($_POST['zone']) ? $_POST['zone'] : false;
$paySystem = $_POST['paySystem'];
$comment = isset($_POST['comment']) ? $_POST['comment'] : false;
$change = isset($_POST['change']) ? $_POST['change'] : false;

$section = isset($_POST['section']) ? $_POST['section'] : false;
$stage = isset($_POST['stage']) ? $_POST['stage'] : false;
$flat = isset($_POST['flat']) ? $_POST['flat'] : false;

$_SESSION['DEFAULTS'] = Array(
	'name' => $name,
	'phone' => $phone,
	'address' => $address,
	'locationID' => $locationID,
	'paySystem' => $paySystem,
	'comment' => $comment,
	'change' => $change,
	'section' => $section,
	'stage' => $stage,
	'flat' => $flat
);

$phone = str_replace(Array('+', ' ', '-', '(', ')'), Array('', '', '', '', ''), $phone);
$firstLetter = substr($phone, 0, 1);
if ($firstLetter == 8 || $firstLetter == 7) {
	$phone = substr($phone, 1);
}
$firstLetter = substr($phone, 0, 1);
if ($firstLetter != 9) {
	return array('result' => 'error', 'error' => 'mobile');
}

$arZones = $APPLICATION->IncludeComponent("pronto24:restaurants.parser", "", Array("RETURN" => "Y"));

$dbBasketItems = CSaleBasket::GetList(
	array(
		"NAME" => "ASC"
	),
	array(
		"FUSER_ID" => CSaleBasket::GetBasketUserID(),
		"LID" => SITE_ID,
		"ORDER_ID" => "NULL"
	),
	false,
	false,
	array("PRICE", "QUANTITY")
);
$totalPrice = 0;
while ($arItem = $dbBasketItems->Fetch()) {
	$totalPrice += $arItem['PRICE'] * $arItem['QUANTITY'];
}
$totalPrice = round($totalPrice);

$APPLICATION->IncludeComponent("pronto24:stoplist.parser", "", Array("restaurant_id" => $restaurantID));

if (!$APPLICATION->IncludeComponent("pronto24:stoplist.basket.check")) {
	return Array('result' => 'error', 'error' => 'stoplist');
}

if ($arZones[$restaurantID][$zoneID]['sum_min'] > IntVal($totalPrice)) {
	return Array('result' => 'error', 'error' => 'summ', 'summ' => $arZones[$restaurantID][$zoneID]['sum_min']);
}

if (!$APPLICATION->IncludeComponent("pronto24:timetable.delivery.check", "", Array("ID" => $restaurantID))) {
	return Array('result' => 'error', 'error' => 'time');
}

if (!$address) {
	return Array('result' => 'error', 'error' => 'address');
}

$basketUser = CSaleBasket::GetBasketUserID();
if (!$USER->IsAuthorized()) {
	$bAuthorized = false;
	$regResult = $APPLICATION->IncludeComponent("pronto24:ajax.registration");
	if ($regResult['result'] == 'ok') {
		$userId = $regResult['user'];
		$rsUser = $USER->GetByID($userId);
		$user = $rsUser->Fetch();
		if (empty($user['NAME'])) {
			$USER->Update($userId, Array('NAME' => $name));
		}
	} else {
		$rsUser = $USER->GetByLogin($phone);
		$user = $rsUser->Fetch();
		$userId = $user['ID'];
		if (empty($user['NAME'])) {
			$USER->Update($userId, Array('NAME' => $name));
		}
	}
	$USER->Authorize($userId);
} else {
	$bAuthorized = true;
	$userId = $USER->GetID();
	$userName = $USER->getFirstName();
	if (empty($userName)) {
		$USER->Update($userId, Array('NAME' => $name));
	}
	$rsUser = CUser::GetByID($userId);
	$arUser = $rsUser->Fetch();
	if (empty($arUser['PERSONAL_PHONE'])) {
		$USER->Update($userId, Array('PERSONAL_PHONE' => $phone));
	}
}
$return = Array();
if ($name && $phone && $address && $restaurantID && $zoneID && $paySystem) {

	$resAddress = $APPLICATION->IncludeComponent("pronto24:ajax.location.add");

	$addressID = $resAddress['locationID'];

	if ($paySystem == 1) {
		$paySystem = 2;
	} else {
		$arSelect = Array("ID", "IBLOCK_ID", "PROPERTY_JUPITER_ID", "PROPERTY_PLATRON_ID", "PROPERTY_SBERBANK_ID");
		$arFilter = Array("IBLOCK_ID" => 3, "PROPERTY_JUPITER_ID" => $restaurantID);
		$obRestoraunt = CIBlockElement::GetList(Array(), $arFilter, false, false, $arSelect);
		$arRestoraunt = $obRestoraunt->GetNext();
		if ($paySystem == 3) {
			$return['paySystemType'] = 'platron';
			$paySystem = $arRestoraunt['PROPERTY_PLATRON_ID_VALUE'];

		} else {//$paySystem == 2) {
			$return['paySystemType'] = 'sberbank';
			$paySystem = $arRestoraunt['PROPERTY_SBERBANK_ID_VALUE'];
		}
		$return['paySystem'] = $paySystem;
	}

	if (empty($paySystem)) {
		return Array('result' => 'error', 'error' => 'paysystem');
	}

	$arFields = array(
		"LID" => SITE_ID,
		"PAYED" => "N",
		"PERSON_TYPE_ID" => 1,
		"CANCELED" => "N",
		"STATUS_ID" => "N",
		"PRICE" => $totalPrice,
		"CURRENCY" => "RUB",
		"USER_ID" => IntVal($userId),
		"PAY_SYSTEM_ID" => $paySystem
	);
	$ORDER_ID = CSaleOrder::Add($arFields);
	$return['order'] = $ORDER_ID;

	CSaleOrder::DeliverOrder($ORDER_ID, "Y");

	$result = CSaleBasket::OrderBasket($ORDER_ID);

	$arFields = array(
		"ORDER_ID" => $ORDER_ID,
		"ORDER_PROPS_ID" => 1,
		"NAME" => "Имя",
		"CODE" => "NAME",
		"VALUE" => $name
	);

	CSaleOrderPropsValue::Add($arFields);

	$arFields = array(
		"ORDER_ID" => $ORDER_ID,
		"ORDER_PROPS_ID" => 2,
		"NAME" => "Телефон",
		"CODE" => "PHONE",
		"VALUE" => $phone
	);

	CSaleOrderPropsValue::Add($arFields);

	$arFields = array(
		"ORDER_ID" => $ORDER_ID,
		"ORDER_PROPS_ID" => 3,
		"NAME" => "Адрес доставки",
		"CODE" => "ADDRESS",
		"VALUE" => $address
	);

	CSaleOrderPropsValue::Add($arFields);

	$arFields = array(
		"ORDER_ID" => $ORDER_ID,
		"ORDER_PROPS_ID" => 4,
		"NAME" => "Комментарий",
		"CODE" => "COMMENT",
		"VALUE" => $comment
	);

	CSaleOrderPropsValue::Add($arFields);

	$arFields = array(
		"ORDER_ID" => $ORDER_ID,
		"ORDER_PROPS_ID" => 5,
		"NAME" => "ID ресторана",
		"CODE" => "RESTORANT_ID",
		"VALUE" => $restaurantID
	);

	CSaleOrderPropsValue::Add($arFields);

	$arFields = array(
		"ORDER_ID" => $ORDER_ID,
		"ORDER_PROPS_ID" => 6,
		"NAME" => "ID зоны доставки",
		"CODE" => "ZONE",
		"VALUE" => $zoneID
	);

	CSaleOrderPropsValue::Add($arFields);

	$arFields = array(
		"ORDER_ID" => $ORDER_ID,
		"ORDER_PROPS_ID" => 7,
		"NAME" => "Сдача",
		"CODE" => "CHANGE",
		"VALUE" => $change
	);

	CSaleOrderPropsValue::Add($arFields);

	$arFields = array(
		"ORDER_ID" => $ORDER_ID,
		"ORDER_PROPS_ID" => 13,
		"NAME" => "Подъезд",
		"CODE" => "SECTION",
		"VALUE" => $section
	);

	CSaleOrderPropsValue::Add($arFields);

	$arFields = array(
		"ORDER_ID" => $ORDER_ID,
		"ORDER_PROPS_ID" => 14,
		"NAME" => "Этаж",
		"CODE" => "STAGE",
		"VALUE" => $stage
	);

	CSaleOrderPropsValue::Add($arFields);

	$arFields = array(
		"ORDER_ID" => $ORDER_ID,
		"ORDER_PROPS_ID" => 15,
		"NAME" => "Квартира",
		"CODE" => "FLAT",
		"VALUE" => $flat
	);

	CSaleOrderPropsValue::Add($arFields);

	$_SESSION['ORDER_ID'] = $ORDER_ID;
	$_SESSION['NAME'] = $name;
	$_SESSION['RESTAURANT'] = $restaurantID;
	$_SESSION['ZONE'] = $zoneID;
	$_SESSION['UPLOAD'] = false;

	CSaleOrder::SetMark($ORDER_ID, 'NOT_LOADED_TO_JUPITER');

	/*GIFTS*/

	$stocks = $APPLICATION->IncludeComponent("pronto24:stock.update", "", Array("TOTAL_PRICE" => IntVal($totalPrice)));
	if(!empty($stocks)) {
		foreach($stocks['stock']['gifts'] as $gift) {
			if($gift['CHOOSED']) {
				$arFields = array(
					"ORDER_ID" => $ORDER_ID,
					"ORDER_PROPS_ID" => 16,
					"NAME" => "Подарок",
					"CODE" => "GIFT",
					"VALUE" => $gift['ID']
				);

				CSaleOrderPropsValue::Add($arFields);
			}
		}
	}
	/*GIFTS*/

	unset($_SESSION['DEFAULTS']);
}

$return['result'] = 'ok';
return $return;