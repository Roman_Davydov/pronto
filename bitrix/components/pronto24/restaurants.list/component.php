<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

CModule::IncludeModule('iblock');

$arProperties = Array(
	'ID',
	'CODE',
	'IBLOCK_ID',
	'NAME',
	'PROPERTY_B_CHILDREN',
	'PROPERTY_B_MUSIC',
	'PROPERTY_B_LUNCH',
	'PROPERTY_B_BANQUET',
	'PROPERTY_B_ZELENOGRAD',
	'PROPERTY_B_MOSOBL',
	'PROPERTY_B_MOSCOW',
	'PROPERTY_B_RUSSIA'
);
$arFilters = Array("IBLOCK_ID" => 3);
if (!empty($_GET)) {
	foreach ($_GET as $key => $val) {
		$arFilters['PROPERTY_B_' . strtoupper($key) . '_VALUE'] = "Y";
	}
}
else
	$arFilters['PROPERTY_B_MOSCOW_VALUE'] = "Y";

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilters, false, false, $arProperties);

while ($obElement = $res->GetNextElement()) {
	$arElement = Array();
	$arElement['ID'] = $obElement->fields['ID'] ;
	$arElement['CODE'] = $obElement->fields['CODE'] ;
	$props = $obElement->GetProperties();
	foreach($props as $key => $property) {
		if(isset($property['VALUE'])) {
			$arElement[$key] = $property['VALUE'];
		}
	}
	$arResult['PLACES'][] = $arElement;
}
if(isset($_GET['russia'])) {
	$arResult['CENTER'] = '55.9825, 42';
	$arResult['ZOOM'] = '5';
}
elseif(isset($_GET['zelenograd'])) {
	$arResult['CENTER'] = '55.9825, 37.18139';
	$arResult['ZOOM'] = '11';
}
elseif(isset($_GET['mosobl'])) {
	$arResult['CENTER'] = '55.753559, 37.609218';
	$arResult['ZOOM'] = '7';
}
else {
	$arResult['CENTER'] = '55.753559, 37.609218';
	$arResult['ZOOM'] = '9';
}


$this->IncludeComponentTemplate();