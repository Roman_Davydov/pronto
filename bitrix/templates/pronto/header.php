<? if (!defined("B_PROLOG_INCLUDED") || B_PROLOG_INCLUDED !== true) die();
use Bitrix\Main\Page\Asset;

global $USER;

CJSCore::Init(array("ajax"));
Asset::getInstance()->addJs('https://code.jquery.com/jquery-1.12.4.min.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/lib/jquery/parallax.min.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/lib/bootstrap/bootstrap.min.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/lib/bootstrap/bootstrap-select.min.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/lib/jquery/jquery.mCustomScrollbar.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/lib/jquery/owl.carousel.min.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/lib/jquery/jquery.maskedinput.js');
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/manager.js');

Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/lib/jquery/owl.carousel.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/lib/jquery/jquery.mCustomScrollbar.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/normalize.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/font/include-font.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/icon/include-icon.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/base.css");
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/ui.css");

if (substr($APPLICATION->GetCurPage(), 0, 7) == '/about/') {
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/about.css");
	if(($APPLICATION->GetCurPage() == '/about/delivery/')) {
		Asset::getInstance()->addJs('https://api-maps.yandex.ru/2.0-stable/?load=package.full&mode=release&lang=ru-RU');
	}
} elseif (substr($APPLICATION->GetCurPage(), 0, 13) == '/restaurants/' || $APPLICATION->GetCurPage() == '/about/delivery/') {
	Asset::getInstance()->addJs('https://api-maps.yandex.ru/2.0-stable/?load=package.full&mode=release&lang=ru-RU');
	Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/pages/restaurant.js');
} elseif (substr($APPLICATION->GetCurPage(), 0, 8) == '/basket/') {
	Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/cabinet.css");
} else {
	switch ($APPLICATION->GetCurPage()) {
		case '/':
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/pages/main.js');
			Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/main.css");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');
			break;
		case '/paste/':
		case '/snacks/':
		case '/dessert/':
		case '/hot/':
		case '/drink/':
			Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/parallax.css");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');
		case '/personal/':
			Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/cabinet.css");
			break;
		case '/pizza/':
			Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/main.css");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');
			break;
		case '/constructor/':
			Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . '/css/constructor.css');
			Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/parallax.css");
			Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/product.js');
			break;
		case '/children/':
			Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/children.css");
			break;
	}
}
Asset::getInstance()->addCss(SITE_TEMPLATE_PATH . "/css/media.css");
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/init.js');
?>
<!DOCTYPE html>
<html>
<head>
	<link href="/bitrix/templates/pronto/favicon.ico" rel="shortcut icon" type="image/x-icon" />
	<meta charset="UTF-8">
	<title><? $APPLICATION->ShowTitle(); ?></title>
	<meta name="developed" content="RDA-studio.ru">
	<meta name="viewport" content="width=device-width">
	<script type="text/javascript">
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function () {
				try {
					w.yaCounter12122377 = new Ya.Metrika({id: 12122377, enableAll: true});
				} catch (e) {
				}
			});
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<? $APPLICATION->ShowHead(); ?>
	<!-- Yandex.Metrika counter -->
	<script type="text/javascript">
		(function (d, w, c) {
			(w[c] = w[c] || []).push(function() {
				try {
					w.yaCounter40106425 = new Ya.Metrika({
						id:40106425,
						clickmap:true,
						trackLinks:true,
						accurateTrackBounce:true,
						webvisor:true,
						ut:"noindex"
					});
				} catch(e) { }
			});

			var n = d.getElementsByTagName("script")[0],
				s = d.createElement("script"),
				f = function () { n.parentNode.insertBefore(s, n); };
			s.type = "text/javascript";
			s.async = true;
			s.src = "https://mc.yandex.ru/metrika/watch.js";

			if (w.opera == "[object Opera]") {
				d.addEventListener("DOMContentLoaded", f, false);
			} else { f(); }
		})(document, window, "yandex_metrika_callbacks");
	</script>
	<style>
		.phone a {
			text-decoration: none;
			cursor: text;
		}

		.phone a:hover {
			color: black;
		}
	</style>
	<noscript><div><img src="https://mc.yandex.ru/watch/40106425?ut=noindex" style="position:absolute; left:-9999px;" alt="" /></div></noscript>
	<!-- /Yandex.Metrika counter -->
	<!-- GOOGLE ANALYTICS-->
	<script type="text/javascript">
		(function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
				(i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
			m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
		})(window,document,'script','//www.google-analytics.com/analytics.js','ga');

		ga('create', 'UA-30876421-1', 'pronto24.ru');
		ga('send', 'pageview');
	</script>
	<!-- /GOOGLE ANALYTICS-->
</head>
<body>
<? $APPLICATION->ShowPanel() ?>
<!--fixed row-->
<div class="demo">
	<div class="link">Отзывы о новом сайте присылайте на <a href="mailto:pronto24site@gmail.com">pronto24site@gmail.com</a></div>
</div>
<a class="fix-link" href="/constructor/"><span>конструктор пиццы</span><div class="ic icon-svg-2s"></div></a>
<div class="back" data-back></div>
<? if($APPLICATION->GetCurPage() == '/') :?>
	<ul id="parallax" class="parallax top">
		<li class="layer _1" data-depth="0.1">
			<div class="wrapper">
				<div class="background"></div>
			</div>
		</li>
		<li class="layer _2" data-depth="0.15">
			<div class="wrapper">
				<div class="background"></div>
			</div>
		</li>
	</ul>

<? endif ?>

<div data-row-fixed class="fixed">
	<div class="btn-group">
		<? if (!$USER->IsAuthorized()): ?>
			<button class="btn border" onClick="MainManager.showModal('registration')">регистрация</button>
			<button class="btn border" onClick="MainManager.showModal('authorization')">войти</button>
		<? else: ?>
			<a href="/personal/"
			   class="btn border link"><?= ($USER->GetFullName() == '' ? 'личный кабинет' : $USER->GetFullName()) ?></a>
		<? endif ?>
	</div>

	<div class="basket-box" data-basket>
		<? $APPLICATION->IncludeComponent("pronto24:basket.line"); ?>
	</div>
</div>
<div id="fakeMap"></div>
<div class="wrapper index">
	<div class="header row">
		<div class="left-col">
			<a href="/" class="logo"></a>
			<button onClick="MainManager.showModal('call')" class="btn orange-bg">заказать звонок</button>
		</div>
		<div class="right-col">
			<div class="main-nav row">
				<div class="right-col">
					<div class="phone"><a href="tel:+74955055757">+7 495</a>/<a href="tel:+74995055757">499 <span>505-57-57</span></a></div>
					<? $APPLICATION->IncludeComponent("pronto24:pizza.counter"); ?>
				</div>
				<ul class="nav" data-main-menu>
					<? if (!$USER->IsAuthorized()): ?>
						<li class="for-mobile"><a href="#" onclick="MainManager.showModal('authorization'); return false;">войти</a></li>
						<li class="for-mobile"><a href="#" onclick="MainManager.showModal('registration'); return false;">регистрация</a></li>
					<? else: ?>
						<li class="for-mobile"><a href="#" class="link" onclick="return false;"><?= ($USER->GetFullName() == '' ? 'личный кабинет' : $USER->GetFullName()) ?></a></li>
					<? endif ?>
					<li><a <?= $APPLICATION->GetCurPage() == '/about/delivery/' ? 'class="active"' : '' ?> href="/about/delivery/">ДОСТАВКА</a></li>
					<li class="sep"></li>
					<li><a <?= $APPLICATION->GetCurPage() == '/stocks/' ? 'class="active"' : '' ?> href="/stocks/">АКЦИИ</a></li>
					<li class="sep"></li>
					<li><a <?= substr($APPLICATION->GetCurPage(), 0, 13) == '/restaurants/' ? 'class="active"' : '' ?> href="/restaurants/">РЕСТОРАНЫ</a></li>
					<li class="sep"></li>
					<li><a <?= (substr($APPLICATION->GetCurPage(), 0, 7) == '/about/' && $APPLICATION->GetCurPage() != '/about/delivery/') ? 'class="active"' : '' ?> href="/about/">ПРОНТО</a></li>
					<li class="for-mobile _s">
						<div class="social">
							<div><a href="https://vk.com/pizzapronto" target="_blank" class="soc-icon-s_1"></a></div>
							<div><a href="https://www.facebook.com/prontopizzacafe" target="_blank" class="soc-icon-s_2"></a></div>
							<div><a href="https://ok.ru/prontopizza" target="_blank" class="soc-icon-s_3"></a></div>
							<div><a href="https://www.instagram.com/pronto_pizza_/" target="_blank" class="soc-icon-s_4"></a></div>
						</div>
					</li>
				</ul>
			</div>
			<div class="_txt-center row">
				<div class="btn-group">
					<? if (!$USER->IsAuthorized()): ?>
						<button class="btn border" onClick="MainManager.showModal('registration')">регистрация</button>
						<button class="btn border" onClick="MainManager.showModal('authorization')">войти</button>
					<? else: ?>
						<a href="/personal/"
						   class="btn border link"><?= ($USER->GetFullName() == '' ? 'личный кабинет' : $USER->GetFullName()) ?></a>
					<? endif ?>
				</div>
				<div class="social">
					<a href="https://vk.com/pizzapronto" target="_blank" class="soc-icon-s_1"></a>
					<a href="https://www.facebook.com/prontopizzacafe" target="_blank" class="soc-icon-s_2"></a>
					<a href="https://ok.ru/prontopizza" target="_blank" class="soc-icon-s_3"></a>
					<a href="https://www.instagram.com/pronto_pizza_/" target="_blank" class="soc-icon-s_4"></a>
				</div>
				<div class="basket-box" data-basket-2>
					<? $APPLICATION->IncludeComponent("pronto24:basket.line"); ?>
				</div>
			</div>
		</div>
		<? $APPLICATION->IncludeComponent("pronto24:menu.main"); ?>
	</div>
	<!--Slider for main page-->
	<? if ($APPLICATION->GetCurPage() == '/')
		$APPLICATION->IncludeComponent("pronto24:banner.main"); ?>
</div>