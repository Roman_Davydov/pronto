<a href="/pizza/<?= $arResult['CODE'] ?>" class="product-link">
	<img src="<?= $arResult['SRC'] ?>" alt="">
	<?= !empty($arResult['~HTML_TITLE']) ? html_entity_decode($arResult['~HTML_TITLE']) : '<span class="title">'.$arResult['NAME'].'</span>' ?>
</a>
<div class="text-intro" data-height><?= $arResult['DESCRIPTION']; ?></div>
<div class="controls row">
	<div class="btn-group number-group">
		<button data-btn-number type="button" class="btn border btn-number" data-type="minus"></button>
		<input data-input-number type="text" name="" class="btn border input-number" value="1" min="1"
		       readonly>
		<button data-btn-number type="button" class="btn border btn-number" data-type="plus"></button>
	</div>
	<div class="rub"><span class="price"><?= $arResult['SIZES'][0]['PRICE'] ?></span> руб.</div>
	<select data-btn-select class="selectpicker">
		<? foreach ($arResult['SIZES'] as $arSizeValues) : ?>
			<option value="<?= $arSizeValues['ID'] ?>"
			        data-price="<?= $arSizeValues['PRICE'] ?>"><?= $arSizeValues['SIZE'] ?> см
			</option>
		<? endforeach ?>
	</select>
</div>
<div class="set dropup btn-group row">
	<button class="btn border set-btn" data-toggle="dropdown">Добавить ингредиенты</button>
	<button class="btn pink set-btn basket-add" data-cnt="1" data-product-id="<?=$arResult['SIZES'][0]['ID']?>">в корзину</button>
	<div class="dropdown-menu" data-dropdown-menu>
		<button class="close-icon"></button>
		<div data-modificators class="scroll" data-scroll>
			<? $APPLICATION->IncludeComponent("pronto24:modificators") ?>
		</div>
	</div>
</div>
