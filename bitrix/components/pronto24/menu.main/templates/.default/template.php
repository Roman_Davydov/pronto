<ul class="menu nav" data-cat-menu>
	<? foreach ($arResult['ELEMENTS'] as $menu => $element): ?>
		<li> <a href="/<?= $menu ?>/" <?= ($element['SELECTED'] ? 'class="active"' : '') ?>><?= $element['NAME'] ?></a>
		</li>
	<? endforeach; ?>
</ul>
<button class="btn border cat-menu" onclick="MainManager.showMenu('data-cat-menu');">Категории меню</button>
<button class="btn border main-menu" onclick="MainManager.showMenu('data-main-menu');">
	<span></span>
</button>
<? if (isset($arResult['CURRENT_SECTION_COLOR'])) : ?>
	<style>
		.wrapper.index.product div.intro, .wrapper.index.product a.active, .wrapper.index.product a:hover {
			color: # <?= $arResult['CURRENT_SECTION_COLOR']?> !important;
		}
	</style>
<? endif ?>