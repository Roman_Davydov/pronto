<div class="item">
	<div class="grid icon">
		<a href="#" class="product-link">
			<?= !empty($arResult['~HTML_TITLE']) ? html_entity_decode($arResult['~HTML_TITLE']) : '<span class="title">'.$arResult['NAME'].'</span>' ?>
		</a>
		<div class="text-intro"><?= $arResult['DESCRIPTION']; ?></div>
		<div class="rub _t">Выбери свой вкус</div>
		<? foreach($arResult['TASTES'] as $taste) :?>
			<p><?= $taste ?></p>
		<? endforeach ?>
		<div class="rub"><?= $arResult['PRICE'] ?> руб.</div>
	</div>
	<div class="grid">
		<img src="<?= $arResult['SRC'] ?>" alt="">
	</div>
</div>