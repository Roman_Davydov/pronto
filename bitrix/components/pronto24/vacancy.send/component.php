<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$name = isset($_POST['name']) ? $_POST['name'] : '';
$phone = isset($_POST['phone']) ? $_POST['phone'] : '';
$profession = isset($_POST['profession']) ? $_POST['profession'] : '';
$text = isset($_POST['text']) ? $_POST['text'] : '';

if (isset($_FILES) && !empty($_FILES)) {
	$uploaddir = $_SERVER['DOCUMENT_ROOT'] . '/upload/files/';
	$uploadfile = $uploaddir . basename($_FILES['file']['name']);
	move_uploaded_file($_FILES['file']['tmp_name'], $uploadfile);
	$arFile = Array(
		'name' => $_FILES['file']['name'],
		'src' => $_SERVER['HTTP_ORIGIN'] . '/upload/files/' . basename($_FILES['file']['name'])
	);
}

CModule::IncludeModule('iblock');
$res = CIBlockElement::GetByID($profession);
if ($ar_res = $res->GetNext())
	$professionName = $ar_res['NAME'];

if ($name && $phone && $professionName && $text) {
	$subject = "Пришло письмо про вакансию с сайта";

	$message = "<p>С сайта пришо сообщение о вакансии. Данные клиента:</p>";
	$message .= "<br/><b>Имя клиента:</b> " . htmlspecialchars($name);
	$message .= "<br/><b>Контакты клиента:</b> " . htmlspecialchars($phone);
	$message .= "<br/><b>Желаемая вакансия:</b> " . htmlspecialchars($professionName);
	$message .= "<br/><b>Сообщение:</b> " . htmlspecialchars($text);
	$message .= "<p>Данное письмо сформировано автоматически. Отвечать на него не нужно.</p>";

	if($arFile && !empty($arFile['name'])) {
		$message .= "<a href='" . $arFile['src'] . "' target='blank'>ссылка на файл " . $arFile['name'] . "</a>";
	}
	$headers = "Content-type: text/html; charset=utf-8 \r\n";
	$headers .= "From: Сайт pronto \r\n";
	$headers .= "Reply-To: info@pronto24.ru \r\n";

	$APPLICATION->IncludeComponent('pronto24:email.send', '', Array('FILTER' => 'B_CV', 'SUBJECT' => $subject, 'MESSAGE' => $message, 'HEADERS' => $headers));
}