<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/modules/main/include/prolog_before.php");

global $USER;
$arGroups = CUser::GetUserGroup($USER->GetID());
if (in_array(1, $arGroups)) {
	CModule::IncludeModule('sale');
	if (isset($_GET['ORDER_ID'])) {
		$order = CSaleOrder::GetByID($_GET['ORDER_ID']);
		$arOrder = $APPLICATION->IncludeComponent("pronto24:jupiter.order.element", '.default', Array('ORDER' => $order));
		$arFullOrder = Array('service' => Array('order' => $arOrder, '@attributes' => Array('action' => 'orders', 'source' => 4, 'id' => $_GET['ORDER_ID'])));

		$xml = Array2XML::createXML('root', $arFullOrder);
		$contents = $xml->saveXML();
		header('Content-type: application/xml');
		echo $contents;
	} else {
		$rsSales = CSaleOrder::GetList(array("DATE_INSERT" => "DESC"), $arFilter);
		while ($arSales = $rsSales->Fetch()) {
			echo '<a href="/order/?ORDER_ID=' . $arSales['ID'], '">Заказ №' . $arSales['ID'] . '</a><br>';
		}
	}
} else {
	require($_SERVER['DOCUMENT_ROOT'] . '/404.php');
}