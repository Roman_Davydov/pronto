<? foreach ($arResult as $element) : ?>
	<div class="set-list">
		<div class="btn-group number-group">
			<button data-btn-number type="button" class="btn border btn-number"
			        data-type="minus"></button>
			<input data-input-number data-product-id="<?= $element['ID'] ?>" type="text" name="" class="btn border input-number"
			       value="0" min="0" readonly>
			<button data-btn-number type="button" class="btn border btn-number"
			        data-type="plus"></button>
		</div>
		<div class="set-title" data-title="<?= $element['PRICE'] ?>р"><?= $element['NAME'] ?></div>
	</div>
<? endforeach ?>
