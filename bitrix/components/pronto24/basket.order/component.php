<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

use Bitrix\Main\Page\Asset;
Asset::getInstance()->addJs(SITE_TEMPLATE_PATH . '/js/location.js');
Asset::getInstance()->addJs('https://api-maps.yandex.ru/2.0-stable/?load=package.full&mode=release&lang=ru-RU');

if (CModule::IncludeModule("sale") && CModule::IncludeModule("catalog")) {
	global $USER;

	$rsUser = CUser::GetByID($USER->GetID());
	$arUser = $rsUser->Fetch();
	$arResult['NAME'] = $arUser['NAME'];
	$arResult['PHONE'] = $arUser['PERSONAL_PHONE'];

	$dbBasketItems = CSaleBasket::GetList(
		array(
			"NAME" => "ASC"
		),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
		false,
		false,
		array("ID", "NAME", "PRICE", "QUANTITY", "PRODUCT_ID", "DETAIL_PAGE_URL", "DISCOUNT_PRICE")
	);

	while ($arItem = $dbBasketItems->Fetch()) {
		$arResult[] = $arItem;
		$arResult['TOTAL_PRICE'] += $arItem['PRICE'] * $arItem['QUANTITY'];
	}
	$arResult['TOTAL_PRICE'] = round($arResult['TOTAL_PRICE']);
	$APPLICATION->IncludeComponent("pronto24:location.list");

}

$this->IncludeComponentTemplate();