<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/header.php");
$APPLICATION->SetTitle("Горячие блюда");
?>
	<div class="content">
		<div class="wrapper index product hot-style">
			<div class="intro">
				Горячие блюда
			</div>
			<div class="sort">
				<a href="/hot/" <?= empty($_GET)?"class='active'":"" ?>>все</a>
				<a href="/hot/?meat" <?= isset($_GET['meat'])?"class='active'":"" ?>>мясо</a>
				<a href="/hot/?chicken" <?= isset($_GET['chicken'])?"class='active'":"" ?>>курица</a>
				<a href="/hot/?fish" <?= isset($_GET['fish'])?"class='active'":"" ?>>рыба</a>
				<a href="/hot/?garnish" <?= isset($_GET['garnish'])?"class='active'":"" ?>>гарнир</a>
				<a href="/hot/?sauce" <?= isset($_GET['sauce'])?"class='active'":"" ?>>соус</a>
			</div>
			<? $APPLICATION->IncludeComponent("pronto24:catalog.section", '.default', Array('IBLOCK_ID' => 10)); ?>
		</div>
	</div>

<? require($_SERVER["DOCUMENT_ROOT"] . "/bitrix/footer.php") ?>