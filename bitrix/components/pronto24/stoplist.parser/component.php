<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

unset($_SESSION['STOP_LIST']);

$dir = $_SERVER['DOCUMENT_ROOT'] . '/xml/';

$filename = $dir . "STOP_LIST_$arParams[restaurant_id].XML";
if(file_exists($filename)) {
	try {
		$xmlStr = file_get_contents($filename);
		$obStopList = new SimpleXMLElement($xmlStr);
		$items = $obStopList->items;

		foreach ($items->item as $item) {
			$attributes = $item->attributes();
			if ((int)$attributes['restaurant_id'] == $arParams['restaurant_id'])
				$_SESSION['STOP_LIST'][] = (int)$attributes['id'];
		}
	} catch (Exception $e) {
		$to = "warlight@mail.ru";
		$subject = "Ошибка STOPLIST на сайте Пронто";
		$subject = "=?UTF-8?B?" . base64_encode($subject) . "?=";
		$from = "Сайт pronto";

		$message .= "<br/><b>Стоплист для ресторана невозможно распарсить. Проблемный ID ресторана:</b> " . $arParams['restaurant_id'];
		$message .= "<br/><b>Блюда были приняты в заказ без стоплиста.</b>";
		$message .= "<br/><b>Требуется проверка стоплиста!</b>";

		$headers = "From: $from\r\nReply-To: \r\n";
		$headers .= "Content-type: text/html; charset=utf-8;\r\n";

		mail($to, $subject, $message, $headers);
	}
}