<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

$result = true;

if(CModule::IncludeModule('iblock') && CModule::IncludeModule("sale")) {
	$dbBasketItems = CSaleBasket::GetList(
		array(
			"NAME" => "ASC"
		),
		array(
			"FUSER_ID" => CSaleBasket::GetBasketUserID(),
			"LID" => SITE_ID,
			"ORDER_ID" => "NULL"
		),
		false,
		false,
		array("ID", "PRODUCT_ID", "QUANTITY", "PRICE", "QUANTITY", "DISCOUNT_PRICE")
	);

	while ($arItem = $dbBasketItems->Fetch()) {
		$res = CIBlockElement::GetList(Array(), Array('ID' => $arItem['PRODUCT_ID']), false, false, Array('ID', 'IBLOCK_ID', 'PROPERTY_OUTER_ID'));
		$arElement = $res->GetNext();
		if(in_array($arElement['PROPERTY_OUTER_ID_VALUE'], $_SESSION['STOP_LIST']))
			$result = false;
	}
}

return $result;