<? $addScript = array(); ?>
<div class="restaurant-ul" data-restaurant-ul>
<? foreach ($arResult['PLACES'] as $element): ?>
	<? if (isset($element['METRO'])) : ?>
		<div class="restaurant-list <?= isset($element['B_CHILDREN']) ? 'children' : '' ?>">
			<a href="/restaurants/<?= $element['CODE'] ?>">
				<?= implode('<br>', $element['METRO']); ?>
				<br>
				<?= $element['LOCATION'] ?>
			</a>
			<? foreach ($element['METRO_COLOR'] as $color) : ?>
				<div style="background:#<?= $color ?>!important" class="line"></div>
			<? endforeach ?>
		</div>
	<? else: ?>
		<div class="restaurant-list">
			<a href="/restaurants/<?= $element['CODE'] ?>">
				<?= $element['LOCATION'] ?>
			</a>
		</div>
	<? endif ?>
<? endforeach; ?>
</div>

<script>
	function points() {
		var points = [
			<? foreach ($arResult['PLACES'] as $element): ?>
			{id: "<?=$element['CODE']?>", geo: [<?= $element['GEO'] ?>]},
			<? endforeach?>
		];
		return points;
	}

	function children_points() {
		var points = [
			<? foreach ($arResult['PLACES'] as $element): ?>
			<? if($element['B_CHILDREN'] == 'Y') :?>
			{id: "<?=$element['CODE']?>", geo: [<?= $element['GEO'] ?>]},
			<? endif ?>
			<? endforeach?>
		];
		return points;
	}

	function banquet_points() {
		var points = [
			<? foreach ($arResult['PLACES'] as $element): ?>
			<? if($element['B_BANQUET'] == 'Y') :?>
			{id: "<?=$element['CODE']?>",geo: [<?= $element['GEO'] ?>]},
			<? endif ?>
			<? endforeach?>
		];
		return points;
	}

	function lunch_points() {
		var points = [
			<? foreach ($arResult['PLACES'] as $element): ?>
			<? if($element['B_LUNCH'] == 'Y') :?>
			{id: "<?=$element['CODE']?>",geo: [<?= $element['GEO'] ?>]},
			<? endif ?>
			<? endforeach?>
		];
		return points;
	}

	function music_points() {
		var points = [
			<? foreach ($arResult['PLACES'] as $element): ?>
			<? if($element['B_MUSIC'] == 'Y') :?>
			{id: "<?=$element['CODE']?>",geo: [<?= $element['GEO'] ?>]},
			<? endif ?>
			<? endforeach?>
		];
		return points;
	}

	function center() {
		return [<?= $arResult['CENTER'] ?>];
	}

	function zoom() {
		return <?= $arResult['ZOOM'] ?>;
	}
</script>
