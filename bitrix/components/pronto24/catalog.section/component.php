<? if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED !== true) die();

CModule::IncludeModule('iblock');

$arProperties = Array(
	'ID',
	'CODE',
	'IBLOCK_ID',
	'NAME',
	'DETAIL_PICTURE'
);

$arFilters = Array("IBLOCK_ID" => $arParams['IBLOCK_ID'], "ACTIVE" => "Y");

if (isset($_GET)) {
	foreach ($_GET as $key => $val) {
		$arFilters['PROPERTY_B_' . strtoupper($key) . '_VALUE'] = "Y";
	}
}

if ($arParams['IBLOCK_ID'] == 12) {
	$arFilters['PROPERTY_POSITION_VALUE'] = $arParams['POSITION'];
}

if ($arParams['IBLOCK_ID'] == 5) {
	$arFilters['!PROPERTY_B_CONSTRUCTOR_VALUE'] = "Y";
}

$res = CIBlockElement::GetList(Array("SORT" => "ASC"), $arFilters, false, false, $arProperties);

if ($arParams['IBLOCK_ID'] == 5) {//pizza!!
	$bMain = ($APPLICATION->GetCurPage() == '/');
	$arLinked = Array();
	while ($obElement = $res->GetNextElement()) {
		$arElement = $obElement->fields;
		$arProps = $obElement->GetProperties();
		foreach ($arProps as $key => $property) {
			$arElement[$key] = $property['VALUE'];
			if ($key == 'HTML_TITLE') {
				$arElement['~HTML_TITLE'] = $property['~VALUE'];
			}
		}
		//STOP_LIST
		if(empty($_SESSION['STOP_LIST']) || (isset($_SESSION['STOP_LIST']) && !in_array($arElement['OUTER_ID'], $_SESSION['STOP_LIST']))) {
			if ($arElement['LINK_TO']) {
				$arLinked[$arElement['LINK_TO']] = $arElement;
			} else {
				if (!$bMain && $arElement["MORE_PICTURES"][0])
					$picture = CFile::GetFileArray($arElement["MORE_PICTURES"][0]);
				else
					$picture = CFile::GetFileArray($arElement["DETAIL_PICTURE"]);
				$arElement['SRC'] = $picture['SRC'];
				$arElement['SIZES'][] = Array(
					'SIZE' => $arElement['SIZE'],
					'ID' => $arElement['ID'],
					'PRICE' => getPrice($arElement['ID']));
				$arResult['elements'][] = $arElement;
			}
		}
	}

	foreach ($arResult['elements'] as $key => $arPizza) {
		if (isset($arLinked[$arPizza['ID']])) {
			$arResult['elements'][$key]['SIZES'][] = Array(
				'SIZE' => $arLinked[$arPizza['ID']]['SIZE'],
				'ID' => $arLinked[$arPizza['ID']]['ID'],
				'PRICE' => getPrice($arLinked[$arPizza['ID']]['ID']));
			$arResult['NAME'] = compareStrings($arResult['NAME'], $arPizza['NAME']);
		}
	}
} else {
	while ($obElement = $res->GetNextElement()) {
		$arElement = $obElement->fields;
		$arProps = $obElement->GetProperties();
		foreach ($arProps as $key => $property) {
			$arElement[$key] = $property['VALUE'];
		}
		//STOP_LIST
		if(empty($_SESSION['STOP_LIST']) || (isset($_SESSION['STOP_LIST']) && !in_array($arElement['OUTER_ID'], $_SESSION['STOP_LIST']))) {
			$arElement['TASTES'] = $arProps['TASTES']['VALUE'];
			$picture = CFile::GetFileArray($arElement["DETAIL_PICTURE"]);
			if ($arParams['IBLOCK_ID'] != 12) {
				$arElement['PRICE'] = getPrice($arElement['ID']);
			}
			$arElement['SRC'] = $picture['SRC'];
			$arResult['elements'][] = $arElement;
		}
	}
}

switch ($arParams['IBLOCK_ID']) {
	case 5:
		$arResult['template'] = 'pizza';
		break;
	case 12:
		if ($arParams['POSITION'] == 4) {
			$arResult['template'] = 'children.slider';
		} else {
			$arResult['template'] = 'children';
		}
		break;
	default:
		$arResult['template'] = '.default';
		break;
}

$this->IncludeComponentTemplate();