<?if (!defined('B_PROLOG_INCLUDED') || B_PROLOG_INCLUDED!==true)die();

if(isset($_POST['login']) && isset($_POST['password'])) {
	$rawLogin = $_POST['login'];
	$rawLogin = str_replace(Array('+',' ', '-', '(', ')'), Array('', '', '', '', ''), $rawLogin);
	$firstLetter = substr($rawLogin, 0, 1);
	if($firstLetter == 8 || $firstLetter == 7) {
		$rawLogin = substr($rawLogin, 1);
	}
	$firstLetter = substr($rawLogin, 0, 1);
	if($firstLetter != 9) {
		return array('result' => 'error', 'error' => 'mobile');
	}

	global $USER;
	$arAuthResult = $USER->Login($rawLogin, $_POST['password']);
	if(isset($arAuthResult['TYPE'])) {
		$result = array('result' => 'error');
	}
	else {
		$result = array('result' => 'ok');
	}
	return $result;
}